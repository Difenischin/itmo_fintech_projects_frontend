import Vue from 'vue';
//https://next.vuetifyjs.com/ru/guides/a-la-carte

import Vuetify from 'vuetify/lib';
import VGrid from 'vuetify/lib/components/VGrid/';
import {
    VApp,
    VCard,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VToolbar,
    VAlert,
    VTextField,
    VSubheader,
    VDataTable,
    VCheckbox
} from 'vuetify/lib';

Vue.use(Vuetify, {
    components: {
        Vuetify,
        VApp,
        VCard,
        VNavigationDrawer,
        VFooter,
        VList,
        VBtn,
        VIcon,
        VGrid,
        VToolbar,
        VAlert,
        VTextField,
        VSubheader,
        VDataTable,
        VCheckbox
    }
});

const opts = {};

export default new Vuetify(opts)