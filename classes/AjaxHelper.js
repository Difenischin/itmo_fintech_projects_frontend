export default class AjaxHelper {
    static productionUrl = 'http://185.185.70.11:5000';
    static productionTransactionUrl = '//http://185.185.71.254:5000';
    static testUrl = 'http://localhost:5000';
    static productionUrlBt = 'http://185.185.70.11:3001';

    static getUrlBase() {
        return AjaxHelper.productionUrl;
        // 'http://localhost:5000/get_all_transaction_production';
        // "http://185.185.71.254:8888/getAllTransactionProduction";
    }

    static getUrlAllTransactionProduction() {
        return `${AjaxHelper.getUrlBase()}/get_all_transaction_production`;
    }

    static getUrlPricesBySectors(ticker) {
        return `${AjaxHelper.getUrlBase()}/get_prices_sector/${ticker}`;
    }

    static getUrlCorrelationsBySectorsTicker(ticker) {
        return `${AjaxHelper.getUrlBase()}/get_correlations_sector_ticker/${ticker}`;
    }

    static getUrlCorrelationsBySectorsShortName(ticker) {
        return `${AjaxHelper.getUrlBase()}/get_correlations_sector_short_name/${ticker}`;
    }

    static getUrlMultiplikatorsBySectors(ticker) {
        return `${AjaxHelper.getUrlBase()}/get_multiplikators_sector/${ticker}`;
    }

    static getUrlMultiplikatorsAll() {
        return `${AjaxHelper.getUrlBase()}/get_multiplikators_all`;
    }

    static getUrlManipulationFZ(ticker) {
        return `${AjaxHelper.getUrlBase()}/manipulation_fz/${ticker}`;
    }

    static getUrlMLFilterNegativeDays(date) {
        return `${AjaxHelper.getUrlBase()}/modelFilterNegativeDaysJson/${date}`;
    }

    static getUrlMLFilterNegativeDaysLast(count) {
        return `${AjaxHelper.getUrlBase()}/modelFilterNegativeDaysJsonLast/${count}`;
    }

    static getUrlMultiplikatorsBest() {
        return `${AjaxHelper.getUrlBase()}/get_multiplikators_best`;
    }

    static getUrlMultiplikatorsBestBt() {
        return `${AjaxHelper.productionUrlBt}/`;
    }

    static getUrlDataUploadDateBt() {
        return `${AjaxHelper.productionUrlBt}/date`;
    }

    static getUrlForcedUpdateBt() {
        return `${AjaxHelper.productionUrlBt}/update`;
    }

    static getUrlCorrelationsTickers() {
        return `${AjaxHelper.getUrlBase()}/get_correlations_tickers`;
    }

    static getUrlMultiplikatorsTickers() {
        return `${AjaxHelper.getUrlBase()}/get_multiplikators_tickers`;
    }
}
