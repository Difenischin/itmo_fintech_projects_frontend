export default class Transactions {
    constructor(allTransactions) {
        this.transactions = allTransactions;
        this.strategyStatistic = {};
        for (const strategyId of this.getStrategyIds()) {
            this.strategyStatistic[strategyId] = this.getStrategyStatistic(strategyId);
        }
    }

    getStrategyStatistic(strategyId) {
        const arrSum = arr => arr.reduce((a, b) => a + b, 0);
        const allStrategyTransaction = this.getTransactionsFor(strategyId);
        const statistic = {
            positionSumSell: [],
            positionSumBuy: [],
            positionCountSell: [],
            positionCountBuy: [],
            positionSumCleanSell: [],
            positionSumCleanBuy: [],
            profitCleanPosition: 0,
            profitTotal: 0,
            strategyId,
            strategyName:allStrategyTransaction[0]['tradingAccount'],
        };
        for (const {transactionPrice, transactionVolume, transactionDirection} of allStrategyTransaction) {
            if (transactionDirection === "Sell") {
                for (let i = 0; i < transactionVolume; i++) {
                    statistic.positionSumSell.push(transactionPrice);
                }
                statistic.positionCountSell.push(transactionVolume);
            }
            if (transactionDirection === "Buy") {
                for (let i = 0; i < transactionVolume; i++) {
                    statistic.positionSumBuy.push(transactionPrice);
                }
                statistic.positionCountBuy.push(transactionVolume);
            }
        }
        statistic.profitTotal = +this.arrSum(statistic.positionSumSell) - this.arrSum(statistic.positionSumBuy);
        statistic.positionSumCleanSell = getSellPositionSumClean(statistic);
        statistic.positionSumCleanBuy = getBuyPositionSumClean(statistic);
        statistic.profitCleanPosition = this.arrSum(statistic.positionSumCleanSell) - this.arrSum(statistic.positionSumCleanBuy);
        statistic.profitCleanPositionCount = statistic.positionSumCleanBuy.length;
        return statistic;

        function getSellPositionSumClean({positionCountSell, positionCountBuy, positionSumSell}) {
            const countOpenPosition = arrSum(positionCountSell) - arrSum(positionCountBuy);
            if (arrSum(positionCountSell) > arrSum(positionCountBuy)) {
                return positionSumSell.slice(0, positionSumSell.length - countOpenPosition);
            }
            return positionSumSell;
        }

        function getBuyPositionSumClean({positionCountSell, positionCountBuy, positionSumBuy}) {
            const countOpenPosition = arrSum(positionCountSell) - arrSum(positionCountBuy);
            if (arrSum(positionCountSell) < arrSum(positionCountBuy)) {
                return positionSumBuy.slice(0, positionSumBuy.length - countOpenPosition);
            }
            return positionSumBuy;
        }
    }


    getTransactionsFor(strategyId) {
        const result = this.transactions.filter(
            transact => transact.strategyId === strategyId);
        if (result.length && result.length > 0) {
            return result;
        }
        return [];
    }

    getStrategyIds() {
        const strategyIds = new Set();
        for (const transact of this.transactions) {
            strategyIds.add(transact.strategyId);
        }
        return [...strategyIds];
    }

    getStatistic(strategyId) {
        return this.strategyStatistic[strategyId];
    }

    arrSum(arr) {
        return arr.reduce((a, b) => a + b, 0);
    }
}
/*
{
            "transactionStockId": 38602343,
            "instrumentName": "SRM9",
            "strategyId": 4,
            "transactionPrice": 21926.0,
            "transactionVolume": 1.0,
            "transactionMode": "Limit",
            "transactionStockStatus": "Done",
            "transactionDateDone": "2019-05-08T00:00:00.000Z",
            "transactionTimeDone": "1970-01-01T18:05:21.000Z",
            "transactionComment": "",
            "tradingMode": "Production",
            "tradingAccount": "MolotNegative_0.011608_0.020656_0.005836_800_hash_194429253128112223",
            "transactionDirection": "Sell"
        }*/