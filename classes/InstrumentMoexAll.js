export default class InstrumentMoexAll {
    static getAll() {
        return [{
            "company_id": "31", "short_name": "ГАЗПРОМ ао", "ticker": "GAZP",
            "company_desc": "Энергетическая компания. Лидер мировой газодобывающей отрасли. Контролируется государством",
            "company_url": "www.gazprom.ru", "sector_name": "Нефть и газ", "sector_id": "3"
        },
            {
                "company_id": "503", "short_name": "FIVE", "ticker": "FIVE",
                "company_desc": "Владеет магазинами «Перекресток», «Пятерочка» и «Карусель»",
                "company_url": "http://x5.ru", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "135", "short_name": "СевСт-ао", "ticker": "CHMF",
                "company_desc": "Одно из крупнейших в мире сталелитейных и горнодобывающих предприятий",
                "company_url": "www.severstal.com", "sector_name": "Металлургия", "sector_id": "7"
            }, {
                "company_id": "138", "short_name": "Синерг. ао", "ticker": "BELU",
                "company_desc": "Лидер в России по производству крепкого алкоголя. Раньше компания называлась — «Синергия»",
                "company_url": "www.sygroup.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "179", "short_name": "ЯрШинЗ ао", "ticker": "YASH", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "507", "short_name": "ABBN", "ticker": "ABBN",
                "company_desc": "Предоставляет все виды финансовых услуг. Работает в Казахстане",
                "company_url": "", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "509", "short_name": "КузбТК ао", "ticker": "KBTK",
                "company_desc": "Добывает и поставляет уголь в Западной Сибири",
                "company_url": "http://www.e-disclosure.ru/portal/company.aspx?id=5964,  http://www.oaoktk.ru",
                "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "457", "short_name": "Европлан", "ticker": "SFIN",
                "company_desc": "Инвестиционная компания. Владеет лизинговой компанией «Европлан», страховой компанией ВСК и пенсионным фондом «Сафмар». Входит в «Сафмар»",
                "company_url": "www.safmarinvest.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "508", "short_name": "ENPL", "ticker": "ENPL",
                "company_desc": "Металлургическая компания. Производит алюминий. Владеет частью «Русала»",
                "company_url": "", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "446", "short_name": "OBUV", "ticker": "OBUV",
                "company_desc": "Обувная компания. Владеет брендом Westfalika",
                "company_url": "http://obuvrus.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "180", "short_name": "iQIWI", "ticker": "QIWI",
                "company_desc": "Международный платежный сервис. Лидер российского рынка моментальных платежей",
                "company_url": "www.qiwi.com", "sector_name": "Технологии", "sector_id": "1"
            },
            {
                "company_id": "438", "short_name": "АстрЭнСб", "ticker": "ASSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричесво в Астраханской области",
                "company_url": "http://www.astsbyt.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "48", "short_name": "КМЗ", "ticker": "KMEZ",
                "company_desc": "Машиностроительная компания. Выпускает газовые центрифуги для атомной энергетики. Входит в «Росатом»",
                "company_url": "http://www.kvmz.ru/", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "181", "short_name": "Лента др", "ticker": "LNTA",
                "company_desc": "Одна из пяти крупнейших сетей гипермаркетов в России",
                "company_url": "http://www.lentainvestor.com/", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "407", "short_name": "МагадЭн ап", "ticker": "MAGEP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Магаданскую область, Чукотский автономный округ, частично Якутию. Входит в «Русгидро»",
                "company_url": "www.magadanenergo/content/raskrytie-informatsii",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "19", "short_name": "БашИнСв ао", "ticker": "BISV",
                "company_desc": "Телекоммуникационная компания. Предоставляет услуги интернета, телефонии, телевидения. Работает в республике Башкирия. Входит в «Ростелеком»",
                "company_url": "www.bashtel.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "88", "short_name": "МосБиржа", "ticker": "MOEX",
                "company_desc": "Крупнейшая биржа в России. Контролируется государством",
                "company_url": "www.moex.com", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "44", "short_name": "iИСКЧ ао", "ticker": "ISKJ",
                "company_desc": "Биотехнологическая компания. Разрабатывает лекарства и медицинские технологии. Владеет банком стволовых клеток «Гемабанк»",
                "company_url": "http://www.hsci.ru", "sector_name": "Здравоохранение", "sector_id": "10"
            },
            {
                "company_id": "19", "short_name": "БашИнСв ап", "ticker": "BISVP",
                "company_desc": "Телекоммуникационная компания. Предоставляет услуги интернета, телефонии, телевидения. Работает в республике Башкирия. Входит в «Ростелеком»",
                "company_url": "www.bashtel.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "87", "short_name": "Морион ао", "ticker": "MORI",
                "company_desc": "Научно-производственная компания. Разрабатывает, производит, устанавливает и обслуживает телекоммуникационное оборудование",
                "company_url": "http://www.morion.ru/", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "16", "short_name": "БСП ао", "ticker": "BSPB",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Работает в Северо-Западном федеральном округе",
                "company_url": "www.bspb.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "100", "short_name": "iАвиастКао", "ticker": "UNAC",
                "company_desc": "Производит и продает гражданскую и военную авиатехнику. Владеет брендами «Су», «МиГ», «Ил», «Ту», «Як», SSJ. Контролируется государством",
                "company_url": "www.uacrussia.ru", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "171", "short_name": "ЧелябЭС ап", "ticker": "CLSBP",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Челябинскую область",
                "company_url": "http://www.esbt74.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "50", "short_name": "ОргСинт ао", "ticker": "KZOS",
                "company_desc": "Химическая компания. Производит полиэтилен, ацетон, реагенты для добычи нефти и осушки природного газа. Входит в ТАИФ",
                "company_url": "www.kazanorgsintez.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "50", "short_name": "ОргСинт ап", "ticker": "KZOSP",
                "company_desc": "Химическая компания. Производит полиэтилен, ацетон, реагенты для добычи нефти и осушки природного газа. Входит в ТАИФ",
                "company_url": "www.kazanorgsintez.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "92", "short_name": "iМультиСис", "ticker": "MSST",
                "company_desc": "Многопрофильный холдинг. Занимается строительством и ремонтом зданий, инженерных сетей, устанавливает и обслуживает водосчетчики",
                "company_url": "http://мультисистема.рф", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "115", "short_name": "ЭнергияРКК", "ticker": "RKKE",
                "company_desc": "Ракетно-космическая компания. Разрабатывает ракеты, спутники, межпланетные станции. Создает космические корабли для NASA, ESA и «Роскосмоса»",
                "company_url": "http://www.energia.ru/ru/corporation/oao.html",
                "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "128", "short_name": "СЗПароход", "ticker": "SZPR",
                "company_desc": "Судоходная компания. Перевозит грузы по речным и морским маршутам. Входит в UCL Holding",
                "company_url": "www.nwsc.spb.ru", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "86", "short_name": "Мечел ао", "ticker": "MTLR",
                "company_desc": "Горнодобывающая и металлургическая компания. Добывает и перерабатывает  уголь и железо. Владеет торговыми портами и транспортными операторами",
                "company_url": "www.mechel.ru", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "55", "short_name": "М.видео", "ticker": "MVID",
                "company_desc": "Крупнейшая в России торговая сеть по продаже бытовой техники и электроники",
                "company_url": "www.mvideo.ru", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "129", "short_name": "Мегион-ап", "ticker": "MFGSP",
                "company_desc": "Нефтегазовая компания. Разрабатывает месторождения, добывает нефть и газ в ХМАО. Ранее называлась «Мегионнефтегаз». Входит в «Славнефть»",
                "company_url": "www.sn-mng.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "95", "short_name": "Новатэк ао", "ticker": "NVTK",
                "company_desc": "Крупнейший независимый производитель природного газа в России",
                "company_url": "www.novatek.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "152", "short_name": "ТНСэнРст", "ticker": "RTSB",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Ростовской области. Входит в «ТНС Энерго»",
                "company_url": "https://rostov.tns-e.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "152", "short_name": "ТНСэнРст-п", "ticker": "RTSBP",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Ростовской области. Входит в «ТНС Энерго»",
                "company_url": "https://rostov.tns-e.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "404", "short_name": "СаратЭн-ап", "ticker": "SAREP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Саратовскую область. Входит в «Интер РАО»",
                "company_url": "www.saratovenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "402", "short_name": "МКБ ао", "ticker": "CBOM",
                "company_desc": "Частный банк. Работает в Москве и Московской области", "company_url": "",
                "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "163", "short_name": "iФармсинтз", "ticker": "LIFE",
                "company_desc": "Фармацевтическая компания. Производит и продает лекарства. Входит в «Роснано»",
                "company_url": "http://www.pharmsynthez.com", "sector_name": "Здравоохранение",
                "sector_id": "10"
            }, {
                "company_id": "407", "short_name": "МагадЭн ао", "ticker": "MAGE",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Магаданскую область, Чукотский автономный округ, частично Якутию. Входит в «Русгидро»",
                "company_url": "www.magadanenergo/content/raskrytie-informatsii",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "404", "short_name": "СаратЭн-ао", "ticker": "SARE",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Саратовскую область. Входит в «Интер РАО»",
                "company_url": "www.saratovenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "403", "short_name": "УрКузница", "ticker": "URKZ",
                "company_desc": "Металлургическая компания. Производит штампованную продукцию из стали и сплавов для машиностроения. Входит в «Мечел»",
                "company_url": "http://www.mechel.ru/shareholders/disclosure/filials/metallurgy/kuznitsa/",
                "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "400", "short_name": "ОВК ао", "ticker": "UWGN",
                "company_desc": "Железнодорожный холдинг. Производит и продает грузовые вагоны. Владеет «Тихвинским вагоностроительным заводом»",
                "company_url": "", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "420", "short_name": "Химпром ао", "ticker": "HIMC",
                "company_desc": "Производит химические вещества для теплоэнергетики, нефтедобывающей и нефтеперерабатывающей промышленности. Входит в «Оргсинтез»",
                "company_url": "http://www.himprom.com/", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "408", "short_name": "МЕРИДИАН", "ticker": "MERF",
                "company_desc": "Инвестиционный фонд. Покупает и сдает недвижимость в Москве. Входит в «Тринфико»",
                "company_url": "http://www.if-meridian.ru/", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "409", "short_name": "Отисифарм", "ticker": "OTCP",
                "company_desc": "Фармацевтическая компания. Производит и продает лекарства. Владеет брендами «Арбидол», «Компливит», «Афобазол», «Пенталгин»",
                "company_url": "http://otcpharm.ru/", "sector_name": "Здравоохранение", "sector_id": "10"
            },
            {
                "company_id": "412", "short_name": "ТНСэнМарЭл", "ticker": "MISB",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством республику Марий Эл. Ранее называлась «Мариэнергосбыт». Входит в «ТНС энерго»",
                "company_url": "www.marienergosbyt.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "410", "short_name": "iНаукаСвяз", "ticker": "NSVZ",
                "company_desc": "Предоставляет услуги интернета, телефонии, телевидения, видеонаблюдения, сигнализации и облачного хранилища в России, Европе и США",
                "company_url": "oaonsv.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "419", "short_name": "iЧЗПСН ао", "ticker": "PRFN",
                "company_desc": "Строительная компания. Производит металлоконструкции, сэндвич-панели, блок-контейнеры, профнастил, металлочерепицу, строительные профили",
                "company_url": "https://стройсистема.рф", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "414", "short_name": "РГС СК ао", "ticker": "RGSS",
                "company_desc": "Крупнейшая в России страховая компания", "company_url": "www.rgs.ru",
                "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "142", "short_name": "СтаврЭнСбп", "ticker": "STSBP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Ставропольский край",
                "company_url": "http://www.staves.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "416", "short_name": "ТАНТАЛ ао", "ticker": "TANL",
                "company_desc": "Изготавливает электровакуумные приборы, проектирует и выпускает современные системы и оборудование для ВПК и аэрокосмической отрасли",
                "company_url": "http://www.oao-tantal.ru/news.php", "sector_name": "Промышленные товары",
                "sector_id": "8"
            }, {
                "company_id": "422", "short_name": "Кокс ао", "ticker": "KSGR",
                "company_desc": "Металлургическая и горнодобывающая компания. Продает товарный кокс и чугун",
                "company_url": "http://www.koksgroup.ru", "sector_name": "Металлургия",
                "sector_id": "7"
            },
            {
                "company_id": "411", "short_name": "ТамбЭнСб", "ticker": "TASB",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Тамбовскую область. Входит в «Интер РАО»",
                "company_url": "www.tesk.su", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "423", "short_name": "Телеграф", "ticker": "CNTL",
                "company_desc": "Телекоммуникационная компания. Предоставляет услуги мобильной связи и доступа в интернет. Входит в «Ростелеком»",
                "company_url": "http://www.moscow.cnt.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "420", "short_name": "Химпром ап", "ticker": "HIMCP",
                "company_desc": "Производит химические вещества для теплоэнергетики, нефтедобывающей и нефтеперерабатывающей промышленности. Входит в «Оргсинтез»",
                "company_url": "http://www.himprom.com/", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "412", "short_name": "МариЭнСб-п", "ticker": "MISBP",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством республику Марий Эл. Ранее называлась «Мариэнергосбыт». Входит в «ТНС энерго»",
                "company_url": "www.marienergosbyt.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "416", "short_name": "ТАНТАЛ ап", "ticker": "TANLP",
                "company_desc": "Изготавливает электровакуумные приборы, проектирует и выпускает современные системы и оборудование для ВПК и аэрокосмической отрасли",
                "company_url": "http://www.oao-tantal.ru/news.php", "sector_name": "Промышленные товары",
                "sector_id": "8"
            }, {
                "company_id": "417", "short_name": "iЗаводДИОД", "ticker": "DIOD",
                "company_desc": "Выпускает препараты, биологически активные добавки к пище, лечебную косметику. Занимается исследованиями и внедрением новых технологий",
                "company_url": "www.diod.ru", "sector_name": "Здравоохранение",
                "sector_id": "10"
            },
            {
                "company_id": "421", "short_name": "iДонскЗР", "ticker": "DZRD",
                "company_desc": "Керамический завод. Производит и выпускает более 160 разных изделий из керамики",
                "company_url": "http;//www.alund.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            },
            {
                "company_id": "15", "short_name": "БМ-Банк", "ticker": "MMBM", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "425", "short_name": "КурганГКао", "ticker": "KGKC",
                "company_desc": "Энергетическая компания. Поставляет электричество в Кургане и Шадринске",
                "company_url": "www.kgk-kurgan.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "425", "short_name": "КурганГКап", "ticker": "KGKCP",
                "company_desc": "Энергетическая компания. Поставляет электричество в Кургане и Шадринске",
                "company_url": "www.kgk-kurgan.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "428", "short_name": "КСБ ап", "ticker": "KTSBP",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Костромской области",
                "company_url": "www.k-sc.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "432", "short_name": "КЗМС ао", "ticker": "KZMS",
                "company_desc": "Металлургическая компания. Производит металлические и синтетические сетки",
                "company_url": "http://www.rosset-kzms.ru/company/index.php", "sector_name": "Металлургия",
                "sector_id": "7"
            }, {
                "company_id": "436", "short_name": "ЛЭСК ао", "ticker": "LPSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Липецкой области",
                "company_url": "http://www.lesk.ru/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "433", "short_name": "МРСК СК", "ticker": "MRKK",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Северо-Кавказском федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsk-sk.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "429", "short_name": "ГЕОТЕК ао", "ticker": "GTSS",
                "company_desc": "Геофизическая компания. Предоставляет услуги сейсморазведки",
                "company_url": "www.gseis.ru/default.aspx", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "430", "short_name": "МурмТЭЦ-ао", "ticker": "MUGS",
                "company_desc": "Тепловая электростанция. Обеспечивает теплом и горячей водой жителей Мурманска",
                "company_url": "http://www.murmantec.com/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "439", "short_name": "iНПОНаука", "ticker": "NAUK",
                "company_desc": "Производит системы кондиционирования воздуха, регулирования давления и жизнеобеспечения для авиационно-космической техники",
                "company_url": "http://npo-nauka.ru/", "sector_name": "Промышленные товары",
                "sector_id": "8"
            },
            {
                "company_id": "431", "short_name": "НКХП ао", "ticker": "NKHP",
                "company_desc": "Зерновой терминал. Перерабатывает и хранит зерновые культуры",
                "company_url": "http://novoroskhp.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "442", "short_name": "РДБанк ао", "ticker": "RDRB",
                "company_desc": "Коммерческий банк. Предоставляет все финансовые услуги предприятиям",
                "company_url": "www.rdb.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "441", "short_name": "Русолово", "ticker": "ROLO",
                "company_desc": "Добывает олово в месторождениях на Дальнем Востоке",
                "company_url": "http://rus-olovo.ru/", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "426", "short_name": "КУЗОЦМ ао", "ticker": "KUNF",
                "company_desc": "Металлургическая компания. Выпускает продукцию из меди, никеля, цинка и сплавов на их основе. Входит в «Ренова»",
                "company_url": "http://www.kuzocm.ru/", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "448", "short_name": "Русгрэйн", "ticker": "RUGR",
                "company_desc": "Агропромышленная компания. Выращивает, перерабатывает и реализует зерновые, масличные культуры, производит и продает комбикормы, птицеводческую продукцию",
                "company_url": "www.rusgrain.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "449", "short_name": "ТЗА ао", "ticker": "TUZA",
                "company_desc": "Машиностоительная компания. Производит автобетоносмесители, стационарные бетононасосы, автобетононасосы, пожарные пеноподъемники",
                "company_url": "www.tzacom.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "443", "short_name": "ЗВЕЗДА ао", "ticker": "ZVEZ",
                "company_desc": "Машиностоительная компания. Производит дизельные двигатели",
                "company_url": "www.zvezda.spb.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "423", "short_name": "Телеграф-п", "ticker": "CNTLP",
                "company_desc": "Телекоммуникационная компания. Предоставляет услуги мобильной связи и доступа в интернет. Входит в «Ростелеком»",
                "company_url": "http://www.moscow.cnt.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "435", "short_name": "Ижсталь ап", "ticker": "IGSTP",
                "company_desc": "Металлургическая компания. Из металлопродукции предприятия изготавливают детали машин, подшипников, буровых установок",
                "company_url": "http://www.mechel.ru/shareholders/disclosure/filials/metallurgy/izhstal/",
                "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "428", "short_name": "КСБ ао", "ticker": "KTSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Костромской области",
                "company_url": "www.k-sc.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "434", "short_name": "iЛевенгук", "ticker": "LVHK",
                "company_desc": "Оптическая компания. Производит микроскопы, бинокли, телескопы, зрительные трубы, монокуляры, лупы и средства по уходу за оптикой",
                "company_url": "www.levenhuk.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "430", "short_name": "МурмТЭЦ-п", "ticker": "MUGSP",
                "company_desc": "Тепловая электростанция. Обеспечивает теплом и горячей водой жителей Мурманска",
                "company_url": "http://www.murmantec.com/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "447", "short_name": "ДагСб ао", "ticker": "DASB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Дагестане",
                "company_url": "http://www.dag-esk.ru", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "465", "short_name": "Кубанэнр", "ticker": "KUBE",
                "company_desc": "Электросетевая компания. Передает электричество в Адыгее и Краснодарском крае",
                "company_url": "www.kubanenergo.ru/stockholders/disclosure_of_information",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "458", "short_name": "ЭРКО ао", "ticker": "ERCO",
                "company_desc": "Энергоатомная компания. Владеет и управляет пакетами акций в финансовых и промышленных компаниях. Входит в «Росэнергоатом»",
                "company_url": "http://erco.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "466", "short_name": "КузнецкийБ", "ticker": "KUZB",
                "company_desc": "Частный банк. Предоставляет финансовые услуги: потребительское кредитование, расчетно-кассовое обслуживание, выпуск пластиковых карт, аренда сейфовых ячеек, вклады, денежные переводы. Работает в Пензе, Чебоксарах и Самаре",
                "company_url": "www.kuzbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "469", "short_name": "Физика ао", "ticker": "NPOF",
                "company_desc": "Машиностоительная компания. Производит микроэлектронные компоненты для авиации и ракетостроения",
                "company_url": "www.npofizika.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "472", "short_name": "ПавлАвт ао", "ticker": "PAZA",
                "company_desc": "Автомобилестроительная компания. Производит автобусы малого и среднего классов. Входит в ГАЗ",
                "company_url": "www.paz-bus.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "470", "short_name": "Медиахолд", "ticker": "ODVA",
                "company_desc": "Телевизионная компания. Работает в сфере кабельно-спутникового телевизионного вещания. Владеет телеканалом О2",
                "company_url": "www.o2tvbiz.ru", "sector_name": "Медиа", "sector_id": "16"
            },
            {
                "company_id": "474", "short_name": "Промсвб ао", "ticker": "PSBR",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Работает по всей России",
                "company_url": "www.psbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "473", "short_name": "Polymetal", "ticker": "POLY",
                "company_desc": "Горнорудная компания. Добывает серебро, золото и медь",
                "company_url": "www.polymetalinternational.com", "sector_name": "Драгоценности",
                "sector_id": "12"
            }, {
                "company_id": "450", "short_name": "iРоллман", "ticker": "RLMN",
                "company_desc": "Многопрофильный холдинг. Произодит фильтры и фильтроэлементы, изделия из пластика, автохимию и бытовую химию, домокомлпекты из дерева",
                "company_url": "rollman-gk.com", "sector_name": "Промышленные товары",
                "sector_id": "8"
            },
            {
                "company_id": "464", "short_name": "КосогМЗ ао", "ticker": "KMTZ",
                "company_desc": "Металлургическая компания. Производит чугун, ферромарганц, шлаковый щебень и кирпич. Занимается промышленным и художественным литьем",
                "company_url": "http://www.kmz-tula.ru/", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "450", "short_name": "iРоллман-п", "ticker": "RLMNP",
                "company_desc": "Многопрофильный холдинг. Произодит фильтры и фильтроэлементы, изделия из пластика, автохимию и бытовую химию, домокомлпекты из дерева",
                "company_url": "rollman-gk.com", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "476", "short_name": "СибГост ао", "ticker": "SIBG",
                "company_desc": "Производитель сублимированных продуктов питания. Собирает, перерабатывает и продает сушеные грибы, ягоды и орехи",
                "company_url": "www.siberiangostinec.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "468", "short_name": "МН-фонд ао", "ticker": "MNFD",
                "company_desc": "Инвестиционный фонд. Инвестирует в ценные бумаги, покупает земельные участки и коммерческую недвижимость, потом сдает их в аренду",
                "company_url": "www.mnfond.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "451", "short_name": "ЗИЛ ао", "ticker": "ZILL",
                "company_desc": "Автомобилестроительная компания. Производит грузовики и автобусы под брендом ЗИЛ. Контролируется государством",
                "company_url": "http://www.amo-zil.ru/", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "453", "short_name": "БестЭфБ ао", "ticker": "ALBK",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Входит в РТС",
                "company_url": "http://www.besteffortsbank.ru/", "sector_name": "Финансы",
                "sector_id": "2"
            },
            {
                "company_id": "455", "short_name": "Арсагера", "ticker": "ARSA",
                "company_desc": "Инвестиционная компания. Управляет паевыми инвестиционными фондами и предоставляет услуги индивидуального доверительного управления",
                "company_url": "http://www.arsagera.ru/", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "460", "short_name": "ГАЗ-сервис", "ticker": "GAZS",
                "company_desc": "Автомобилестроительная компания. Производит легкие и среднетоннажные автомобили, автобусы, грузовики. Входит в «Русские машины»",
                "company_url": "www.gaz-services.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "471", "short_name": "Омскшина", "ticker": "OMSH",
                "company_desc": "Машиностоительная компания. Производит шины для грузовых автомобилей, автобусов, троллейбусов и специальной техники",
                "company_url": "www.omsktyre.ru", "sector_name": "Промышленные товары",
                "sector_id": "8"
            },
            {
                "company_id": "475", "short_name": "РязЭнСб", "ticker": "RZSB",
                "company_desc": "Энергосбытовая компания. Передает электричество в Рязанской области. Входит в «Русгидро»",
                "company_url": "http://www.resk.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "483", "short_name": "ВостРАО ао", "ticker": "VRAO",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Дальний Восток. Входит в «Русгидро»",
                "company_url": "http://www.rao-esv.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "483", "short_name": "ВостРАО ап", "ticker": "VRAOP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Дальний Восток. Входит в «Русгидро»",
                "company_url": "http://www.rao-esv.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "118", "short_name": "RUSAL plc", "ticker": "RUAL",
                "company_desc": "Металлургическая компания. Производит алюминий и глинозем. Входит в En+",
                "company_url": "http://rusal.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "480", "short_name": "ОКС ао", "ticker": "UCSS",
                "company_desc": "Инвестиционная компания. Консультирует по финансовым вопросам, инвестирует и управляет ценными бумагами",
                "company_url": "www.ucsys.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "172", "short_name": "Юнипро ао", "ticker": "UPRO",
                "company_desc": "Владеет тепловыми электростанциями в ХМАО, Пермском, Красноярском, Смоленском и Московском регионах. Входит в немецкую Uniper",
                "company_url": "", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "484", "short_name": "ВТОРРЕСао", "ticker": "VTRS",
                "company_desc": "Металлургическая компания. Владеет площадками сбора и переработки лома и отходов черных и цветных металлов. Работает в СЗФО, УФО и ЦФО",
                "company_url": "vtorr.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "144", "short_name": "ТПлюс ао", "ticker": "VTGK", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "164", "short_name": "Фармстанд", "ticker": "PHST", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "174", "short_name": "ЭнелРос ао", "ticker": "ENRU",
                "company_desc": "Энергетическая компания. Владеет тепло- и электростанциями в Тверской, Свердловской областях и Ставропольском крае. Входит в Enel",
                "company_url": "www.enelrussia.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "452", "short_name": "AGRO-гдр", "ticker": "AGRO",
                "company_desc": "Сельскохозяйственная компания. Занимается свиноводством и растениеводством, производит сахар",
                "company_url": "http://www.rusagrogroup.ru/", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "479", "short_name": "ТКСМ ао", "ticker": "TUCH",
                "company_desc": "Комбинат строительных материалов. Производит щебень, песок, бетон, строительные растворы, дробильно-сортировочное оборудование",
                "company_url": "www.t-ksm.ru", "sector_name": "Строительство",
                "sector_id": "11"
            },
            {
                "company_id": "72", "short_name": "МосОблБанк", "ticker": "MOBB",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Владеет сетью из 57 офисов по всей России",
                "company_url": "http://mosoblbank.ru/", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "143", "short_name": "Сургнфгз", "ticker": "SNGS",
                "company_desc": "Нефтегазодобывающая компания. Одна из крупнейших в России",
                "company_url": "www.surgutneftegas.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "20", "short_name": "Башнефт ао", "ticker": "BANE",
                "company_desc": "Нефтяная компания. Головной офис расположен в Башкирии",
                "company_url": "www.bashneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "64", "short_name": "Куйбазот-п", "ticker": "KAZTP",
                "company_desc": "Химическая компания. Производит техническую нить, аммиачную селитру, полиамид",
                "company_url": "www.kuazot.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "133", "short_name": "СаратНПЗ-п", "ticker": "KRKNP",
                "company_desc": "Нефтеперерабатывающий завод. Выпускает бензин, дизельное топливо, мазут, техническую серу и другие продукты. Входит в «Роснефть»",
                "company_url": "http://www.saratov-npz.ru/", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "40", "short_name": "ДИКСИ ао", "ticker": "DIXY",
                "company_desc": "Одна из крупнейших продовольственных розничных сетей в России",
                "company_url": "dixygroup.ru", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "9", "short_name": "Акрон", "ticker": "AKRN",
                "company_desc": "Ведущий производитель минеральных удобрений в России и мире",
                "company_url": "www.acron.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "170", "short_name": "ЧЦЗ ао", "ticker": "CHZN",
                "company_desc": "Металлургическая компания. Добывает и обогащает руду, производит цинк и сплавы на его основе. Входит в УГМК",
                "company_url": "www.zinc.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "2", "short_name": "АЛРОСА ао", "ticker": "ALRS",
                "company_desc": "Алмазодобывающая компания. Добывает, перерабатывает и продает драгоценные камни",
                "company_url": "http://www.alrosa.ru", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "11", "short_name": "АшинскийМЗ", "ticker": "AMEZ",
                "company_desc": "Металлургическая компания. Перерабатывает сырье и продает изделия из стали. Владеет брендом «Амет». 80% продукции продает в России",
                "company_url": "http://www.amet.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "1", "short_name": "АВТОВАЗ ао", "ticker": "AVAZ",
                "company_desc": "Крупнейший производитель легковых автомобилей в России",
                "company_url": "info.avtovaz.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "45", "short_name": "ИнтерРАОао", "ticker": "IRAO",
                "company_desc": "Энергетическая компания. Работает в России, а также в странах Европы и СНГ",
                "company_url": "www.interrao.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "62", "short_name": "КубаньЭнСб", "ticker": "KBSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество жителям в Адыгее и Краснодарском крае",
                "company_url": "https://kuban.tns-e.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "52", "short_name": "КамчатЭ ап", "ticker": "KCHEP",
                "company_desc": "Энергетическая компания. Поставляет электричество в Камчатской области. Входит в «Русгидро»",
                "company_url": "http://www.kamenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "133", "short_name": "СаратНПЗ", "ticker": "KRKN",
                "company_desc": "Нефтеперерабатывающий завод. Выпускает бензин, дизельное топливо, мазут, техническую серу и другие продукты. Входит в «Роснефть»",
                "company_url": "http://www.saratov-npz.ru/", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "60", "short_name": "КрасОкт-ао", "ticker": "KROT",
                "company_desc": "Производит конфеты, какао, шоколад. Владеет брендами «Аленка», «Мишка косолапый», «Золотой Ярлык». Входит в «Объединенные кондитеры",
                "company_url": "http://www.konfetki.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "66", "short_name": "Лензол. ап", "ticker": "LNZLP",
                "company_desc": "Золотодобывающая компания. Разрабатывает рудники в Иркутской области. Входит в «Полюс»",
                "company_url": "http://www.len-zoloto.ru", "sector_name": "Драгоценности",
                "sector_id": "12"
            },
            {
                "company_id": "53", "short_name": "Квадра-п", "ticker": "TGKDP",
                "company_desc": "Энергетическая компания. Владеет электростанциями в центральной части России",
                "company_url": "www.quadra.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "5", "short_name": "АбрауДюрсо", "ticker": "ABRD",
                "company_desc": "Ведущий производитель игристого вина на российском рынке",
                "company_url": "www.abraudurso.ru/investors", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "10", "short_name": "Аптеки36и6", "ticker": "APTK",
                "company_desc": "Сеть аптек. Владеет брендами «36,6», «Горздрав», «A5» и A.v.e. Работает в Москве, Московской области, Санкт-Петербурге и Ленинградской области",
                "company_url": "pharmacychain366.ru", "sector_name": "Здравоохранение",
                "sector_id": "10"
            },
            {
                "company_id": "39", "short_name": "ДВМП ао", "ticker": "FESH",
                "company_desc": "Частная транспортно-логистическая компания. Перевозит грузы по всему миру. Владеет флотом на Дальнем Востоке",
                "company_url": "www.fesco.ru", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "30", "short_name": "ГАЗКОН-ао", "ticker": "GAZC",
                "company_desc": "Финансовая компания. Инвестирует деньги в депозиты и российские ценные бумаги",
                "company_url": "www.gazcon.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "64", "short_name": "Куйбазот", "ticker": "KAZT",
                "company_desc": "Химическая компания. Производит техническую нить, аммиачную селитру, полиамид",
                "company_url": "www.kuazot.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "67", "short_name": "Ленэнерго", "ticker": "LSNG",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Санкт-Петербурге и Ленинградской области. Входит в «Россети»",
                "company_url": "www.lenenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "106", "short_name": "ПРОТЕК ао", "ticker": "PRTK",
                "company_desc": "Фармацевтическая компания. Одна из крупнейших в России",
                "company_url": "www.protek-group.ru", "sector_name": "Здравоохранение", "sector_id": "10"
            },
            {
                "company_id": "42", "short_name": "Дорогбж ао", "ticker": "DGBZ", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "71", "short_name": "ММК", "ticker": "MAGN",
                "company_desc": "Производитель стали. Добывает и перерабатывает сырье, продает изделия из металла. Больше 70% продукции продает в России",
                "company_url": "www.mmk.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "84", "short_name": "Магнит ао", "ticker": "MGNT",
                "company_desc": "Лидирующая сеть продовольственных магазинов в России",
                "company_url": "http://ir.magnit.com", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "79", "short_name": "МРСК Центр", "ticker": "MRKC",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Центральном федеральном округе. Входит в «Россети»",
                "company_url": "https://www.mrsk-1.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "74", "short_name": "МОЭСК", "ticker": "MSRS",
                "company_desc": "Распределительная электросетевая компания. Передает электричество в Москве и Московской области. Входит в «Россети»",
                "company_url": "www.moesk.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "73", "short_name": "Мостотрест", "ticker": "MSTT",
                "company_desc": "Строительная компания. Строит мосты, автомагистрали и транспортные развязки по всей России",
                "company_url": "ir.mostotrest.ru", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "93", "short_name": "НЛМК ао", "ticker": "NLMK",
                "company_desc": "Металлургическая компания. Добывает, перерабатывает и продает сталь в России, Северной Америке и странах ЕС",
                "company_url": "http://www.nlmk.com", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "113", "short_name": "РБК ао", "ticker": "RBCM",
                "company_desc": "Медиакомпания. Владеет новостным сайтом rbc.ru, одноименными печатными СМИ и телеканалом, а также сайтом знакомств LovePlanet",
                "company_url": "rbcholding.ru", "sector_name": "Медиа", "sector_id": "16"
            },
            {
                "company_id": "117", "short_name": "Росбанк ао", "ticker": "ROSB",
                "company_desc": "Предоставляет все виды банковских услуг. Владеет 550 офисами в 332 городах России, а также в Белоруссии и Швейцарии. Входит в Societe Generale",
                "company_url": "www.rosbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "123", "short_name": "Россети ао", "ticker": "RSTI",
                "company_desc": "Электросетевая компания. Владеет ФСК ЕЭС и МРСК",
                "company_url": "www.rosseti.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "156", "short_name": "Таттел. ао", "ticker": "TTLK",
                "company_desc": "Телекоммуникационная компания. Предоставляет услуги интернета, телефонии, телевидения. Работает в республике Татарстан",
                "company_url": "http://www.tattelecom.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "171", "short_name": "ЧелябЭС ао", "ticker": "CLSB",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Челябинскую область",
                "company_url": "http://www.esbt74.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "173", "short_name": "Электрцинк", "ticker": "ELTZ",
                "company_desc": "Металлургическая компания. Производит цинк, свинец, кадмий, серную кислоту, сплавы на основе меди. Входит в УГМК",
                "company_url": "http://electrozinc.ugmk.com/ru/business/shareholder/",
                "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "140", "short_name": "Слав-ЯНОСп", "ticker": "JNOSP",
                "company_desc": "Перерабатывает нефть: производит бензин, дизельное топливо, авиационный керосин, топливо для реактивных двигателей. Входит в «Славнефть»",
                "company_url": "www.refinery.yaroslavl.ru/", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "66", "short_name": "Лензолото", "ticker": "LNZL",
                "company_desc": "Золотодобывающая компания. Разрабатывает рудники в Иркутской области. Входит в «Полюс»",
                "company_url": "http://www.len-zoloto.ru", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "75", "short_name": "МРСКВол", "ticker": "MRKV",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Приволжском федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsk-volgi.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "59", "short_name": "Красэсб ап", "ticker": "KRSBP",
                "company_desc": "Энергосбытовая компания. Поставляет электричество и обслуживает многоквартирные дома в Красноярском крае. Входит в «Русгидро»",
                "company_url": "http://krsk-sbit.ru/raskritie_informacii", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "81", "short_name": "МРСКЮга ао", "ticker": "MRKY",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Южном федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsk-yuga.ru", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "132", "short_name": "СамарЭн-ап", "ticker": "SAGOP",
                "company_desc": "Энергосбытовая компания. Поставляет электроэнергию в Самарской области",
                "company_url": "www.samaraenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "167", "short_name": "ЧКПЗ ао", "ticker": "CHKZ",
                "company_desc": "Машиностоительная компания. Производит спецтехнику Hartung и колесные диски",
                "company_url": "www.chkpz.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "60", "short_name": "КрасОкт-1п", "ticker": "KROTP",
                "company_desc": "Производит конфеты, какао, шоколад. Владеет брендами «Аленка», «Мишка косолапый», «Золотой Ярлык». Входит в «Объединенные кондитеры",
                "company_url": "http://www.konfetki.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "168", "short_name": "ЧМК ао", "ticker": "CHMK",
                "company_desc": "Металлургическая компания. Выпускает изделия для подшипниковых и трубопрокатных заводов, атомной энергетики, машиностроения. Входит в «Мечел»",
                "company_url": "http://www.chelmk.ru", "sector_name": "Металлургия",
                "sector_id": "7"
            },
            {
                "company_id": "141", "short_name": "СМЗ-ао", "ticker": "MGNZ",
                "company_desc": "Металлургическая компания. Производит магний и его сплавы, литий, титан, соединения ниобия, тантала и циркония",
                "company_url": "www.smw.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "89", "short_name": "+МосЭнерго", "ticker": "MSNG",
                "company_desc": "Энергетическая компания. Обеспечивает электроэнергией и теплом московский регион",
                "company_url": "www.mosenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "98", "short_name": "Нижкамшина", "ticker": "NKSH",
                "company_desc": "Производитель автомобильных шин. Входит в «Татнефть»",
                "company_url": "http://shinakama.tatneft.ru/raskritie-informatsii?lang=ru",
                "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "101", "short_name": "ОГК-2 ао", "ticker": "OGKB",
                "company_desc": "Производит и продает электрическую и тепловую энергии. Владеет федеральными теплоэлектростанциями. Входит в «Газпром»",
                "company_url": "www.ogk2.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "3", "short_name": "АЛРОСА-Нюр", "ticker": "ALNU",
                "company_desc": "Алмазодобывающая компания. Добывает, обрабатывает и продает алмазы и изделия из драгоценных камней. Входит в «Алросу»",
                "company_url": "http://alrosanurba.ru/akcioneram", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "21", "short_name": "Белон ао", "ticker": "BLNG",
                "company_desc": "Угольная компания. Добывает и перерабатывает уголь, производит стройматериалы. Входит в ММК",
                "company_url": "www.belon.ru", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "34", "short_name": "Галс-Девел", "ticker": "HALS",
                "company_desc": "Строительная компания. Строит коммерческую и жилую недвижимость в России. Входит в ВТБ",
                "company_url": "hals-development.ru", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "97", "short_name": "НКНХ ао", "ticker": "NKNC",
                "company_desc": "Нефтехимическая компания. Производит и продает пластик и синтетический каучук. Входит в ТАИФ",
                "company_url": "www.nknh.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "104", "short_name": "ОПИН ао", "ticker": "OPIN",
                "company_desc": "Девелоперская компания. Продает коттеджи, таунхаусы и многоквартирные дома в Москве и Подмосковье",
                "company_url": "www.ingrad.com", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "149", "short_name": "ТНСэнВорон", "ticker": "VRSB",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Воронежской области. Входит в «ТНС Энерго»",
                "company_url": "https://voronezh.tns-e.ru/population/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "58", "short_name": "КраснГЭС", "ticker": "KRSG", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "6", "short_name": "Авангрд-ао", "ticker": "AVAN",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Владеет региональной сетью из 300 офисов в 75 городах России",
                "company_url": "www.avangard.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "51", "short_name": "КалужскСК", "ticker": "KLSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Калужской области",
                "company_url": "www.ksc.kaluga.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "57", "short_name": "КоршГОК ао", "ticker": "KOGK",
                "company_desc": "Горно-обогатительный комбинат. Добывает и обогащает железную руду. Входит в «Мечел»",
                "company_url": "http://www.korgok.com", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "47", "short_name": "КАМАЗ", "ticker": "KMAZ",
                "company_desc": "Автомобильная компания. Производит грузовики, автобусы, тракторы, комбайны и автозапчасти. Входит в «Ростех»",
                "company_url": "www.kamaz.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "77", "short_name": "МРСКСиб", "ticker": "MRKS",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Сибирском федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsk-sib.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "107", "short_name": "ПермьЭнС-п", "ticker": "PMSBP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Пермский край",
                "company_url": "http://permenergosbyt.ru/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "91", "short_name": "Мотовил ао", "ticker": "MOTZ",
                "company_desc": "Металлургическая компания. Производит специальные марки стали, военную технику и нефтепромысловое оборудование",
                "company_url": "http://www.mzperm.ru", "sector_name": "Металлургия",
                "sector_id": "7"
            },
            {
                "company_id": "177", "short_name": "Якутскэн-п", "ticker": "YKENP",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Якутию. Входит в «Русгидро»",
                "company_url": "yakutskenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "169", "short_name": "ЧТПЗ ао", "ticker": "CHEP",
                "company_desc": "Производит и продает трубы для магистрального и внутрипромыслового трубопроводного транспорта. Владеет Челябинским трубопрокатным заводом",
                "company_url": "www.chelpipe.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "65", "short_name": "ЛУКОЙЛ", "ticker": "LKOH",
                "company_desc": "Нефтяная компания. Крупнейшая частная компания в стране",
                "company_url": "www.lukoil.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "85", "short_name": "МегаФон ао", "ticker": "MFON",
                "company_desc": "Одна из трех ведущих российских телекоммуникационных компаний. Владеет Mail.ru",
                "company_url": "http://corp.megafon.ru/", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "68", "short_name": "МГТС-5ао", "ticker": "MGTS",
                "company_desc": "Предоставляет услуги телефонии, мобильной связи, интернета, цифрового ТВ. Работает в Москве и Московской области. Входит в МТС",
                "company_url": "mgts.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "86", "short_name": "Мечел ап", "ticker": "MTLRP",
                "company_desc": "Горнодобывающая и металлургическая компания. Добывает и перерабатывает  уголь и железо. Владеет торговыми портами и транспортными операторами",
                "company_url": "www.mechel.ru", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "94", "short_name": "НМТП ао", "ticker": "NMTP",
                "company_desc": "Новороссийский морской торговый порт. Разгружает и загружает суда. Владеет крупными портами России: Новороссийск, Приморск, Балтийск",
                "company_url": "nmtp.info/ncsp", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "165", "short_name": "ФосАгро ао", "ticker": "PHOR",
                "company_desc": "Химический холдинг. Производит минеральные удобрения",
                "company_url": "www.phosagro.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "110", "short_name": "Полюс", "ticker": "PLZL",
                "company_desc": "Золотодобывающая компания. Крупнейшая в России ",
                "company_url": "www.polyus.com", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "111", "short_name": "Приморье", "ticker": "PRMB",
                "company_desc": "Региональный банк. Предоставляет все виды финансовых услуг. Работает в Приморском и Хабаровских краях",
                "company_url": "http://www.primbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "46", "short_name": "ИркЭнерго", "ticker": "IRGZ",
                "company_desc": "Энергоугольная компания. Владеет гидро- и теплоэлектростанциями в Иркутском и Красноярском регионах. Добывает каменный и бурый уголь",
                "company_url": "www.irkutskenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "76", "short_name": "МРСК СЗ", "ticker": "MRKZ",
                "company_desc": "Распределительная сетевая компания. Северо-Западном федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsksevzap.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "83", "short_name": "МТС-ао", "ticker": "MTSS",
                "company_desc": "Одна из трех ведущих российских телекоммуникационных компаний",
                "company_url": "www.company.mts.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "151", "short_name": "ТНСэнНН ап", "ticker": "NNSBP",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Нижегородской области. Входит в «ТНС Энерго»",
                "company_url": "www.nsk.elektra.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "124", "short_name": "Ростел -ао", "ticker": "RTKM",
                "company_desc": "Телекоммуникационная компания. Контролируется государством",
                "company_url": "http://www.rostelecom.ru/", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "68", "short_name": "МГТС-4ап", "ticker": "MGTSP",
                "company_desc": "Предоставляет услуги телефонии, мобильной связи, интернета, цифрового ТВ. Работает в Москве и Московской области. Входит в МТС",
                "company_url": "mgts.ru", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "120", "short_name": "Распадская", "ticker": "RASP",
                "company_desc": "Угольная компания. Добывает и обрабатывает уголь. Владеет шахтами в Кемеровской области",
                "company_url": "www.raspadskaya.ru", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "121", "short_name": "РОСИНТЕРао", "ticker": "ROST",
                "company_desc": "Сеть ресторанов. Владеет «IL Патио» и «Планета Суши», развивает в России TGI Fridays и Costa Coffee по франшизе",
                "company_url": "www.rosinter.ru", "sector_name": "Сфера услуг", "sector_id": "14"
            },
            {
                "company_id": "132", "short_name": "СамарЭн-ао", "ticker": "SAGO",
                "company_desc": "Энергосбытовая компания. Поставляет электроэнергию в Самарской области",
                "company_url": "www.samaraenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "25", "short_name": "ВСМПО-АВСМ", "ticker": "VSMO",
                "company_desc": "Металлургическая компания. Добывает и перерабатывает титан. Продает продукцию Boeing, Airbus, Rolls-Royce. Входит в «Ростех»",
                "company_url": "www.vsmpo.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "35", "short_name": "ПИК ао", "ticker": "PIKK",
                "company_desc": "Строительная компания. Одна из ведущих в России",
                "company_url": "https://www.pik.ru", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "108", "short_name": "Плазмек", "ticker": "PLSM",
                "company_desc": "Разрабатывает плазменные технологии, производит сварочное оборудование, устанавливает сварные конструкции, собирает модульные здания",
                "company_url": "www.plasmeq.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "122", "short_name": "Роснефть", "ticker": "ROSN",
                "company_desc": "Нефтегазовая компания. Контролируется государством. Владеет «Башнефтью»",
                "company_url": "www.rosneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "33", "short_name": "ГазпРнД ао", "ticker": "RTGZ",
                "company_desc": "Газораспределительная компания. Доставляет газ в республике Калмыкия и Ростовской области. Входит в «Газпром»",
                "company_url": "www.rostovoblgaz.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "134", "short_name": "Сбербанк-п", "ticker": "SBERP",
                "company_desc": "Крупнейший банк в России и СНГ. Основан в 1841 году. Контролируется государством",
                "company_url": "www.sberbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "17", "short_name": "УралСиб ао", "ticker": "USBN",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Владеет сетью из 278 офисов в 46 регионах России",
                "company_url": "www.bankuralsib.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "26", "short_name": "ВХЗ-ао", "ticker": "VLHZ",
                "company_desc": "Химическая компания. Производит ПВХ-пластикаты и стеклопластиковые изделия",
                "company_url": "http://www.vhz-jsc.ru/", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "96", "short_name": "Нефтекамск", "ticker": "NFAZ",
                "company_desc": "Автомобилестроительная компания. Производит спецнадстройки на шасси автомобилей. Входит в «Камаз»",
                "company_url": "www.nefaz.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "43", "short_name": "ЗМЗ-ао", "ticker": "ZMZN",
                "company_desc": "Автомобильная компания. Производит бензиновые и дизельные двигатели, автозапчасти. Входит в «Соллерс»",
                "company_url": "www.zmz.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "37", "short_name": "Разгуляй", "ticker": "GRAZ", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "29", "short_name": "ГАЗ ао", "ticker": "GAZA",
                "company_desc": "Автомобильная компания. Выпускает грузовик «Урал», автобусы ЛиАЗ и ПАЗ. Владеет брендом «Газель»",
                "company_url": "gazgroup.ru/shareholders/pao_gaz/investors",
                "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "105", "short_name": "ПМП ао", "ticker": "PRIM", "company_desc": "",
                "company_url": "", "sector_name": "", "sector_id": ""
            },
            {
                "company_id": "107", "short_name": "ПермьЭнСб", "ticker": "PMSB",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Пермский край",
                "company_url": "http://permenergosbyt.ru/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "134", "short_name": "Сбербанк", "ticker": "SBER",
                "company_desc": "Крупнейший банк в России и СНГ. Основан в 1841 году. Контролируется государством",
                "company_url": "www.sberbank.ru", "sector_name": "Финансы",
                "sector_id": "2"
            },
            {
                "company_id": "137", "short_name": "Селигдар", "ticker": "SELG",
                "company_desc": "Добывает золото в Якутии, Бурятии, Алтайском и Хабаровском краях, Оренбургской области. Добывает и перерабатывает олово. Владеет «Русолово»",
                "company_url": "www.seligdar.ru", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "161", "short_name": "Уркалий-ао", "ticker": "URKA",
                "company_desc": "Крупнейший в мире производитель калийных удобрений",
                "company_url": "www.uralkali.com", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "13", "short_name": "ВТБ ао", "ticker": "VTBR",
                "company_desc": "Вторая по величине банковская группа страны. Контролируется государством",
                "company_url": "www.vtb.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "14", "short_name": "Возрожд-ао", "ticker": "VZRZ",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Работает по всей России",
                "company_url": "www.vbank.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "166", "short_name": "ЦМТ ао", "ticker": "WTCM",
                "company_desc": "Сдает в аренду офисы, предоставляет гостиничные услуги, проводит конгрессы. Владеет комплексом ЦМТ, гостиницами Crowne Plaza и «Союз»",
                "company_url": "www.wtcmoscow.ru", "sector_name": "Сфера услуг", "sector_id": "14"
            },
            {
                "company_id": "176", "short_name": "ЯТЭК ао", "ticker": "YAKG",
                "company_desc": "Газодобывающая компания. Производит бензин, дизельное и моторное топливо, газовый конденсат. Снабжает газом центральные регионы Якутии",
                "company_url": "www.yatec.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "78", "short_name": "МРСК Ур", "ticker": "MRKU",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Уральском федеральном округе. Входит в «Россети»",
                "company_url": "www.mrsk-ural.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "24", "short_name": "ВыбСудЗ ап", "ticker": "VSYDP",
                "company_desc": "Производит гражданские суда: ледоколы, рыболовные траулеры, буровые платформы для разработки морских шельфовых месторождений. Входит в ОСК",
                "company_url": "www.vyborgshipyard.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "102", "short_name": "ОМЗ-ап", "ticker": "OMZZP",
                "company_desc": "Машиностроительная компания. Производит и обслуживает оборудование для атомной энергетики, нефтехимической, нефтегазовой и горной промышленности. Входит в «Газпромбанк»",
                "company_url": "www.omz.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "124", "short_name": "Ростел -ап", "ticker": "RTKMP",
                "company_desc": "Телекоммуникационная компания. Контролируется государством",
                "company_url": "http://www.rostelecom.ru/", "sector_name": "Связь", "sector_id": "13"
            },
            {
                "company_id": "32", "short_name": "Газпрнефть", "ticker": "SIBN",
                "company_desc": "Нефтяная компания. Контролируется государством",
                "company_url": "www.gazprom-neft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "27", "short_name": "Варьеган-п", "ticker": "VJGZP",
                "company_desc": "Нефтегазодобывающая компания. Добывает нефть и газ в ХМАО. Входит в «Роснефть»",
                "company_url": "http://www.varyeganneftegaz.ru/", "sector_name": "Нефть и газ",
                "sector_id": "3"
            }, {
                "company_id": "149", "short_name": "ТНСэнВор-п", "ticker": "VRSBP",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Воронежской области. Входит в «ТНС Энерго»",
                "company_url": "https://voronezh.tns-e.ru/population/",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "126", "short_name": "Русполимет", "ticker": "RUSP",
                "company_desc": "Металлургическая компания. Производит кольца для авиадвигателей. Владеет Кулебакским металлургическим заводом",
                "company_url": "www.ruspolymet.ru/rus/about/documents", "sector_name": "Металлургия",
                "sector_id": "7"
            }, {
                "company_id": "67", "short_name": "Ленэнерг-п", "ticker": "LSNGP",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Санкт-Петербурге и Ленинградской области. Входит в «Россети»",
                "company_url": "www.lenenergo.ru", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "123", "short_name": "Россети ап", "ticker": "RSTIP",
                "company_desc": "Электросетевая компания. Владеет ФСК ЕЭС и МРСК",
                "company_url": "www.rosseti.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "159", "short_name": "Транснф ап", "ticker": "TRNFP",
                "company_desc": "Трубопроводная компания. Транспортирует более 83% добываемой в России нефти",
                "company_url": "www.transneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "103", "short_name": "ОМПК ао", "ticker": "OSMP",
                "company_desc": "Мясоперерабатывающая компания. Владеет брендами «Останкино» и «Папа Может»",
                "company_url": "www.sosiska.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "36", "short_name": "ЛСР ао", "ticker": "LSRG",
                "company_desc": "Строительная компания. Крупнейшая в России", "company_url": "www.lsrgroup.ru",
                "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "155", "short_name": "Татнфт 3ап", "ticker": "TATNP",
                "company_desc": "Нефтяная компания. Головной офис находится в Татарстане",
                "company_url": "www.tatneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "8", "short_name": "ЮТэйр ао", "ticker": "UTAR",
                "company_desc": "Авиакомпания. Перевозит пассажиров и грузы на самолетах и вертолетах. Работает в России и других странах",
                "company_url": "www.utair.ru", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "24", "short_name": "ВыбСудЗ ао", "ticker": "VSYD",
                "company_desc": "Производит гражданские суда: ледоколы, рыболовные траулеры, буровые платформы для разработки морских шельфовых месторождений. Входит в ОСК",
                "company_url": "www.vyborgshipyard.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "18", "short_name": "ОткрФКБ ао", "ticker": "OFCB",
                "company_desc": "Частный банк. Предоставляет все виды финансовых услуг. Работает по всей России. Входит в «Открытие Холдинг»",
                "company_url": "www.open.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "29", "short_name": "ГАЗ ап", "ticker": "GAZAP",
                "company_desc": "Автомобильная компания. Выпускает грузовик «Урал», автобусы ЛиАЗ и ПАЗ. Владеет брендом «Газель»",
                "company_url": "gazgroup.ru/shareholders/pao_gaz/investors",
                "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "38", "short_name": "ЧеркизГ-ао", "ticker": "GCHE",
                "company_desc": "Крупнейший в России производитель мясной продукции и комбикормов",
                "company_url": "www.cherkizovo.com", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "99", "short_name": "ГМКНорНик", "ticker": "GMKN",
                "company_desc": "Крупнейший в мире производитель никеля и палладия",
                "company_url": "www.nornik.ru", "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "140", "short_name": "Славн-ЯНОС", "ticker": "JNOS",
                "company_desc": "Перерабатывает нефть: производит бензин, дизельное топливо, авиационный керосин, топливо для реактивных двигателей. Входит в «Славнефть»",
                "company_url": "www.refinery.yaroslavl.ru/", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "80", "short_name": "МРСК ЦП", "ticker": "MRKP",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Центральном и Приволжском федеральных округах. Входит в «Россети»",
                "company_url": "www.mrsk-cp.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "147", "short_name": "ТГК-2 ап", "ticker": "TGKBP",
                "company_desc": "Производит электричество и тепло. Работает в Архангельской, Вологодской, Костромской, Новгородской и Ярославской областях",
                "company_url": "www.tgc-2.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "178", "short_name": "Yandex clA", "ticker": "YNDX",
                "company_desc": "Технологическая компания. Владеет самой популярной российской поисковой системой «Яндекс»",
                "company_url": "http://company.yandex.com/", "sector_name": "Технологии", "sector_id": "1"
            },
            {
                "company_id": "52", "short_name": "КамчатЭ ао", "ticker": "KCHE",
                "company_desc": "Энергетическая компания. Поставляет электричество в Камчатской области. Входит в «Русгидро»",
                "company_url": "http://www.kamenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "437", "short_name": "РуссНфт ао", "ticker": "RNFT",
                "company_desc": "Нефтегазовая компания. Разрабатывает месторождения, добывает нефть и газ",
                "company_url": "", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "61", "short_name": "ТКЗКК ао", "ticker": "KRKO",
                "company_desc": "Машиностроительная компания. Производит котельное оборудование для электростанций. Входит в «Силовые машины»",
                "company_url": "http://www.tkz.su/", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "162", "short_name": "ФСК ЕЭС ао", "ticker": "FEES",
                "company_desc": "Монопольный оператор крупнейших электросетей для передачи энергии по территории России",
                "company_url": "www.fsk-ees.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "151", "short_name": "ТНСэнНН ао", "ticker": "NNSB",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Нижегородской области. Входит в «ТНС Энерго»",
                "company_url": "www.nsk.elektra.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "424", "short_name": "Сахэнер ао", "ticker": "SLEN",
                "company_desc": "Энергетическая компания. Поставляет электричество в Сахалинской области",
                "company_url": "www.sahen.elektra.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "27", "short_name": "Варьеган", "ticker": "VJGZ",
                "company_desc": "Нефтегазодобывающая компания. Добывает нефть и газ в ХМАО. Входит в «Роснефть»",
                "company_url": "http://www.varyeganneftegaz.ru/", "sector_name": "Нефть и газ",
                "sector_id": "3"
            }, {
                "company_id": "153", "short_name": "ТНСэнЯр-п", "ticker": "YRSBP",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Ярославской области. Входит в «ТНС Энерго»",
                "company_url": "http://www.yrsk.ru/", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "401", "short_name": "ДетскийМир", "ticker": "DSKY",
                "company_desc": "Сеть магазинов товаров для детей. Работает в 180 городах России и Казахстана. Входит в АФК «Система»",
                "company_url": "www.detmir.ru", "sector_name": "Торговля", "sector_id": "15"
            },
            {
                "company_id": "61", "short_name": "ТКЗКК ап", "ticker": "KRKOP",
                "company_desc": "Машиностроительная компания. Производит котельное оборудование для электростанций. Входит в «Силовые машины»",
                "company_url": "http://www.tkz.su/", "sector_name": "Промышленные товары", "sector_id": "8"
            },
            {
                "company_id": "59", "short_name": "Красэсб ао", "ticker": "KRSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество и обслуживает многоквартирные дома в Красноярском крае. Входит в «Русгидро»",
                "company_url": "http://krsk-sbit.ru/raskritie_informacii", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            }, {
                "company_id": "444", "short_name": "МордЭнСб", "ticker": "MRSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Мордовии",
                "company_url": "http://www.mesk.ru/company/",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "41", "short_name": "ДЭК ао", "ticker": "DVEC",
                "company_desc": "Энергетическая компания. Передает электричество в Приморском и Хабаровском краях, Амурской области и Еврейской АО. Входит в «Русгидро»",
                "company_url": "www.dvec.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "127", "short_name": "РусАква ао", "ticker": "AQUA",
                "company_desc": "Промысловая компания. Выращивает форель и лосося в СЗФО для поставок на российский рынок",
                "company_url": "russaquaculture.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "22", "short_name": "БурЗолото", "ticker": "BRZL",
                "company_desc": "Золотодобывающая компания. Владеет двумя рудниками в республике Бурятия. Входит состав Nordgold",
                "company_url": "www.buryatzoloto.ru", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "1", "short_name": "АВТОВАЗ ап", "ticker": "AVAZP",
                "company_desc": "Крупнейший производитель легковых автомобилей в России",
                "company_url": "info.avtovaz.ru", "sector_name": "Потребительские товары", "sector_id": "5"
            },
            {
                "company_id": "20", "short_name": "Башнефт ап", "ticker": "BANEP",
                "company_desc": "Нефтяная компания. Головной офис расположен в Башкирии",
                "company_url": "www.bashneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "463", "short_name": "Инв-Девел", "ticker": "IDVP",
                "company_desc": "Занимается коммерческой недвижимостью. Инвестирует в строительство апартаментов, торговых и офисных центров",
                "company_url": "www.invest-development.ru", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "129", "short_name": "Мегион-ао", "ticker": "MFGS",
                "company_desc": "Нефтегазовая компания. Разрабатывает месторождения, добывает нефть и газ в ХМАО. Ранее называлась «Мегионнефтегаз». Входит в «Славнефть»",
                "company_url": "www.sn-mng.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "137", "short_name": "Селигдар-п", "ticker": "SELGP",
                "company_desc": "Добывает золото в Якутии, Бурятии, Алтайском и Хабаровском краях, Оренбургской области. Добывает и перерабатывает олово. Владеет «Русолово»",
                "company_url": "www.seligdar.ru", "sector_name": "Драгоценности", "sector_id": "12"
            },
            {
                "company_id": "143", "short_name": "Сургнфгз-п", "ticker": "SNGSP",
                "company_desc": "Нефтегазодобывающая компания. Одна из крупнейших в России",
                "company_url": "www.surgutneftegas.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "142", "short_name": "СтаврЭнСб", "ticker": "STSB",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством Ставропольский край",
                "company_url": "http://www.staves.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "130", "short_name": "СОЛЛЕРС", "ticker": "SVAV",
                "company_desc": "Ведущий российский производитель автомобилей",
                "company_url": "www.sollers-auto.com", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "411", "short_name": "ТамбЭнСб-п", "ticker": "TASBP",
                "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Тамбовскую область. Входит в «Интер РАО»",
                "company_url": "www.tesk.su", "sector_name": "Электроэнергетика",
                "sector_id": "4"
            },
            {
                "company_id": "155", "short_name": "Татнфт 3ао", "ticker": "TATN",
                "company_desc": "Нефтяная компания. Головной офис находится в Татарстане",
                "company_url": "www.tatneft.ru", "sector_name": "Нефть и газ", "sector_id": "3"
            },
            {
                "company_id": "145", "short_name": "ТГК-1", "ticker": "TGKA",
                "company_desc": "Производит электричество и тепло. Работает в Северо-западном регионе России",
                "company_url": "www.tgc1.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "147", "short_name": "ТГК-2", "ticker": "TGKB",
                "company_desc": "Производит электричество и тепло. Работает в Архангельской, Вологодской, Костромской, Новгородской и Ярославской областях",
                "company_url": "www.tgc-2.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "53", "short_name": "Квадра", "ticker": "TGKD",
                "company_desc": "Энергетическая компания. Владеет электростанциями в центральной части России",
                "company_url": "www.quadra.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "150", "short_name": "ТНСэнрг ао", "ticker": "TNSE",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество. Владеет 10 энергосбытовыми компаниями, которые обслуживают 11 регионов России",
                "company_url": "https://corp.tns-e.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "406", "short_name": "ТРК ао", "ticker": "TORS",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Томской области. Входит в «Россети»",
                "company_url": "http://www.trk.tom.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "406", "short_name": "ТРК ап", "ticker": "TORSP",
                "company_desc": "Распределительная сетевая компания. Передает электричество в Томской области. Входит в «Россети»",
                "company_url": "http://www.trk.tom.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "157", "short_name": "ТрансК ао", "ticker": "TRCN",
                "company_desc": "Транспортная компания. Перевозит грузы в контейнерах на поездах и автомобилях. Оформляет документы, оптимизирует и отслеживает доставку",
                "company_url": "www.trcont.ru", "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "421", "short_name": "iДонскЗР п", "ticker": "DZRDP",
                "company_desc": "Керамический завод. Производит и выпускает более 160 разных изделий из керамики",
                "company_url": "http;//www.alund.ru", "sector_name": "Потребительские товары",
                "sector_id": "5"
            }, {
                "company_id": "459", "short_name": "БУДУЩЕЕ ао", "ticker": "FTRE",
                "company_desc": "Инвестиционная компания. Управляет негосударственными пенсионными фондами «Будущее», «Образование», «Телеком-Союз», «Уралсиб», «Наше будущее»",
                "company_url": "www.futurefg.ru", "sector_name": "Финансы",
                "sector_id": "2"
            },
            {
                "company_id": "415", "short_name": "ГАЗ-Тек ао", "ticker": "GAZT",
                "company_desc": "Инвестиционная компания. Вкладывает в ценные бумаги",
                "company_url": "www.gaz-tek.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "461", "short_name": "ГИТ ао", "ticker": "GRNT",
                "company_desc": "Многопрофильный холдинг. Оказывает сервисные услуги в сфере ЖКХ:  проектирование, реконструкция, капремонт",
                "company_url": "www.paogit.ru", "sector_name": "Строительство", "sector_id": "11"
            },
            {
                "company_id": "125", "short_name": "РусГидро", "ticker": "HYDR",
                "company_desc": "Энергетическая компания. Владееет большинством гидроэлектростанций страны",
                "company_url": "www.rushydro.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "435", "short_name": "Ижсталь2ао", "ticker": "IGST",
                "company_desc": "Металлургическая компания. Из металлопродукции предприятия изготавливают детали машин, подшипников, буровых установок",
                "company_url": "http://www.mechel.ru/shareholders/disclosure/filials/metallurgy/izhstal/",
                "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "97", "short_name": "НКНХ ап", "ticker": "NKNCP",
                "company_desc": "Нефтехимическая компания. Производит и продает пластик и синтетический каучук. Входит в ТАИФ",
                "company_url": "www.nknh.ru", "sector_name": "Химия и агрохимия", "sector_id": "6"
            },
            {
                "company_id": "119", "short_name": "ИКРУСС-ИНВ", "ticker": "RUSI",
                "company_desc": "Инвестиционная компания. Занимается брокерским и депозитарным обслуживанием, доверительным управлением, венчурными проектами",
                "company_url": "www.russ-invest.com", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "146", "short_name": "ТГК-14", "ticker": "TGKN",
                "company_desc": "Производит электричество и тепло. Работает в Бурятии и Забайкальском крае",
                "company_url": "www.tgk-14.com", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "56", "short_name": "ИРКУТ-3", "ticker": "IRKT",
                "company_desc": "Авиастроительная компания. Проектирует, испытывает, производит, продает и обслуживает авиатехнику военного и гражданского назначения",
                "company_url": "http://www.irkut.com/ru/", "sector_name": "Промышленные товары",
                "sector_id": "8"
            }, {
                "company_id": "148", "short_name": "ТМК ао", "ticker": "TRMK",
                "company_desc": "Металлургическая компания. Производит и продает стальные трубы",
                "company_url": "www.tmk-group.ru", "sector_name": "Металлургия",
                "sector_id": "7"
            },
            {
                "company_id": "175", "short_name": "ЮжКузб. ао", "ticker": "UKUZ",
                "company_desc": "Горнодобывающая компания. Добывает, обогащает и продает каменный уголь. Входит в «Мечел»",
                "company_url": "http://www.ukuzbass.ru", "sector_name": "Уголь", "sector_id": "18"
            },
            {
                "company_id": "54", "short_name": "ЮУНК ао", "ticker": "UNKL",
                "company_desc": "Металлургический компания. Добывала и производила никель. Не работает с 2012 года. Входит в «Мечел»",
                "company_url": "http://www.mechel.ru/shareholders/disclosure/filials/ferroalloys/yunk/",
                "sector_name": "Металлургия", "sector_id": "7"
            },
            {
                "company_id": "482", "short_name": "ВЭК 01 ао", "ticker": "VDSB",
                "company_desc": "Энергосбытовая компания. Передает электричество во Владимирской области",
                "company_url": "www.ves-k.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "427", "short_name": "ВолгЭнСб", "ticker": "VGSB",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Волгоградской области",
                "company_url": "http://www.energosale34.ru/Pages/default.aspx",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "4", "short_name": "Система ао", "ticker": "AFKS",
                "company_desc": "Финансово-промышленная группа. Владеет МТС, «Детским миром» и другими крупными компаниями в различных отраслях экономики",
                "company_url": "www.sistema.ru", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "12", "short_name": "Аэрофлот", "ticker": "AFLT",
                "company_desc": "Крупнейший авиаперевозчик России", "company_url": "www.aeroflot.ru",
                "sector_name": "Транспорт", "sector_id": "9"
            },
            {
                "company_id": "405", "short_name": "РН-ЗапСиб", "ticker": "CHGZ",
                "company_desc": "Покупает и продает недвижимость. До 2017 года называлась «Черногорнефть»",
                "company_url": "http://www.chernogorneft.ru/", "sector_name": "Финансы", "sector_id": "2"
            },
            {
                "company_id": "427", "short_name": "ВолгЭнСб-п", "ticker": "VGSBP",
                "company_desc": "Энергосбытовая компания. Поставляет электричество в Волгоградской области",
                "company_url": "http://www.energosale34.ru/Pages/default.aspx",
                "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "166", "short_name": "ЦМТ ап", "ticker": "WTCMP",
                "company_desc": "Сдает в аренду офисы, предоставляет гостиничные услуги, проводит конгрессы. Владеет комплексом ЦМТ, гостиницами Crowne Plaza и «Союз»",
                "company_url": "www.wtcmoscow.ru", "sector_name": "Сфера услуг", "sector_id": "14"
            },
            {
                "company_id": "177", "short_name": "Якутскэнрг", "ticker": "YKEN",
                "company_desc": "Энергетическая компания. Обеспечивает электричеством и теплом Якутию. Входит в «Русгидро»",
                "company_url": "yakutskenergo.ru", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "153", "short_name": "ТНСэнЯр", "ticker": "YRSB",
                "company_desc": "Энергоснабжающая компания. Поставляет электричество в Ярославской области. Входит в «ТНС Энерго»",
                "company_url": "http://www.yrsk.ru/", "sector_name": "Электроэнергетика", "sector_id": "4"
            },
            {
                "company_id": "43", "short_name": "ЗМЗ-ап", "ticker": "ZMZNP",
                "company_desc": "Автомобильная компания. Производит бензиновые и дизельные двигатели, автозапчасти. Входит в «Соллерс»",
                "company_url": "www.zmz.ru", "sector_name": "Промышленные товары", "sector_id": "8"
            }];
    }
}