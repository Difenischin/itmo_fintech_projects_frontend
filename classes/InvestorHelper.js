const DataFrame = require('dataframe-js');
export default class InvestorHelper {
    static getMultiplicatorsDf(historyData) {
        const df = new DataFrame.DataFrame(historyData.data, historyData.columns);
        return df;
    }
    static getMultiplicatorsDfBt(historyData, columns) {
        const df = new DataFrame.DataFrame(historyData, columns);
        return df;
    }

    static getNormalizeMultiplicators(multiplicatorDf, columnNames, prefixColumnName) {
        /*
        * найти в нужных столбцах максимумы и минимумы
        * каждый столбец отнормировать
        * вывести результат
        *
        * задать критериям веса, чтобы в сумме была 1
        * сложить значения в столбцах по этим критериям
        * */
        let resultDf = multiplicatorDf;
        for (const name of columnNames) {
            const max = multiplicatorDf.stat.max(name);
            const min = multiplicatorDf.stat.min(name);
            const difference = max - min;
            const normalize = prefixColumnName + name;
            resultDf = resultDf.withColumn(normalize, row => (row.get(name) - min) / difference);
        }
        return resultDf;
    }

    static getDFWithMulticriterion(multiplicatorDf, columnNames, newColumnName) {
        let resultDf = multiplicatorDf.withColumn(newColumnName, () => 0);
        for (const {name, weight} of columnNames) {
            resultDf = resultDf.withColumn(newColumnName,
                row =>
                    row.get(newColumnName) + row.get(name) * weight);
        }
        return resultDf;
    }
}

//  "jest": {
//    "verbose": true,
//    "testURL": "http://localhost/",
//    "testEnvironment": "node"
//  },