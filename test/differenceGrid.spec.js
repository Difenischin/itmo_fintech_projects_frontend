describe('Index page', () => {
        /*
        * 4.99     -0.96171290292679334
    5.00     -0.95892427466313845
    5.01     -0.95603975377111805*/
        const data = [
            {
                x: 4.99, f: -0.96171290292679334
            },
            {
                x: 5, f: -0.95892427466313845
            },
            {
                x: 5.01, f: -0.95603975377111805
            }
        ];
        const accuracy = 5;

        it('Вторая производная', () => {
            const a = data[2].f - 2 * data[1].f + data[0].f;
            const denominator = 0.01 * 0.01;
            expect(a / denominator).toBeCloseTo(0.8, accuracy);
        });

        function getDifferenceScheme(point1, point2) {
            const difference = point2.f - point1.f;
            const step = Math.abs(point2.x - point1.x);
            return difference / step;
        }

        it('Левосторонняя схема', () => {
            expect(getDifferenceScheme({
                x: 4.99, f: -0.9617129
            }, {
                x: 5, f: -0.9589243
            })).toBeCloseTo(0.27886, accuracy);
        });
        it('Правосторонняя схема', () => {
            expect(getDifferenceScheme({
                x: 5, f: -0.9589243
            }, {
                x: 5.01, f: -0.9560398
            })).toBeCloseTo(0.28845, accuracy);
        });
        it('Комплексная схема', () => {
            expect(getDifferenceScheme({
                x: 4.99, f: -0.9617129
            }, {
                x: 5.01, f: -0.9560398
            })).toBeCloseTo(0.283655, accuracy);
        });
        it('Первая производная', () => {
            const trueResult = [{
                "complex": "",
                "f": -0.9617129,
                "left": "",
                "right": 0.27886000000000893,
                "x": 4.99
            }, {
                "complex": 0.2836550000000054,
                "f": -0.9589243,
                "left": 0.27886000000000893,
                "right": 0.2884500000000018,
                "x": 5
            }, {
                "complex": "", "f": -0.9560398, "left": 0.2884500000000018, "right": "", "x": 5.01
            }];
            expect(getFirstDifferenceScheme(data)).toEqual(trueResult);
        });

        it('Данные для второй производной', () => {
            const trueResult = [{
                "f": 0.27886000000000893,
                "x": 4.99
            }, {
                "f": 0.2836550000000054,
                "x": 5
            }, {
                "f": 0.2884500000000018,
                "x": 5.01
            }];
            expect(getTwoDifferenceScheme(data)).toEqual(trueResult);
        });

        it('Третья производная', () => {
            const trueResult = [{
                "f": 0.27886000000000893,
                "x": 4.99
            }, {
                "f": 0.2836550000000054,
                "x": 5
            }, {
                "f": 0.2884500000000018,
                "x": 5.01
            }];
            expect(getFirstDifferenceScheme(getTwoDifferenceScheme(data))).toEqual(trueResult);
        });

        function getTwoDifferenceScheme(data) {
            const firstDifferenceScheme = getFirstDifferenceScheme(data);
            const result = [];
            for (const element of firstDifferenceScheme) {
                result.push(getElement(element));
            }
            return result;

            function getElement({x, left, right, complex}) {
                if (complex !== '') {
                    return {x, f: complex};
                }
                if (right !== '') {
                    return {x, f: right};
                }
                if (left !== '') {
                    return {x, f: left};
                }
            }
        }

        function getFirstDifferenceScheme(data) {
            const result = [];
            const first = {
                x: data[0].x,
                f: data[0].f,
                left: '',
                right: getDifferenceScheme(data[0], data[1]),
                complex: ''
            };
            result.push(first);
            for (let i = 1; i <= data.length - 2; i++) {
                let step = {
                    x: data[i].x,
                    f: data[i].f,
                    left: getDifferenceScheme(data[i - 1], data[i]),
                    right: getDifferenceScheme(data[i], data[i + 1]),
                    complex: getDifferenceScheme(data[i - 1], data[i + 1])
                };
                result.push(step);
            }

            const last = {
                x: data[data.length - 1].x,
                f: data[data.length - 1].f,
                left: getDifferenceScheme(data[data.length - 2], data[data.length - 1]),
                right: '',
                complex: ''
            };
            result.push(last);
            return result;
        }


        it('Тестирование определения колличества необходимых итераций', () => {

            expect(getCountStepBySustainability(0.3, 1, 21)).toBeCloseTo(9.88515520, accuracy);

            function getCountStepBySustainability(sigma, time, countPriceStep) {
                return sigma ** 2 * time * countPriceStep ** 2;
            }
        });
        it('Тестирование определения шага по времени', () => {

            expect(getDeltaTime(38, 1)).toBeCloseTo(9.88515520, accuracy);

            function getDeltaTime(countStepBySustainability, time) {
                return time / countStepBySustainability;
            }
        });

        it('Тестирвание вычисления узла по трём точкам', () => {
            const forTest = {
                strikePrice: 21,
                spotPrice: 21,
                rate: 0.04,
                time: 2,
                sigma: 0.1,
                countPriceStep: 10
            };
            const matrix = {mDown: 0, mMiddle: 0, mUp: 2.333333};
            const stepIndex = 2;
            const priceIndex = 5;
            const countStepBySustainability = 3;
            const a = getPriceForFiniteDifferenceGrid(matrix, stepIndex, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(0.3409091, accuracy);
        });

        it('Тестирвание вычисления узла по трём точкам 2', () => {
            const forTest = {
                strikePrice: 21,
                spotPrice: 21,
                rate: 0.04,
                time: 2,
                sigma: 0.1,
                countPriceStep: 10
            };
            const matrix = {mDown: 11.66667, mMiddle: 16.33333, mUp: 21.55260};
            const stepIndex = 2;
            const priceIndex = 9;
            const countStepBySustainability = 3;
            const a = getPriceForFiniteDifferenceGrid(matrix, stepIndex, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(17.20992, accuracy);

        });

        function getPriceForFiniteDifferenceGrid({mDown, mMiddle, mUp}, stepIndex, priceIndex, countStepBySustainability, {strikePrice, spotPrice, rate, time, sigma}) {
            const dawn = mDown * getPDawn(priceIndex);
            const middle = mMiddle * getPMiddle(priceIndex);
            const up = mUp * getPUp(priceIndex);
            const denominator = 1 + rate * getDeltaTime(countStepBySustainability);
            const value = (dawn + middle + up) / denominator;
            return value;

            function getPDawn(i) {
                const dawn = (sigma * sigma) * (i * i) - rate * i;
                return (dawn * getDeltaTime(countStepBySustainability)) / 2;
            }

            function getDeltaTime(countStepBySustainability) {
                return time / countStepBySustainability;
            }

            function getPMiddle(i) {
                const m = (sigma * sigma) * (i * i);
                return 1 - (m * getDeltaTime(countStepBySustainability));
            }

            function getPUp(i) {
                const up = (sigma * sigma) * (i * i) + rate * i;
                return (up * getDeltaTime(countStepBySustainability)) / 2;
            }
        }

        it('Европейский колл', () => {
            const forTest = {
                strikePrice: 10,
                spotPrice: 11,
                rate: 0.08,
                time: 1,
                sigma: 0.3,
                countPriceStep: 20
            };
            const matrix = {mDown: 0, mMiddle: 0, mUp: 1.1};
            const stepIndex = 20;
            const priceIndex = 11;
            const countStepBySustainability = 39;
            const a = getPriceForFiniteDifferenceGrid(matrix, stepIndex, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(0.05195682, accuracy);

            function getCall(spotPrice = 21, X = 21, T = 2, r = 0.04, sigma = 0.1, I = 400) {
                /* const K = Math.round(sigma ** 2 * I ** 2 * T) + 1;
                 const dt = T / K;
                 const V = getStartMatrix(K * I, K);

                 const maxPriseBorder = matrix.length - 1;
                 for (let step = matrix[0].length - 1; step >= 0; step--) {
                     let expPart = Math.exp(-rate * dt * step);
                     let revers = matrix[0].length - 1 - step;
                     matrix[maxPriseBorder][revers] = spotPrice * 2 - X * expPart;
                 }

                 const expirationStep = matrix[0].length - 1;
                 for (let priceIndex = 1; priceIndex < matrix.length - 1; priceIndex++) {
                     matrix[priceIndex][expirationStep] = getPayoffCall();
                 }
                 # итоговые условия для цены
                 V[K,] = sapply(seq(0, 2*X, length.out = I) - X, function(x) max(x, 0))
                 # граничные условия
                 V[,1] = 0
                 V[,I] = rev(2*X - X * exp(-r * dt * (1:K)))
                 # Идем обратно
                 for (k in (K-1):1)
                 for (i in 2:(I-1)) {
                     pu = (sigma**2 * i**2 + r* i) * dt / 2
                     pm = 1 - sigma**2 * i**2 *dt
                     pd = (sigma**2 * i**2 - r*i) * dt / 2
                     V[k, i] = (pu * V[k+1, i+1] + pd * V[k+1, i-1] + pm * V[k+1, i])/(1 + r*dt)}
                 # возвращаем результат
                 (V[1, as.integer(spotPrice*I/2/X)] + V[1, round(spotPrice*I/2/X) + 1])/2

                 function getPayoffCall(numberStepPrice) {
                     const strikePriceForStep = getPriceForStep(numberStepPrice);
                     const profit = spotPrice - strikePriceForStep;
                     return Math.max(profit, 0);
                 }

                 function getDeltaTime(countStepBySustainability) {
                     return time / countStepBySustainability;
                 }
                 function getStartMatrix(stepIndexMax, priceIndexMax) {
                     let matrix = [];
                     for (let priceIndex = 0; priceIndex < priceIndexMax; priceIndex++) {
                         let priceVector = [];
                         for (let step = 0; step < stepIndexMax; step++) {
                             priceVector.push(0);
                         }
                         matrix.push(priceVector);
                     }
                     return matrix;
                 }*/
            }

            function getPriceOptionsCallDifferenceGrid(countPriceStep, {strikePrice, spotPrice, rate, time, sigma}) {
                const matrixCall = getMatrixPayoffEuropeCall();
                const priceEuropeCall = getOptionPrice(matrixCall);
                const matrixPut = getMatrixPayoffEuropePut();
                const priceEuropePut = getOptionPrice(matrixPut);
                const matrixAmericanPut = getMatrixPayoffAmericanPut();
                const priceAmericanPut = getOptionPrice(matrixAmericanPut);
                console.log('priceEuropeCall ' + priceEuropeCall);
                console.log('priceEuropePut ' + priceEuropePut);
                console.log('priceAmericanPut ' + priceAmericanPut);
                return {
                    priceEuropeCall,
                    priceAmericanPut,
                    priceEuropePut
                };

                function getOptionPrice(grid) {
                    const lastStep = grid[0].length - 1;
                    const middleIndex = Math.floor(countPriceStep / 2);
                    return grid[middleIndex][lastStep];
                }

                function getMatrixPayoffEuropeCall() {
                    const countStep = getCountStepBySustainability();
                    let matrix = getMatrixWithBirderCall(countStep);
                    for (let step = 1; step < countStep; step++) {
                        for (let priceIndex = 1; priceIndex < countPriceStep - 1; priceIndex++) {
                            matrix[priceIndex][step] = getPriceForFiniteDifferenceGrid(matrix, step - 1, priceIndex, countStep);
                        }
                    }
                    return matrix;

                    function getMatrixWithBirderCall(countStepBySustainability) {
                        let matrix = getStartMatrix(countStepBySustainability, countPriceStep);

                        const expirationStep = matrix[0].length - 1;
                        for (let priceIndex = 1; priceIndex < matrix.length - 1; priceIndex++) {
                            matrix[priceIndex][expirationStep] = getPayoffCall(matrix.length - priceIndex);
                        }

                        const maxPrice = spotPrice * 2;
                        const maxPriseBorder = matrix.length - 1;
                        for (let step = 0; step < matrix[0].length; step++) {
                            let revert = matrix[0].length - step;
                            let expPart = Math.exp(-rate * getDeltaTime(countStepBySustainability) * revert);
                            matrix[maxPriseBorder][step] = maxPrice - spotPrice * expPart;
                        }

                        const minPriceBorder = 0;
                        for (let step = 0; step < matrix[0].length; step++) {
                            matrix[minPriceBorder][step] = 0;
                        }
                        return matrix;
                    }

                    function getPayoffCall(numberStepPrice) {
                        const strikePriceForStep = getPriceForStep(numberStepPrice);
                        const profit = spotPrice - strikePriceForStep;
                        return Math.max(profit, 0);
                    }

                    function getDeltaTime(countStepBySustainability) {
                        return time / countStepBySustainability;
                    }
                }

                function getCountStepBySustainability() {
                    return Math.round(sigma ** 2 * time * countPriceStep ** 2) + 1;
                }

                function getStartMatrix(stepIndexMax, priceIndexMax) {
                    let matrix = [];
                    for (let priceIndex = 0; priceIndex < priceIndexMax; priceIndex++) {
                        let priceVector = [];
                        for (let step = 0; step < stepIndexMax; step++) {
                            priceVector.push(0);
                        }
                        matrix.push(priceVector);
                    }
                    return matrix;
                }

                function getPriceForFiniteDifferenceGrid(matrix, stepIndex, priceIndex, countStepBySustainability) {
                    const dawn = matrix[priceIndex - 1][stepIndex] * getPDawn(priceIndex);
                    const middle = matrix[priceIndex][stepIndex] * getPMiddle(priceIndex);
                    const up = matrix[priceIndex + 1][stepIndex] * getPUp(priceIndex);
                    const denominator = 1 + rate * getDeltaTime(countStepBySustainability);
                    const value = (dawn + middle + up) / denominator;
                    return value;

                    function getPDawn(numberStepPrice) {
                        const dawn = (sigma ** 2) * (numberStepPrice ** 2) - rate * numberStepPrice;
                        return (dawn * getDeltaTime(countStepBySustainability)) / 2;
                    }

                    function getDeltaTime(countStepBySustainability) {
                        return time / countStepBySustainability;
                    }

                    function getPMiddle(numberStepPrice) {
                        const m = (sigma ** 2) * (numberStepPrice ** 2);
                        return 1 - (m * getDeltaTime(countStepBySustainability));
                    }

                    function getPUp(numberStepPrice) {
                        const up = (sigma ** 2) * (numberStepPrice ** 2) + rate * numberStepPrice;
                        return (up * getDeltaTime(countStepBySustainability)) / 2;
                    }
                }

                function getPriceForStep(numberStepPrice) {
                    const minPrice = 0;
                    return minPrice + numberStepPrice * getDeltaPrice();

                    function getDeltaPrice() {
                        return (spotPrice * 2) / countPriceStep;
                    }
                }

                function getMatrixPayoffEuropePut() {
                    const countStep = getCountStepBySustainability();
                    let matrix = getMatrixWithBorderPut(countStep);
                    for (let step = 1; step < countStep; step++) {
                        for (let priceIndex = 1; priceIndex < countPriceStep - 1; priceIndex++) {
                            matrix[priceIndex][step] = getPriceForFiniteDifferenceGrid(matrix, step - 1, priceIndex, countStep);
                        }
                    }
                    return matrix;
                }

                function getMatrixWithBorderPut(countStepBySustainability) {
                    let matrix = getStartMatrix(countStepBySustainability, countPriceStep);
                    const maxPrice = spotPrice * 2;
                    const maxPriseBorder = matrix.length - 1;

                    const expirationStep = 0;
                    for (let priceIndex = 1; priceIndex < matrix.length - 1; priceIndex++) {
                        matrix[priceIndex][expirationStep] = getPayoffPut(priceIndex);
                    }

                    for (let step = 0; step < matrix[0].length; step++) {
                        matrix[maxPriseBorder][step] = 0;
                    }

                    const minPriceBorder = 0;
                    for (let step = 0; step < matrix[0].length; step++) {
                        matrix[minPriceBorder][step] = maxPrice - spotPrice;
                    }
                    return matrix;
                }

                function getPayoffPut(numberStepPrice) {
                    const strikePriceForStep = getPriceForStep(numberStepPrice);
                    const profit = strikePriceForStep - spotPrice;
                    return Math.max(profit, 0);
                }

                function getMatrixPayoffAmericanPut() {
                    const countStep = getCountStepBySustainability();
                    let matrix = getMatrixWithBorderPut(countStep);
                    for (let step = 1; step < countStep; step++) {
                        for (let priceIndex = 1; priceIndex < countPriceStep - 1; priceIndex++) {
                            const priceGrid = getPriceForFiniteDifferenceGrid(matrix, step - 1, priceIndex, countStep);
                            const payoffPut = getPayoffPut(priceIndex);
                            matrix[priceIndex][step] = Math.max(priceGrid, payoffPut);
                        }
                    }
                    return matrix;
                }
            }


            function getPriceForFiniteDifferenceGrid({mDown, mMiddle, mUp}, stepIndex, priceIndex, countStepBySustainability, {strikePrice, spotPrice, rate, time, sigma}) {
                const dawn = mDown * getPDawn(priceIndex);
                const middle = mMiddle * getPMiddle(priceIndex);
                const up = mUp * getPUp(priceIndex);
                const denominator = 1 + rate * getDeltaTime(countStepBySustainability);
                const value = (dawn + middle + up) / denominator;
                return value;

                function getPDawn(i) {
                    const dawn = (sigma * sigma) * (i * i) - rate * i;
                    return (dawn * getDeltaTime(countStepBySustainability)) / 2;
                }

                function getDeltaTime(countStepBySustainability) {
                    return time / countStepBySustainability;
                }

                function getPMiddle(i) {
                    const m = (sigma * sigma) * (i * i);
                    return 1 - (m * getDeltaTime(countStepBySustainability));
                }

                function getPUp(i) {
                    const up = (sigma * sigma) * (i * i) + rate * i;
                    return (up * getDeltaTime(countStepBySustainability)) / 2;
                }
            }
        });

    }
);