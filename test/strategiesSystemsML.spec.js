import AjaxHelper from "../classes/AjaxHelper";

describe('Check Ml Servers Filter', () => {
    const axios = require("axios");

    const accuracy = 5;


    it('Проверка ответа сервера на конкретную дату', async () => {

        const response = await axios({
            method: 'get',
            url: AjaxHelper.getUrlMLFilterNegativeDays('10.04.2019'),
        });
        expect(response.data).toEqual('negative');
    });

    it('Вернуть крайние 8 дней', async () => {

        const response = await axios({
            method: 'get',
            url: AjaxHelper.getUrlMLFilterNegativeDaysLast(8),
        });
        expect(response.data).toEqual('negative');
    });


});
