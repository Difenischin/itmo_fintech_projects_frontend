describe('Index page', () => {
        const iqr = require('compute-iqr');
        const math = require('mathjs');
        const accuracy = 5;


        it('Статистика', () => {
            const result = {
                "avg": 5,
                "lowerBoard": 4.970000000000001,
                "skew": 0,
                "stdev": 0.009999999999999787,
                "upperBoard": 5.029999999999999
            };
            expect(getStatistic([4.99, 5, 5.01])).toEqual(result, accuracy);
        });


        it('Колличество анамалий', () => {
            expect(getCountAnomaly(0.1, 10)).toBeCloseTo(1, accuracy);
            expect(getCountAnomaly(0.1, 9)).toBeCloseTo(1, accuracy);
        });

        it('Вставка анамалий', () => {
            const result = {
                anomalyNumbers: [2],
                prices: [4.99, 5, 5.029999999999999]
            };
            const timeserials = [4.99, 5, 5.01];
            const insert = getInsertAnomally(timeserials, getStatistic(timeserials), getCountAnomaly(0.3, timeserials.length));
            expect(insert).toEqual(result, accuracy);
        });

        it('Расчёт центройда', () => {
            expect(getCentroid([[4.99], [4.99], [4.99]]))
                .toEqual([4.99], accuracy);
        });

        it('Расчёт центройда 2', () => {
            const company1 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company2 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company3 = {
                prices: [4.98, 4.99, 5],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const companys = [company1, company2, company3];
            const matrix = getMatrixPrices(companys);
            expect(getCentroid(matrix)).toEqual([4.986666666666667, 4.996666666666667, 5.006666666666667], accuracy);
        });

        it('Матрица цен', () => {
            const result = [
                [4.99, 5, 5.01],
                [4.99, 5, 5.01],
                [4.98, 4.99, 5]
            ];
            const company1 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company2 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company3 = {
                prices: [4.98, 4.99, 5],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const companys = [company1, company2, company3];
            expect(getMatrixPrices(companys)).toEqual(result, accuracy);
        });


        function getStatistic(timeserials) {
            const average = arr => arr.reduce((p, c) => p + c, 0) / arr.length;
            const skew = skewness(timeserials);
            const stdev = math.std(timeserials);
            const avg = average(timeserials);
            let result = {
                skew,
                stdev,
                avg
            };
            if (skew === 0) {
                result.upperBoard = avg + (3 * stdev);
                result.lowerBoard = avg - (3 * stdev);
            } else {
                result.q1 = math.quantileSeq(timeserials, 0.25);
                result.q3 = math.quantileSeq(timeserials, 0.75);
                result.irq = iqr(timeserials);
                result.upperBoard = avg + (result.q3 + (3 * result.irq));
                result.lowerBoard = avg - (result.q1 - (3 * result.irq));
            }
            return result;


            function skewness(arr) {
                if (!Array.isArray(arr)) {
                    throw new TypeError('skewness()::invalid input argument. Must provide an array.');
                }
                var len = arr.length,
                    delta = 0,
                    delta_n = 0,
                    term1 = 0,
                    N = 0,
                    mean = 0,
                    M2 = 0,
                    M3 = 0,
                    g;

                for (var i = 0; i < len; i++) {
                    N += 1;

                    delta = arr[i] - mean;
                    delta_n = delta / N;

                    term1 = delta * delta_n * (N - 1);

                    M3 += term1 * delta_n * (N - 2) - 3 * delta_n * M2;
                    M2 += term1;
                    mean += delta_n;
                }
                // Calculate the population skewness:
                g = Math.sqrt(N) * M3 / Math.pow(M2, 3 / 2);

                // Return the corrected sample skewness:
                return Math.sqrt(N * (N - 1)) * g / (N - 2);
            } // end FUNCTION skewness()
        }

        function getMatrixPrices(companys) {
            let matrix = [];
            for (const company of companys) {
                matrix.push(company.prices);
            }
            return matrix;
        }

        function getCentroid(matrixPrices) {
            let centroid = [];
            for (let col = 0; col < matrixPrices[0].length; col++) {
                let sum = 0;
                for (let row = 0; row < matrixPrices.length; row++) {
                    let cell = matrixPrices[row][col];
                    sum = sum + cell;
                }
                centroid.push(sum / matrixPrices.length);
            }
            return centroid;
        }

        function getCountAnomaly(percent, countElements) {
            //const a = percent / 100;
            const result = Math.round(percent * countElements);
            return result;
        }

        function getInsertAnomally(timeserials, {upperBoard, lowerBoard}, countAnomaly) {
            function getRandomArbitrary(min, max) {
                return Math.random() * (max - min) + min;
            }

            let anomalyNumbers = [];
            let copyPrices = timeserials.slice();

            for (let i = 0; i < countAnomaly; i++) {
                let numberInsert = Math.floor(getRandomArbitrary(0, timeserials.length));
                if (Math.random() < 0.5) {
                    copyPrices[numberInsert] = lowerBoard;
                } else {
                    copyPrices[numberInsert] = upperBoard;
                }
                anomalyNumbers.push(
                    numberInsert
                );
            }
            return {
                prices: copyPrices,
                anomalyNumbers
            };
        }

        it('Конвертер форматов', () => {
            const result = [{
                prices: [45.99, 46.07],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'ММК'
            }, {
                prices: [329.0, 319.0],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'ENPL-гдр'
            }];
            const dataset = [{
                "index": 0,
                "ММК": 45.99,
                "ENPL-гдр": 329.0,
                "СевСт-ао": 1017.2,
                "НЛМК ао": 161.12,
                "ТМК ао": 71.6,
                "RUSAL plc": 25.72,
                "ГМКНорНик": 10819
            }, {
                "index": 1,
                "ММК": 46.07,
                "ENPL-гдр": 319.0,
                "СевСт-ао": 1002.0,
                "НЛМК ао": 160.16,
                "ТМК ао": 71.7,
                "RUSAL plc": 25.37,
                "ГМКНорНик": 10682
            }];
            expect(getConvertedCompanys(["ММК", "ENPL-гдр"], dataset)).toEqual(result, accuracy);
        });

        function getConvertedCompanys(companyNames, dataSet) {
            let result = [];
            for (const companyName of companyNames) {
                let company = {
                    name: companyName,
                    prices: [],
                    timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                }
                for (const set of dataSet) {
                    company.prices.push(set[companyName]);
                }
                result.push(company);
            }
            return result;
        }

        it('Обнаружение аномалий', () => {
            const result = [{
                companyName: '',
                anomalyPricesNumbers: [],
                errors: [],
                predictPrice: [],
                expectPrice: []
            }
            ];
            const company1 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company2 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company3 = {
                prices: [4.98, 4.99, 5],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const companys = [company1, company2, company3];
            const centroid = getCentroid(getMatrixPrices(companys));
            const anomaly = getInsertAnomally(companys[0].prices, getStatistic(companys[0].prices), 1);
            companys[0].prices = anomaly.prices;
            expect(Anomaly.predictAnomaly(companys, centroid)).toEqual(result, accuracy);
        });

        it('Нижняя граница порога анамалий', () => {
            const result = [{
                0: 5,
                1: 5,
                2: 5
            }
            ];
            const company1 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company2 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company3 = {
                prices: [4.98, 4.99, 5],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const companys = [company1, company2, company3];
            const centroid = getCentroid(getMatrixPrices(companys));
            expect(Anomaly.getMinBorderAnomaly(centroid, math.std(centroid))).toEqual(result, accuracy);
        });

        it('Обнаружение аномалий там где их нету', () => {
            const result = [{
                companyName: '',
                anomalyPricesNumbers: [],
                errors: [],
                predictPrice: [],
                expectPrice: []
            }
            ];
            const company1 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company2 = {
                prices: [4.99, 5, 5.01],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const company3 = {
                prices: [4.98, 4.99, 5],
                timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                name: 'apple'
            }
            const companys = [company1, company2, company3];
            const centroid = getCentroid(getMatrixPrices(companys));
            expect(Anomaly.predictAnomalyDifferenceCentroid(companys, centroid)).toEqual(result, accuracy);
        });

        it('Обнаружение аномалий основной', () => {

            expect(getPredictAnomaly()).toEqual([{
                    "anomalyPricesNumbers": [3, 7, 13, 19, 36, 37, 38, 45, 48, 50, 51, 52, 56, 58, 59, 60, 61, 65, 68, 69, 70, 71, 78, 80, 91, 92],
                    "companyName": "ММК",
                    "errors": [0.026221711339755045, 0.031019082207248645, 0.024474372227248804, 0.015311296630014702, 0.023646470098520302, 0.020076016328564993, 0.015628014306195773, 0.029642123178600755, 0.028303715099651378, 0.02461365575301478, 0.024731819348857743, 0.024568596813187266, 0.01607424949601663, 0.016410757745157573, 0.02660601239405453, 0.030520927375571296, 0.020733940253613855, 0.021021667529241395, 0.0217094885813022, 0.016146312585998013, 0.017566224572711994, 0.014911203799900212, 0.03220911305093746, 0.020377494608806956, 0.021391626843698924, 0.01419212669871186],
                    "expectPrice": [0.02652631578947364, -0.030843268073936267, 0.024691358024691287, 0.01599335341156928, 0.02382119400058824, 0.022049659668296397, -0.013801146855865504, 0.028783129096608732, -0.02913868036349272, -0.02564102564102564, -0.026856240126382276, 0.022343492586490956, -0.015153098547392652, -0.016074450084602326, -0.027937819328187927, 0.02820621170504967, -0.01839698762775679, 0.020818077204728624, -0.0207424742048717, -0.01786487656994376, 0.016086076488761106, 0.016243974009641556, -0.03265753424657539, 0.019450179289362234, 0.02113733905579398, 0.01594340618730853],
                    "predictPrice": [0.000304604449718594, 0.00017581413331237754, 0.00021698579744248354, 0.0006820567815545788, 0.00017472390206793934, 0.001973643339731403, 0.001826867450330268, -0.0008589940819920221, -0.0008349652638413411, -0.0010273698880108593, -0.0021244207775245314, -0.002225104226696308, 0.0009211509486239771, 0.0003363076605552474, -0.0013318069341333957, -0.0023147156705216267, 0.0023369526258570634, -0.00020359032451277114, 0.0009670143764304997, -0.0017185639839457471, -0.0014801480839508887, 0.0013327702097413452, -0.00044842119563793034, -0.0009273153194447223, -0.00025428778790494334, 0.0017512794885966702]
                }, {
                    "anomalyPricesNumbers": [1, 3, 7, 8, 9, 13, 18, 23, 31, 36, 38, 45, 51, 53, 55, 56, 59, 61, 65, 67, 78, 87, 90, 93],
                    "companyName": "СевСт-ао",
                    "errors": [0.0146380343025155, 0.012408538515443433, 0.030284614605489812, 0.021208219217880436, 0.01222497948323819, 0.01619144973764887, 0.02499046900877079, 0.013814996530691513, 0.011496025802582294, 0.014211474054407869, 0.02927130392507671, 0.014930251294960997, 0.024351283383995278, 0.014826616303576946, 0.03120909373007077, 0.011389837911626182, 0.026078291872534732, 0.01593327685513185, 0.022368119576054925, 0.016635726265875117, 0.013688042567421843, 0.021533090556378455, 0.01184422183445959, 0.015763712389044164],
                    "expectPrice": [-0.01516966067864276, 0.012630960540487592, -0.030284614605489812, 0.020146883684001655, 0.012931034482758555, 0.01628758673862747, 0.024877462313881313, 0.013497492020063798, 0.011851171336701967, 0.014430931203485082, -0.02904063833735383, 0.014696485623003111, -0.02443133951137334, -0.015154421638212338, 0.030972209226162752, -0.010304405369635177, -0.02625158582999889, -0.01598808341608741, 0.022329333071021095, 0.01676623626903062, -0.013949780789159026, -0.02167689161554197, -0.011942174732872382, -0.01586287661024177],
                    "predictPrice": [-0.0005316263761272603, 0.00022242202504416014, 0, -0.0010613355338787798, 0.0007060549995203659, 0.000096137000978603, -0.00011300669488947767, -0.0003175045106277159, 0.0003551455341196726, 0.00021945714907721379, 0.0002306655877228824, -0.00023376567195788583, -0.00008005612737806241, -0.0003278053346353923, -0.0002368845039080188, 0.0010854325419910047, -0.00017329395746415818, -0.00005480656095556032, -0.000038786505033829824, 0.00013051000315550442, -0.00026173822173718294, -0.0001438010591635121, -0.00009795289841279216, -0.00009916422119760348]
                }, {
                    "anomalyPricesNumbers": [4, 5, 7, 13, 17, 19, 20, 23, 28, 40, 41, 45, 47, 51, 55, 56, 57, 58, 71, 84, 86, 87, 95, 96],
                    "companyName": "НЛМК ао",
                    "errors": [0.01684347248722706, 0.016044249367232505, 0.032845927399419717, 0.014981545766152835, 0.01412900074670248, 0.03134273818355092, 0.016699105617896146, 0.015411806214585013, 0.023588816356267098, 0.01368925829669422, 0.01422125436854829, 0.014123859447114981, 0.017102759928984437, 0.057984117437704874, 0.03259587602214786, 0.022829798671269393, 0.023056197736647636, 0.018761474423347444, 0.013621404159091806, 0.013427277133240994, 0.012817456901313062, 0.016257783296568844, 0.015525769819565143, 0.013069114429346364],
                    "expectPrice": [-0.015663848831427216, -0.017455097394383955, -0.032539372640895486, 0.015376795018426658, -0.013948769972102388, 0.03203410475030445, 0.019584427991401965, 0.01430103682516986, 0.023490331000118683, -0.014047207829591237, 0.012956017729287426, 0.01436622831252069, -0.01596275357499182, -0.058956646021915235, 0.03197470133520719, -0.01994982678294113, -0.0248530852105777, -0.021000000000000085, 0.013655725382109614, -0.013737979268140485, -0.012470084393500376, 0.015134598685026658, -0.015848272278513886, -0.014496573537164057],
                    "predictPrice": [0.0011796236557998416, -0.0014108480271514494, 0.0003065547585242309, 0.00039524925227382284, 0.00018023077460009322, 0.0006913665667535301, 0.002885322373505818, -0.0011107693894151542, -0.00009848535614841662, -0.00035794953289701654, -0.0012652366392608644, 0.00024236886540570895, 0.0011400063539926157, -0.000972528584210362, -0.0006211746869406718, 0.002879971888328262, -0.0017968874739300643, -0.0022385255766526418, 0.00003432122301780842, -0.000310702134899491, 0.0003473725078126865, -0.001123184611542185, -0.00032250245894874274, -0.0014274591078176939]
                }, {
                    "anomalyPricesNumbers": [11, 16, 22, 31, 37, 38, 48, 49, 51, 56, 57, 59, 61, 71, 73, 74, 78, 81, 86, 87, 88, 94, 96],
                    "companyName": "ТМК ао",
                    "errors": [0.02725244431146009, 0.01405128988401371, 0.009206798866855605, 0.017088303607777366, 0.02465158828946461, 0.025160118368340017, 0.00983671091356939, 0.0118303703230779, 0.022993419490007553, 0.022069783770281785, 0.02271028812365249, 0.013772329103479856, 0.009736688782922259, 0.00971122609373548, 0.009906614668077041, 0.009194751607321304, 0.012590523960323988, 0.0094106863290604, 0.03239929947460598, 0.0090510057129951, 0.010430320989552162, 0.01833438161875521, 0.011166671938436377],
                    "expectPrice": [0.027266530334015, -0.013995801259622112, -0.009206798866855605, -0.01713062098501075, 0.024700070571630206, -0.025325615050651232, -0.00978179082016541, 0.011895910780669103, -0.022953328232593728, -0.022064617809298526, -0.022562449637389296, -0.013809910641754694, -0.009779951100244521, -0.009716599190283424, -0.009868421052631603, -0.009128630705394145, -0.012562814070351758, -0.009393680614859169, -0.03239929947460598, -0.0088339222614841, 0.010489510489510514, -0.018298261665141813, -0.011142061281337073],
                    "predictPrice": [0.0000140860225549104, 0.000055488624391597475, -0, -0.00004231737723338211, 0.00004848228216559667, -0.00016549668231121458, 0.00005492009340398031, 0.00006554045759120378, 0.00004009125741382531, 0.000005165960983259046, 0.00014783848626319284, -0.000037581538274838464, -0.000043262317322263464, -0.00000537309654794389, 0.00003819361544543844, 0.0000661209019271593, 0.000027709889972231014, 0.00001700571420123197, -0, 0.00021708345151100013, 0.000059189499958352115, 0.000036119953613399215, 0.00002461065709930327]
                }]
                , accuracy);
        });

        function getPredictAnomaly() {
            const allCompanyForPrediction = getConvertedCompanys(getAssertNames(), recomputeCompanyPrice());
            const centroid = getCentroidData(getAssertNames(), recomputeCompanyPrice());
            const anomaly = Anomaly.predictAnomalyDifferenceCentroid(allCompanyForPrediction, centroid);
            return anomaly;
        }

        function getAssertNames() {
            return ["ММК", 'СевСт-ао', "НЛМК ао", "ТМК ао"];
        }

        function getColumnName() {
            return ["index",
                "ММК",
                "ENPL-гдр",
                "СевСт-ао",
                "НЛМК ао",
                "ТМК ао",
                "RUSAL plc",
                "ГМКНорНик"];
        }

        it('Поиск корреляции', () => {
            const allCompanyForPrediction = getConvertedCompanys(getAssertNames(), recomputeCompanyPrice());
            const centroid = getCentroidData(getAssertNames(), recomputeCompanyPrice());
            const mmk = getCorrelationByCentroid(allCompanyForPrediction[0], centroid);
            const sev = getCorrelationByCentroid(allCompanyForPrediction[1], centroid);
            const nlmk = getCorrelationByCentroid(allCompanyForPrediction[2], centroid);
            const tmk = getCorrelationByCentroid(allCompanyForPrediction[3], centroid);
            const mmkSev = getPearsonCorrelation(allCompanyForPrediction[0].prices, allCompanyForPrediction[0].prices);
            const mmkSev1 = getPearsonCorrelation(allCompanyForPrediction[0].prices, allCompanyForPrediction[1].prices);
            expect(getCorrelationByCentroid(allCompanyForPrediction[0], centroid)).toEqual([]
                , accuracy);

            function getCorrelationByCentroid({prices}, centroid) {
                const forCorrelation = prices.slice(1, prices.length);
                const correlation = getPearsonCorrelation(forCorrelation, centroid);
                return correlation;
            }
        });



        class Anomaly {
            static getBaseResult() {
                return {
                    companyName: '',
                    anomalyPricesNumbers: [],
                    errors: [],
                    predictPrice: [],
                    expectPrice: []
                };
            }

            static predictAnomalyDifferenceCentroid(companys, centroid, errorBorder = math.std(centroid)) {
                let result = [];
                for (const company of companys) {
                    result.push(getAnomalyCompany(company, centroid));
                }
                return result;

                function getAnomalyCompany(company) {
                    let anomalyCompany = Anomaly.getBaseResult();
                    anomalyCompany.companyName = company.name;
                    let {prices} = company;
                    for (let i = 1; i < prices.length; i++) {
                        let error = Math.sqrt((prices[i] - centroid[i - 1]) ** 2);
                        if (error > errorBorder) {
                            anomalyCompany.anomalyPricesNumbers.push(i);
                            anomalyCompany.errors.push(error);
                            anomalyCompany.predictPrice.push(centroid[i - 1]);
                            anomalyCompany.expectPrice.push(prices[i]);
                        }
                    }
                    return anomalyCompany;
                }

                /*
                * устанавливаем границы
                * считаем центроид
                *
                *   проходимся по каждой компании
                *       считаем корреляцию между центройдом и временным рядом помпании
                *
                *       проходимся по временному ряду
                *           предсказываем значение
                *           считаем ошибку между фактическим значением и предсказанным
                *           если ошибка превышает определённый порог то это анамалия
                *           запоминаем её
                *
                * */
            }

            static predictAnomaly(companys, centroid) {
                let result = [];
                for (const company of companys) {
                    result.push(getAnomalyCompany(company));
                }
                return result;

                function getAnomalyCompany(company) {
                    let anomalyCompany = Anomaly.getBaseResult();
                    anomalyCompany.companyName = company.name;
                    let {prices} = company;
                    prices[0] = prices[1];
                    const correlation = getPearsonCorrelation(prices, centroid);
                    const stdev = math.std(prices);
                    for (let i = 1; i < prices.length; i++) {
                        let predict = prices[i - 1] * correlation;
                        let expect = prices[i];
                        let error = Math.sqrt((predict - expect) ** 2);
                        if (error > stdev) {
                            anomalyCompany.anomalyPricesNumbers.push(i);
                            anomalyCompany.errors.push(error);
                            anomalyCompany.predictPrice.push(predict);
                            anomalyCompany.expectPrice.push(expect);
                        }
                    }
                    return anomalyCompany;
                }
            }

            static getMaxBorderAnomaly(centroid, maxBorderError) {
                let result = {};
                for (let i = 0; i < centroid.length; i++) {
                    result[i + 1] = centroid[i] + maxBorderError;
                }
                return result;
            }

            static getMinBorderAnomaly(centroid, maxBorderError) {
                let result = {};
                for (let i = 0; i < centroid.length; i++) {
                    result[i + 1] = centroid[i] - maxBorderError;
                }
                return result;
            }
        }

        function getConvertedCompanys(companyNames, dataSet) {
            let result = [];
            for (const companyName of companyNames) {
                let company = {
                    name: companyName,
                    prices: [],
                    timeserials: ['10.11.2018', '11.11.2018', '12.11.2018'],
                };
                for (const set of dataSet) {
                    company.prices.push(set[companyName]);
                }
                result.push(company);
            }
            return result;
        }

        function recomputeCompanyPrice() {
            const oldPrice = getTablePrices();
            let copy = getTablePrices();
            for (let i = 1; i < oldPrice.length; i++) {
                for (const nameCompany in oldPrice[i]) {
                    if (nameCompany != "index") {
                        copy[i][nameCompany] = (oldPrice[i][nameCompany] - oldPrice[i - 1][nameCompany]) / oldPrice[i - 1][nameCompany];
                    }
                }
            }
            return copy;
        }

        /**
         * calculates pearson correlation
         * @param {number[]} d1
         * @param {number[]} d2
         */
        function getPearsonCorrelation(d1, d2) {
            let {min, pow, sqrt} = Math
            let add = (a, b) => a + b
            let n = min(d1.length, d2.length)
            if (n === 0) {
                return 0
            }
            [d1, d2] = [d1.slice(0, n), d2.slice(0, n)]
            let [sum1, sum2] = [d1, d2].map(l => l.reduce(add))
            let [pow1, pow2] = [d1, d2].map(l => l.reduce((a, b) => a + pow(b, 2), 0))
            let mulSum = d1.map((n, i) => n * d2[i]).reduce(add)
            let dense = sqrt((pow1 - pow(sum1, 2) / n) * (pow2 - pow(sum2, 2) / n))
            if (dense === 0) {
                return 0;
            }
            return (mulSum - (sum1 * sum2 / n)) / dense;
        }

        function getCentroidMinBorderForGrapch(companyName, prices, maxError = '') {
            const centroid = getCentroidData(companyName, prices);
            if (maxError === '') {
                return Anomaly.getMinBorderAnomaly(centroid, math.std(centroid));
            }
            return Anomaly.getMinBorderAnomaly(centroid, maxError);
        }

        function getCentroidMaxBorderForGrapch(companyName, prices, maxError = '') {
            const centroid = getCentroidData(companyName, prices);
            if (maxError === '') {
                return Anomaly.getMaxBorderAnomaly(centroid, math.std(centroid));
            }
            return Anomaly.getMaxBorderAnomaly(centroid, maxError);
        }


        function getCentroidForGrapch(companyNames, prices) {
            const centroid = getCentroidData(companyNames, prices);
            let result = {};
            for (let i = 0; i < centroid.length; i++) {
                result[i + 1] = centroid[i];
            }
            return result;
        }

        function getCentroidData(companyNames, prices) {
            const timeserials = [];
            for (const companyName of companyNames) {
                timeserials.push({
                    prices: getPrices(companyName, prices)
                });
            }
            const matrixPrice = getMatrixPrices(timeserials);
            const centroid = getCentroid(matrixPrice);
            return centroid;

            function getPrices(companyName, prices) {
                let result = [];
                for (let i = 1; i < prices.length; i++) {
                    result.push(prices[i][companyName]);
                }
                return result;
            }

            function getMatrixPrices(companys) {
                let matrix = [];
                for (const company of companys) {
                    matrix.push(company.prices);
                }
                return matrix;
            }

            function getCentroid(matrixPrices) {
                let centroid = [];
                for (let col = 0; col < matrixPrices[0].length; col++) {
                    let sum = 0;
                    for (let row = 0; row < matrixPrices.length; row++) {
                        let cell = matrixPrices[row][col];
                        sum = sum + cell;
                    }
                    centroid.push(sum / matrixPrices.length);
                }
                return centroid;
            }
        }

        function getForGrachByCompany(companyName, prices) {
            let result = {};
            for (let i = 1; i < prices.length; i++) {
                result[i] = prices[i][companyName];
            }
            return result;
        }

        function getTablePrices() {
            return [{
                "index": 0,
                "ММК": 45.99,
                "ENPL-гдр": 329.0,
                "СевСт-ао": 1017.2,
                "НЛМК ао": 161.12,
                "ТМК ао": 71.6,
                "RUSAL plc": 25.72,
                "ГМКНорНик": 10819
            }, {
                "index": 1,
                "ММК": 46.07,
                "ENPL-гдр": 319.0,
                "СевСт-ао": 1002.0,
                "НЛМК ао": 160.16,
                "ТМК ао": 71.7,
                "RUSAL plc": 25.37,
                "ГМКНорНик": 10682
            }, {
                "index": 2,
                "ММК": 46.24,
                "ENPL-гдр": 305.5,
                "СевСт-ао": 1008.4,
                "НЛМК ао": 161.26,
                "ТМК ао": 71.75,
                "RUSAL plc": 24.95,
                "ГМКНорНик": 10671
            }, {
                "index": 3,
                "ММК": 47.5,
                "ENPL-гдр": 304.5,
                "СевСт-ао": 1021.3,
                "НЛМК ао": 163.4,
                "ТМК ао": 71.95,
                "RUSAL plc": 25.43,
                "ГМКНорНик": 10750
            }, {
                "index": 4,
                "ММК": 47.58,
                "ENPL-гдр": 300.0,
                "СевСт-ао": 1020.3,
                "НЛМК ао": 160.88,
                "ТМК ао": 72.1,
                "RUSAL plc": 25.06,
                "ГМКНорНик": 10798
            }, {
                "index": 5,
                "ММК": 47.025,
                "ENPL-гдр": 291.0,
                "СевСт-ао": 1017.2,
                "НЛМК ао": 158.12,
                "ТМК ао": 72.05,
                "RUSAL plc": 24.59,
                "ГМКНорНик": 10769
            }, {
                "index": 6,
                "ММК": 47.125,
                "ENPL-гдр": 283.5,
                "СевСт-ао": 1017.2,
                "НЛМК ао": 158.66,
                "ТМК ао": 71.55,
                "RUSAL plc": 24.17,
                "ГМКНорНик": 10871
            }, {
                "index": 7,
                "ММК": 45.715,
                "ENPL-гдр": 282.5,
                "СевСт-ао": 987.3,
                "НЛМК ао": 153.66,
                "ТМК ао": 71.0,
                "RUSAL plc": 24.74,
                "ГМКНорНик": 10664
            }, {
                "index": 8,
                "ММК": 45.815,
                "ENPL-гдр": 292.0,
                "СевСт-ао": 1007.6,
                "НЛМК ао": 153.74,
                "ТМК ао": 71.1,
                "RUSAL plc": 24.6,
                "ГМКНорНик": 10981
            }, {
                "index": 9,
                "ММК": 46.16,
                "ENPL-гдр": 296.5,
                "СевСт-ао": 1020.8,
                "НЛМК ао": 154.92,
                "ТМК ао": 71.5,
                "RUSAL plc": 24.46,
                "ГМКНорНик": 11044
            }, {
                "index": 10,
                "ММК": 46.055,
                "ENPL-гдр": 298.5,
                "СевСт-ао": 1015.8,
                "НЛМК ао": 154.2,
                "ТМК ао": 71.35,
                "RUSAL plc": 24.16,
                "ГМКНорНик": 10935
            }, {
                "index": 11,
                "ММК": 45.7,
                "ENPL-гдр": 295.5,
                "СевСт-ао": 1017.9,
                "НЛМК ао": 154.28,
                "ТМК ао": 73.35,
                "RUSAL plc": 24.08,
                "ГМКНорНик": 10932
            }, {
                "index": 12,
                "ММК": 45.82,
                "ENPL-гдр": 295.5,
                "СевСт-ао": 1020.7,
                "НЛМК ао": 154.96,
                "ТМК ао": 73.3,
                "RUSAL plc": 24.21,
                "ГМКНорНик": 10870
            }, {
                "index": 13,
                "ММК": 46.98,
                "ENPL-гдр": 289.5,
                "СевСт-ао": 1037.6,
                "НЛМК ао": 157.38,
                "ТМК ао": 73.05,
                "RUSAL plc": 24.26,
                "ГМКНорНик": 10926
            }, {
                "index": 14,
                "ММК": 46.795,
                "ENPL-гдр": 283.0,
                "СевСт-ао": 1044.1,
                "НЛМК ао": 158.0,
                "ТМК ао": 73.05,
                "RUSAL plc": 24.13,
                "ГМКНорНик": 10964
            }, {
                "index": 15,
                "ММК": 46.87,
                "ENPL-гдр": 273.0,
                "СевСт-ао": 1049.5,
                "НЛМК ао": 159.6,
                "ТМК ао": 72.45,
                "RUSAL plc": 23.74,
                "ГМКНорНик": 10979
            }, {
                "index": 16,
                "ММК": 46.655,
                "ENPL-гдр": 268.5,
                "СевСт-ао": 1057.8,
                "НЛМК ао": 159.92,
                "ТМК ао": 71.45,
                "RUSAL plc": 22.87,
                "ГМКНорНик": 11013
            }, {
                "index": 17,
                "ММК": 46.985,
                "ENPL-гдр": 256.0,
                "СевСт-ао": 1054.4,
                "НЛМК ао": 157.72,
                "ТМК ао": 71.15,
                "RUSAL plc": 21.69,
                "ГМКНорНик": 10991
            }, {
                "index": 18,
                "ММК": 47.375,
                "ENPL-гдр": 259.5,
                "СевСт-ао": 1081.3,
                "НЛМК ао": 158.94,
                "ТМК ао": 71.05,
                "RUSAL plc": 21.74,
                "ГМКНорНик": 11033
            }, {
                "index": 19,
                "ММК": 48.145,
                "ENPL-гдр": 262.0,
                "СевСт-ао": 1091.0,
                "НЛМК ао": 164.2,
                "ТМК ао": 71.65,
                "RUSAL plc": 21.87,
                "ГМКНорНик": 11208
            }, {
                "index": 20,
                "ММК": 48.675,
                "ENPL-гдр": 277.5,
                "СевСт-ао": 1092.8,
                "НЛМК ао": 167.48,
                "ТМК ао": 71.25,
                "RUSAL plc": 22.0,
                "ГМКНорНик": 11308
            }, {
                "index": 21,
                "ММК": 48.785,
                "ENPL-гдр": 281.5,
                "СевСт-ао": 1091.5,
                "НЛМК ао": 167.46,
                "ТМК ао": 71.25,
                "RUSAL plc": 21.98,
                "ГМКНорНик": 11285
            }, {
                "index": 22,
                "ММК": 48.485,
                "ENPL-гдр": 278.5,
                "СевСт-ао": 1081.7,
                "НЛМК ао": 165.42,
                "ТМК ао": 70.6,
                "RUSAL plc": 21.74,
                "ГМКНорНик": 11237
            }, {
                "index": 23,
                "ММК": 48.745,
                "ENPL-гдр": 275.0,
                "СевСт-ао": 1096.5,
                "НЛМК ао": 167.82,
                "ТМК ао": 70.9,
                "RUSAL plc": 21.72,
                "ГМКНорНик": 11270
            }, {
                "index": 24,
                "ММК": 48.59,
                "ENPL-гдр": 269.0,
                "СевСт-ао": 1093.1,
                "НЛМК ао": 166.62,
                "ТМК ао": 71.05,
                "RUSAL plc": 21.54,
                "ГМКНорНик": 11227
            }, {
                "index": 25,
                "ММК": 48.475,
                "ENPL-гдр": 264.5,
                "СевСт-ао": 1081.3,
                "НЛМК ао": 165.06,
                "ТМК ао": 70.65,
                "RUSAL plc": 21.34,
                "ГМКНорНик": 11179
            }, {
                "index": 26,
                "ММК": 48.56,
                "ENPL-гдр": 261.5,
                "СевСт-ао": 1069.7,
                "НЛМК ао": 164.8,
                "ТМК ао": 70.65,
                "RUSAL plc": 21.43,
                "ГМКНорНик": 11215
            }, {
                "index": 27,
                "ММК": 48.95,
                "ENPL-гдр": 265.5,
                "СевСт-ао": 1059.8,
                "НЛМК ао": 164.62,
                "ТМК ао": 70.7,
                "RUSAL plc": 20.7,
                "ГМКНорНик": 11175
            }, {
                "index": 28,
                "ММК": 49.07,
                "ENPL-гдр": 255.0,
                "СевСт-ао": 1061.3,
                "НЛМК ао": 168.58,
                "ТМК ао": 70.65,
                "RUSAL plc": 19.84,
                "ГМКНорНик": 11303
            }, {
                "index": 29,
                "ММК": 48.955,
                "ENPL-гдр": 251.0,
                "СевСт-ао": 1064.7,
                "НЛМК ао": 169.04,
                "ТМК ао": 70.8,
                "RUSAL plc": 19.79,
                "ГМКНорНик": 11401
            }, {
                "index": 30,
                "ММК": 49.37,
                "ENPL-гдр": 264.0,
                "СевСт-ао": 1075.6,
                "НЛМК ао": 169.1,
                "ТМК ао": 71.25,
                "RUSAL plc": 21.99,
                "ГМКНорНик": 11516
            }, {
                "index": 31,
                "ММК": 49.55,
                "ENPL-гдр": 266.0,
                "СевСт-ао": 1088.5,
                "НЛМК ао": 169.6,
                "ТМК ао": 70.05,
                "RUSAL plc": 22.26,
                "ГМКНорНик": 11606
            }, {
                "index": 32,
                "ММК": 49.435,
                "ENPL-гдр": 268.5,
                "СевСт-ао": 1090.9,
                "НЛМК ао": 169.58,
                "ТМК ао": 69.9,
                "RUSAL plc": 23.28,
                "ГМКНорНик": 11551
            }, {
                "index": 33,
                "ММК": 49.21,
                "ENPL-гдр": 290.5,
                "СевСт-ао": 1085.3,
                "НЛМК ао": 171.0,
                "ТМК ао": 70.4,
                "RUSAL plc": 25.96,
                "ГМКНорНик": 11468
            }, {
                "index": 34,
                "ММК": 49.685,
                "ENPL-гдр": 306.0,
                "СевСт-ао": 1079.1,
                "НЛМК ао": 172.52,
                "ТМК ао": 70.1,
                "RUSAL plc": 27.61,
                "ГМКНорНик": 11586
            }, {
                "index": 35,
                "ММК": 49.79,
                "ENPL-гдр": 299.5,
                "СевСт-ао": 1085.9,
                "НЛМК ао": 173.26,
                "ТМК ао": 69.6,
                "RUSAL plc": 27.5,
                "ГМКНорНик": 11694
            }, {
                "index": 36,
                "ММК": 51.005,
                "ENPL-гдр": 303.0,
                "СевСт-ао": 1101.8,
                "НЛМК ао": 175.48,
                "ТМК ао": 69.1,
                "RUSAL plc": 27.89,
                "ГМКНорНик": 11751
            }, {
                "index": 37,
                "ММК": 52.155,
                "ENPL-гдр": 324.5,
                "СевСт-ао": 1109.1,
                "НЛМК ао": 177.0,
                "ТМК ао": 70.85,
                "RUSAL plc": 29.11,
                "ГМКНорНик": 11969
            }, {
                "index": 38,
                "ММК": 51.445,
                "ENPL-гдр": 344.0,
                "СевСт-ао": 1077.8,
                "НЛМК ао": 176.84,
                "ТМК ао": 69.1,
                "RUSAL plc": 28.94,
                "ГМКНорНик": 11915
            }, {
                "index": 39,
                "ММК": 51.025,
                "ENPL-гдр": 333.0,
                "СевСт-ао": 1068.5,
                "НЛМК ао": 176.14,
                "ТМК ао": 68.7,
                "RUSAL plc": 27.76,
                "ГМКНорНик": 11889
            }, {
                "index": 40,
                "ММК": 50.67,
                "ENPL-гдр": 324.0,
                "СевСт-ао": 1069.4,
                "НЛМК ао": 173.7,
                "ТМК ао": 68.35,
                "RUSAL plc": 27.84,
                "ГМКНорНик": 11923
            }, {
                "index": 41,
                "ММК": 51.0,
                "ENPL-гдр": 318.0,
                "СевСт-ао": 1075.7,
                "НЛМК ао": 175.98,
                "ТМК ао": 68.5,
                "RUSAL plc": 28.05,
                "ГМКНорНик": 11838
            }, {
                "index": 42,
                "ММК": 51.445,
                "ENPL-гдр": 303.0,
                "СевСт-ао": 1087.6,
                "НЛМК ао": 177.26,
                "ТМК ао": 68.15,
                "RUSAL plc": 27.84,
                "ГМКНорНик": 11296
            }, {
                "index": 43,
                "ММК": 51.65,
                "ENPL-гдр": 310.0,
                "СевСт-ао": 1086.6,
                "НЛМК ао": 177.9,
                "ТМК ао": 68.3,
                "RUSAL plc": 28.38,
                "ГМКНорНик": 11418
            }, {
                "index": 44,
                "ММК": 51.12,
                "ENPL-гдр": 309.5,
                "СевСт-ао": 1079.4,
                "НЛМК ао": 178.38,
                "ТМК ао": 68.2,
                "RUSAL plc": 28.64,
                "ГМКНорНик": 11251
            }, {
                "index": 45,
                "ММК": 52.635,
                "ENPL-гдр": 313.5,
                "СевСт-ао": 1095.5,
                "НЛМК ао": 180.98,
                "ТМК ао": 67.6,
                "RUSAL plc": 28.89,
                "ГМКНорНик": 11404
            }, {
                "index": 46,
                "ММК": 52.62,
                "ENPL-гдр": 312.5,
                "СевСт-ао": 1107.8,
                "НЛМК ао": 183.3,
                "ТМК ао": 67.65,
                "RUSAL plc": 29.22,
                "ГМКНорНик": 11385
            }, {
                "index": 47,
                "ММК": 52.095,
                "ENPL-гдр": 305.0,
                "СевСт-ао": 1106.4,
                "НЛМК ао": 180.42,
                "ТМК ао": 67.1,
                "RUSAL plc": 28.08,
                "ГМКНорНик": 11312
            }, {
                "index": 48,
                "ММК": 50.62,
                "ENPL-гдр": 302.5,
                "СевСт-ао": 1095.2,
                "НЛМК ао": 179.44,
                "ТМК ао": 66.45,
                "RUSAL plc": 27.91,
                "ГМКНорНик": 11332
            }, {
                "index": 49,
                "ММК": 50.0,
                "ENPL-гдр": 304.5,
                "СевСт-ао": 1096.9,
                "НЛМК ао": 179.74,
                "ТМК ао": 67.25,
                "RUSAL plc": 27.97,
                "ГМКНорНик": 11429
            }, {
                "index": 50,
                "ММК": 48.75,
                "ENPL-гдр": 315.0,
                "СевСт-ао": 1094.4,
                "НЛМК ао": 177.82,
                "ТМК ао": 66.85,
                "RUSAL plc": 28.01,
                "ГМКНорНик": 11475
            }, {
                "index": 51,
                "ММК": 47.475,
                "ENPL-гдр": 304.0,
                "СевСт-ао": 1068.3,
                "НЛМК ао": 167.92,
                "ТМК ао": 65.35,
                "RUSAL plc": 27.58,
                "ГМКНорНик": 11208
            }, {
                "index": 52,
                "ММК": 48.56,
                "ENPL-гдр": 317.0,
                "СевСт-ао": 1058.4,
                "НЛМК ао": 166.86,
                "ТМК ао": 65.0,
                "RUSAL plc": 28.47,
                "ГМКНорНик": 11237
            }, {
                "index": 53,
                "ММК": 48.2,
                "ENPL-гдр": 315.5,
                "СевСт-ао": 1042.6,
                "НЛМК ао": 166.44,
                "ТМК ао": 64.9,
                "RUSAL plc": 28.37,
                "ГМКНорНик": 11098
            }, {
                "index": 54,
                "ММК": 48.03,
                "ENPL-гдр": 315.5,
                "СевСт-ао": 1035.6,
                "НЛМК ао": 165.3,
                "ТМК ао": 64.9,
                "RUSAL plc": 28.43,
                "ГМКНорНик": 11177
            }, {
                "index": 55,
                "ММК": 48.57,
                "ENPL-гдр": 314.5,
                "СевСт-ао": 1068.7,
                "НЛМК ао": 170.76,
                "ТМК ао": 64.85,
                "RUSAL plc": 28.5,
                "ГМКНорНик": 11389
            }, {
                "index": 56,
                "ММК": 47.845,
                "ENPL-гдр": 312.5,
                "СевСт-ао": 1057.8,
                "НЛМК ао": 167.42,
                "ТМК ао": 63.45,
                "RUSAL plc": 28.74,
                "ГМКНорНик": 11332
            }, {
                "index": 57,
                "ММК": 48.04,
                "ENPL-гдр": 305.0,
                "СевСт-ао": 1056.8,
                "НЛМК ао": 163.36,
                "ТМК ао": 62.05,
                "RUSAL plc": 28.85,
                "ГМКНорНик": 11389
            }, {
                "index": 58,
                "ММК": 47.28,
                "ENPL-гдр": 301.5,
                "СевСт-ао": 1051.6,
                "НЛМК ао": 160.0,
                "ТМК ао": 62.4,
                "RUSAL plc": 28.62,
                "ГМКНорНик": 11430
            }, {
                "index": 59,
                "ММК": 45.995,
                "ENPL-гдр": 292.5,
                "СевСт-ао": 1024.7,
                "НЛМК ао": 158.84,
                "ТМК ао": 61.55,
                "RUSAL plc": 28.05,
                "ГМКНорНик": 11172
            }, {
                "index": 60,
                "ММК": 47.33,
                "ENPL-гдр": 297.5,
                "СевСт-ао": 1023.1,
                "НЛМК ао": 159.18,
                "ТМК ао": 61.95,
                "RUSAL plc": 28.24,
                "ГМКНорНик": 11137
            }, {
                "index": 61,
                "ММК": 46.475,
                "ENPL-гдр": 297.5,
                "СевСт-ао": 1007.0,
                "НЛМК ао": 158.58,
                "ТМК ао": 61.35,
                "RUSAL plc": 28.38,
                "ГМКНорНик": 10943
            }, {
                "index": 62,
                "ММК": 46.345,
                "ENPL-гдр": 295.0,
                "СевСт-ао": 996.2,
                "НЛМК ао": 157.82,
                "ТМК ао": 61.2,
                "RUSAL plc": 28.32,
                "ГМКНорНик": 10721
            }, {
                "index": 63,
                "ММК": 46.915,
                "ENPL-гдр": 297.0,
                "СевСт-ао": 995.0,
                "НЛМК ао": 158.72,
                "ТМК ао": 61.55,
                "RUSAL plc": 28.74,
                "ГМКНорНик": 10675
            }, {
                "index": 64,
                "ММК": 46.8,
                "ENPL-гдр": 293.5,
                "СевСт-ао": 993.9,
                "НЛМК ао": 158.1,
                "ТМК ао": 62.05,
                "RUSAL plc": 28.43,
                "ГМКНорНик": 10682
            }, {
                "index": 65,
                "ММК": 47.795,
                "ENPL-гдр": 296.5,
                "СевСт-ао": 1016.6,
                "НЛМК ао": 160.06,
                "ТМК ао": 61.8,
                "RUSAL plc": 28.51,
                "ГМКНорНик": 10920
            }, {
                "index": 66,
                "ММК": 47.42,
                "ENPL-гдр": 295.1,
                "СевСт-ао": 1020.4,
                "НЛМК ао": 158.32,
                "ТМК ао": 61.45,
                "RUSAL plc": 28.175,
                "ГМКНорНик": 11054
            }, {
                "index": 67,
                "ММК": 47.98,
                "ENPL-гдр": 298.2,
                "СевСт-ао": 1037.8,
                "НЛМК ао": 158.66,
                "ТМК ао": 61.85,
                "RUSAL plc": 28.385,
                "ГМКНорНик": 11387
            }, {
                "index": 68,
                "ММК": 47.005,
                "ENPL-гдр": 314.2,
                "СевСт-ао": 1038.2,
                "НЛМК ао": 157.66,
                "ТМК ао": 62.3,
                "RUSAL plc": 29.405,
                "ГМКНорНик": 11489
            }, {
                "index": 69,
                "ММК": 46.18,
                "ENPL-гдр": 313.9,
                "СевСт-ао": 1039.5,
                "НЛМК ао": 157.4,
                "ТМК ао": 62.3,
                "RUSAL plc": 29.75,
                "ГМКНорНик": 11620
            }, {
                "index": 70,
                "ММК": 46.935,
                "ENPL-гдр": 314.6,
                "СевСт-ао": 1031.3,
                "НЛМК ао": 157.46,
                "ТМК ао": 62.35,
                "RUSAL plc": 29.83,
                "ГМКНорНик": 11672
            }, {
                "index": 71,
                "ММК": 47.71,
                "ENPL-гдр": 314.7,
                "СевСт-ао": 1030.0,
                "НЛМК ао": 159.64,
                "ТМК ао": 61.75,
                "RUSAL plc": 29.68,
                "ГМКНорНик": 11615
            }, {
                "index": 72,
                "ММК": 47.975,
                "ENPL-гдр": 320.2,
                "СевСт-ао": 1036.7,
                "НЛМК ао": 161.84,
                "ТМК ао": 61.4,
                "RUSAL plc": 29.915,
                "ГМКНорНик": 11651
            }, {
                "index": 73,
                "ММК": 47.655,
                "ENPL-гдр": 324.6,
                "СевСт-ао": 1035.9,
                "НЛМК ао": 163.38,
                "ТМК ао": 60.8,
                "RUSAL plc": 29.665,
                "ГМКНорНик": 11626
            }, {
                "index": 74,
                "ММК": 47.135,
                "ENPL-гдр": 325.0,
                "СевСт-ао": 1031.8,
                "НЛМК ао": 164.58,
                "ТМК ао": 60.25,
                "RUSAL plc": 29.695,
                "ГМКНорНик": 11613
            }, {
                "index": 75,
                "ММК": 47.5,
                "ENPL-гдр": 324.1,
                "СевСт-ао": 1030.0,
                "НЛМК ао": 164.88,
                "ТМК ао": 60.55,
                "RUSAL plc": 29.655,
                "ГМКНорНик": 11781
            }, {
                "index": 76,
                "ММК": 47.37,
                "ENPL-гдр": 326.2,
                "СевСт-ао": 1025.2,
                "НЛМК ао": 163.86,
                "ТМК ао": 60.7,
                "RUSAL plc": 29.555,
                "ГМКНорНик": 11678
            }, {
                "index": 77,
                "ММК": 47.115,
                "ENPL-гдр": 336.7,
                "СевСт-ао": 1017.6,
                "НЛМК ао": 163.2,
                "ТМК ао": 60.45,
                "RUSAL plc": 29.545,
                "ГМКНорНик": 11778
            }, {
                "index": 78,
                "ММК": 45.625,
                "ENPL-гдр": 333.8,
                "СевСт-ао": 1003.6,
                "НЛМК ао": 162.06,
                "ТМК ао": 59.7,
                "RUSAL plc": 29.14,
                "ГМКНорНик": 11670
            }, {
                "index": 79,
                "ММК": 45.12,
                "ENPL-гдр": 334.7,
                "СевСт-ао": 1007.9,
                "НЛМК ао": 162.8,
                "ТМК ао": 59.25,
                "RUSAL plc": 29.2,
                "ГМКНорНик": 11771
            }, {
                "index": 80,
                "ММК": 46.015,
                "ENPL-гдр": 333.3,
                "СевСт-ао": 1008.0,
                "НЛМК ао": 163.66,
                "ТМК ао": 59.1,
                "RUSAL plc": 29.515,
                "ГМКНорНик": 11846
            }, {
                "index": 81,
                "ММК": 46.025,
                "ENPL-гдр": 327.3,
                "СевСт-ао": 1004.6,
                "НЛМК ао": 164.18,
                "ТМК ао": 58.55,
                "RUSAL plc": 29.33,
                "ГМКНорНик": 11897
            }, {
                "index": 82,
                "ММК": 45.735,
                "ENPL-гдр": 318.0,
                "СевСт-ао": 999.3,
                "НЛМК ао": 162.9,
                "ТМК ао": 58.85,
                "RUSAL plc": 28.41,
                "ГМКНорНик": 11898
            }, {
                "index": 83,
                "ММК": 46.125,
                "ENPL-гдр": 320.1,
                "СевСт-ао": 996.1,
                "НЛМК ао": 162.34,
                "ТМК ао": 59.0,
                "RUSAL plc": 28.87,
                "ГМКНорНик": 12187
            }, {
                "index": 84,
                "ММК": 46.575,
                "ENPL-гдр": 321.5,
                "СевСт-ао": 992.9,
                "НЛМК ао": 160.14,
                "ТМК ао": 58.95,
                "RUSAL plc": 28.825,
                "ГМКНорНик": 12589
            }, {
                "index": 85,
                "ММК": 46.25,
                "ENPL-гдр": 320.0,
                "СевСт-ао": 1003.3,
                "НЛМК ао": 160.76,
                "ТМК ао": 58.95,
                "RUSAL plc": 28.885,
                "ГМКНорНик": 12830
            }, {
                "index": 86,
                "ММК": 46.26,
                "ENPL-гдр": 313.6,
                "СевСт-ао": 999.2,
                "НЛМК ао": 158.78,
                "ТМК ао": 57.1,
                "RUSAL plc": 28.845,
                "ГМКНорНик": 12701
            }, {
                "index": 87,
                "ММК": 46.61,
                "ENPL-гдр": 316.7,
                "СевСт-ао": 978.0,
                "НЛМК ао": 161.22,
                "ТМК ао": 56.6,
                "RUSAL plc": 29.14,
                "ГМКНорНик": 12806
            }, {
                "index": 88,
                "ММК": 46.175,
                "ENPL-гдр": 317.8,
                "СевСт-ао": 968.7,
                "НЛМК ао": 159.94,
                "ТМК ао": 57.2,
                "RUSAL plc": 29.03,
                "ГМКНорНик": 12645
            }, {
                "index": 89,
                "ММК": 45.755,
                "ENPL-гдр": 318.2,
                "СевСт-ао": 966.0,
                "НЛМК ао": 158.32,
                "ТМК ао": 57.1,
                "RUSAL plc": 28.85,
                "ГМКНорНик": 12649
            }, {
                "index": 90,
                "ММК": 45.615,
                "ENPL-гдр": 315.3,
                "СевСт-ао": 954.6,
                "НЛМК ао": 157.18,
                "ТМК ао": 56.8,
                "RUSAL plc": 28.465,
                "ГМКНорНик": 12647
            }, {
                "index": 91,
                "ММК": 46.6,
                "ENPL-гдр": 314.7,
                "СевСт-ао": 956.9,
                "НЛМК ао": 158.06,
                "ТМК ао": 56.4,
                "RUSAL plc": 28.26,
                "ГМКНорНик": 12717
            }, {
                "index": 92,
                "ММК": 47.355,
                "ENPL-гдр": 307.2,
                "СевСт-ао": 954.2,
                "НЛМК ао": 157.08,
                "ТМК ао": 55.95,
                "RUSAL plc": 28.235,
                "ГМКНорНик": 12685
            }, {
                "index": 93,
                "ММК": 46.805,
                "ENPL-гдр": 305.7,
                "СевСт-ао": 939.3,
                "НЛМК ао": 156.96,
                "ТМК ао": 55.65,
                "RUSAL plc": 27.765,
                "ГМКНорНик": 12663
            }, {
                "index": 94,
                "ММК": 46.34,
                "ENPL-гдр": 305.0,
                "СевСт-ао": 933.1,
                "НЛМК ао": 156.4,
                "ТМК ао": 54.65,
                "RUSAL plc": 27.52,
                "ГМКНорНик": 12840
            }, {
                "index": 95,
                "ММК": 46.92,
                "ENPL-гдр": 304.5,
                "СевСт-ао": 933.1,
                "НЛМК ао": 153.96,
                "ТМК ао": 54.45,
                "RUSAL plc": 26.83,
                "ГМКНорНик": 13127
            }, {
                "index": 96,
                "ММК": 46.42,
                "ENPL-гдр": 300.3,
                "СевСт-ао": 932.5,
                "НЛМК ао": 151.76,
                "ТМК ао": 53.85,
                "RUSAL plc": 26.395,
                "ГМКНорНик": 13122
            }];

        }
    }
);
/*
* пройтись по каждому сектору,
* выгрузить из датафрейма информацию по компаниям
* посчитать для корреляцию среди бумаг по сектору
* выбрать скоррелированный сектор,
* скачать по нему катировки
* нарисовать их на графке
* сделать предобработку
* нарисовать на графике
* добавить аномалии
* нарисовать их на графике
*
*
*
*
*
*
*
* */

