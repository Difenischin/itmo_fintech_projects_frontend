describe('Moex anomaly find', () => {
        /*
        * урл который отдает данные по манипуляциям с ценной бумагой за последние 100 дней
        *
        * вход тикер
        * выход [{
        * data: '2018-08-01',
        * f: '',
        * }]
        *


@app.route('/get_multiplikators_sector/<tiker>', methods=['GET', 'POST'])
def get_multiplikators_sector(tiker):
    return get_multiplikators_sector_chache(tiker)


@app.route('/get_correlations/<tiker>', methods=['GET', 'POST'])
def get_correlations(tiker):
    return get_correlations_chache(tiker)


@app.route('/manipulation_fz/<tiker>', methods=['GET', 'POST'])
def manipulation_fz(tiker):
    return get_manipulation_fz_cache(tiker)


        *
        * */
        const axios = require("axios");
        const DataFrame = require('dataframe-js');
        const accuracy = 5;

        it('URL для парсинга 1', async () => {
            const url = "http://185.185.71.254:5000/get_multiplikators_sector/AFSK";
            const response = await axios.get(url);
            expect(result).toEqual(url);
        });

        it('URL для парсинга', async () => {
            const url = MoexHelper.getUrlForLoadData('AFKS', '2018-08-01', 5);
            const result = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/AFKS.json?from=2018-08-01&limit=5';
            expect(result).toEqual(url);
        });

        it('URL для получения индексов бумаг в которые входит бумага прямо сейчас', async () => {
            const url = MoexHelper.getUrlForActualIndices('MAGN');
            const result = 'https://iss.moex.com/iss/securities/MAGN/indices.json?only_actual=1';
            expect(result).toEqual(url);
        });

        it('Вернуть список индексов в которые входит бумага', async () => {
            const url = await MoexHelper.getActualIndices('MAGN');
            const result = [
                ["EPSI", "Субиндекс акций", "2015-12-16", "2019-04-05"],
                ["IMOEX", "Индекс МосБиржи", "2007-10-15", "2019-04-05"],
                ["MOEXBMI", "Индекс широкого рынка", "2011-12-30", "2019-04-05"],
                ["MOEXMM", "Индекс металлов и добычи", "2006-07-03", "2019-04-05"],
                ["RTSI", "Индекс РТС", "2009-09-30", "2019-04-05"],
                ["RTSmm", "Индекс РТС металлов и добычи", "2009-09-30", "2019-04-05"],
                ["RUBMI", "Индекс РТС широкого рынка", "2013-05-20", "2019-04-05"]
            ];
            expect(result).toEqual(url);
        });

        it('Показатели для определения критериев существенного отклонения', async () => {
            const url = 'https://iss.moex.com/iss/statistics/engines/stock/deviationcoeffs?date=2018-05-08';
            const response = await axios.get(url);
            expect(response.data).toEqual(url);
            /*/iss/statistics/engines/stock/deviationcoeffs
 Показатели для определения критериев существенного отклонения*/
        });

        xit('URL для парсинга стратегий', async () => {
            const url = 'http://localhost/site.local/yii2/basic/web/site/get-test';
            const response = await axios.get(url);
            expect(response.data).toEqual(url);
        });


        it('Полдучение данных с московской биржи', async () => {

            const result1 = await MoexHelper.getHistoryData('AFKS', '2018-08-01', 5);
            expect({
                    "columns": ["BOARDID", "TRADEDATE", "SHORTNAME", "SECID", "NUMTRADES", "VALUE", "OPEN", "LOW", "HIGH", "LEGALCLOSEPRICE", "WAPRICE", "CLOSE", "VOLUME", "MARKETPRICE2", "MARKETPRICE3", "ADMITTEDQUOTE", "MP2VALTRD", "MARKETPRICE3TRADESVALUE", "ADMITTEDVALUE", "WAVAL"],
                    "data": [
                        ["TQBR", "2018-08-01", "Система ао", "AFKS", 3821, 131587315, 9.025, 8.97, 9.18, 8.98, 9.075, 8.98, 14497000, 9.075, 9.075, 8.98, 131587315, 131587315, 131587315, null],
                        ["TQBR", "2018-08-02", "Система ао", "AFKS", 5568, 130802750, 8.95, 8.81, 9.085, 8.85, 8.915, 8.85, 14673900, 8.915, 8.915, 8.85, 130802750, 130802750, 130802750, null],
                        ["TQBR", "2018-08-03", "Система ао", "AFKS", 2942, 75150225, 8.9, 8.765, 8.94, 8.89, 8.86, 8.89, 8480500, 8.86, 8.86, 8.89, 75150225, 75150225, 75150225, null],
                        ["TQBR", "2018-08-06", "Система ао", "AFKS", 2416, 68002859.5, 8.9, 8.815, 9, 9, 8.945, 9, 7602400, 8.945, 8.945, 9, 68002859.5, 68002859.5, 68002859.5, null],
                        ["TQBR", "2018-08-07", "Система ао", "AFKS", 2459, 79244709.5, 9, 8.91, 9.14, 8.965, 9.025, 8.965, 8778900, 9.025, 9.025, 8.965, 79244709.5, 79244709.5, 79244709.5, null]],
                    "metadata": {
                        "ADMITTEDQUOTE": {"type": "double"},
                        "ADMITTEDVALUE": {"type": "double"},
                        "BOARDID": {"bytes": 12, "max_size": 0, "type": "string"},
                        "CLOSE": {"type": "double"},
                        "HIGH": {"type": "double"},
                        "LEGALCLOSEPRICE": {"type": "double"},
                        "LOW": {"type": "double"},
                        "MARKETPRICE2": {"type": "double"},
                        "MARKETPRICE3": {"type": "double"},
                        "MARKETPRICE3TRADESVALUE": {"type": "double"},
                        "MP2VALTRD": {"type": "double"},
                        "NUMTRADES": {"type": "double"},
                        "OPEN": {"type": "double"},
                        "SECID": {"bytes": 36, "max_size": 0, "type": "string"},
                        "SHORTNAME": {"bytes": 189, "max_size": 0, "type": "string"},
                        "TRADEDATE": {"bytes": 10, "max_size": 0, "type": "date"},
                        "VALUE": {"type": "double"},
                        "VOLUME": {"type": "double"},
                        "WAPRICE": {"type": "double"},
                        "WAVAL": {"type": "double"}
                    }
                }
            ).toEqual(result1);

        });

        it('Предобработка полученных данных с московской биржи', async () => {
            const priceType = 'WAPRICE';
            const result1 = await MoexHelper.getHistoryData('AFKS', '2018-08-01', 5);
            const df = MoexHelper.getPreprocessingPrices(result1);
            expect([9.075, 8.915, 8.86, 8.945, 9.025]).toEqual(df.select(priceType).toDict()[priceType]);

        });

        it('Склеивание торговых дней', async () => {
            const priceType = 'WAPRICE';
            const df = await MoexHelper.getDataFrame('AFKS', '2018-08-01', 5);
            const df1 = await MoexHelper.getDataFrame('AFKS', '2018-08-02', 5);
            const union = df.union(df1);
            const filterid = union.dropDuplicates();
            expect([9.075, 8.915, 8.86, 8.945, 9.025, 8.675]).toEqual(filterid.select(priceType).toDict()[priceType]);
        });

        it('Склеивание торговых дней 2', async () => {
            const priceType = 'WAPRICE';
            const df = await MoexHelper.getDataFrame('AFKS', '2018-08-01', 5);
            const df1 = await MoexHelper.getDataFrame('AFKS', '2018-08-05', 5);
            const union = df.union(df1);
            const filterid = union.dropDuplicates();
            expect([9.075, 8.915, 8.86, 8.945, 9.025, 8.675, 8.61, 8.59]).toEqual(filterid.select(priceType).toDict()[priceType]);
        });

        it('Склеивание котировок за n календарных дней', async () => {
            const priceType = 'WAPRICE';
            const countDays = 150;
            const df = await MoexHelper.getHistoryPrices('AFKS', countDays);
            const df1 = await MoexHelper.getHistoryPrices('AFKS', countDays + 15);
            expect(df1.toArray().length).toEqual(128);
            const union = df.union(df1);
            const filterid = union.dropDuplicates();
            expect(getTry()).toEqual(filterid.select(priceType).toDict()[priceType]);

            function getTry() {
                return [8.198, 8.17, 8.245, 8.162, 8.133, 8.059, 8.047, 8.08, 7.964, 7.995, 7.804, 7.906, 7.937, 7.835, 7.832, 7.78, 8.106, 8.354, 8.535, 8.557, 8.424, 8.48, 8.387, 8.363, 8.14, 8.103, 8.041, 8.003, 7.952, 8.04, 8.033, 7.995, 7.969, 7.927, 7.907, 7.819, 7.966, 7.944, 7.918, 7.99, 8.01, 8.132, 8.196, 8.289, 8.286, 8.389, 8.413, 8.489, 8.465, 8.724, 9.059, 9.494, 9.658, 9.576, 9.387, 9.323, 9.54, 9.516, 9.541, 9.603, 9.564, 9.623, 9.86, 10.123, 9.692, 9.579, 9.701, 9.724, 9.84, 9.507, 9.606, 9.652, 9.803, 9.768, 9.812, 9.808, 9.745, 9.688, 9.665, 9.861, 9.876, 9.907, 9.875, 9.865, 9.852, 9.798, 9.896, 9.878, 9.868, 9.892, 9.856, 9.852, 9.877, 9.858, 9.806, 9.762, 9.796, 9.753, 9.673, 9.561, 9.3, 9.383, 9.319, 9.13, 9.119, 9.01, 8.875, 8.61, 8.35, 8.305, 8.27, 8.12, 8.21, 8.37, 8.385, 8.33, 8.295, 8.195, 8.125, 7.95, 7.975, 7.895, 7.745, 7.825, 7.85, 7.83, 7.878, 8.114]
            }
        });

        class MoexHelper {
            static async getDataFrame(security, dateFrom, limit = 100) {
                const history = await MoexHelper.getHistoryData(security, dateFrom, limit);
                return MoexHelper.getPreprocessingPrices(history);
            }

            static async getActualIndices(security) {
                const url = MoexHelper.getUrlForActualIndices(security);
                const response = await axios.get(url);
                return response.data.indices.data;
            }

            static getUrlForActualIndices(security) {
                return `https://iss.moex.com/iss/securities/${security}/indices.json?only_actual=1`;
            }

            static getUrlForLoadData(security, dateFrom, limit = 100) {
                const baseUrl = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/';
                const params = `${security}.json?from=${dateFrom}&limit=${limit}`;
                return baseUrl + params;
            }

            static async getHistoryData(security, dateFrom, limit = 100) {
                //API https://iss.moex.com/iss/reference/65
                const url = MoexHelper.getUrlForLoadData(security, dateFrom, limit);
                const response = await axios.get(url);
                return response.data.history;
            }

            static getPreprocessingPrices(historyData) {
                const df = new DataFrame.DataFrame(historyData.data, historyData.columns);
                return df;
            }

            static async getHistoryPrices(security, limit = 100) {
                const dates = getDatesSplitLoad(limit);
                const dataframes = [];
                for (const date of dates) {
                    const df = await MoexHelper.getDataFrame(security, date, limit);
                    dataframes.push(df);
                }
                let unionDf = dataframes[0];
                for (const df of dataframes) {
                    unionDf = unionDf.union(df);
                }
                const cleanDf = unionDf.dropDuplicates();
                return cleanDf;


                /*
                * вернуть интервалы дат котоорыеми будем отправлять запросы
                *   вычисляем старотовую дату
                *   потом считаем количество дат которые будут
                *   к стартовой дате прибавляем 100
                *   если новая дата больше текущей тогда возвращаем массив
                *   иначе добавлем массив и прибаляем 100
                *
                *
                * пройтись по всем интервалам дат и плавненько их вырузим
                *
                * смержим все данные
                *
                * вернём пользователю
                *
                * */

                /*
                *
                * распичатать методические рекомендации
                * нужны данные по коэфицентам - есть
                * нужны данные котировок индексов
                *
                * в принципе этого достаточно чтобы расчитать манипуляции за этот период
                *
                * сделать также как на фронте
                *   на выходе иметь датафрейм с уникальными значениями
                *
                * получить в итоге [
                * {data: '15-01-2018'
                * instrument:
                * waprise:
                * openPrice:
                * closePrice:
                * esterdayPriceClose:
                * sigma:
                * betta:
                * f_minus:
                * f_plys:
                * fz_number:
                * difference:
                * }]
                *
                * вход [дата, название бумаги]
                * вход [название бумаги, количество торговых дней]
                *
                * задача - фильтрануть исходный датасет по бумаге
                *
                * в идеале к исходному датасету подмержить катеровки?
                *
                * подтянуть катеровки за указаные торговые периоды
                * просто можно в тупую стягивать их все
                * потом мержить по дате
                *
                * тогда расчитывать коэфиценты манипуляций по методикам и добавлять их как новые столбцы?
                *
                * допустим весь
                *
                *
                * */

                function getDatesSplitLoad(limit) {
                    const dates = [];
                    let dateLoss = new Date();
                    dateLoss.setDate(dateLoss.getDate() - limit);
                    const now = new Date()
                    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf();
                    while (dateLoss.valueOf() < today) {
                        dates.push(getDateFormat(dateLoss));
                        dateLoss.setDate(dateLoss.getDate() + 100);

                    }
                    return dates;

                    function getDateFormat(date) {
                        const year = date.getFullYear();
                        const month = getForMoexFormat(date.getMonth() + 1);
                        const day = getForMoexFormat(date.getDay());
                        const dateFormat = `${year}-${month}-${day}`;
                        return dateFormat;

                        function getForMoexFormat(dayOrMonth) {
                            if (dayOrMonth > 9) {
                                return dayOrMonth;
                            }
                            return `0${dayOrMonth}`;
                        }
                    }
                }
            }
        }

    }
);


