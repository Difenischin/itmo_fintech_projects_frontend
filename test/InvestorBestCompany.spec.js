// InvestorHelper
import InvestorHelper from '@/classes/InvestorHelper';

describe('MultiplikatorsBest', () => {

        const axios = require("axios");
        const DataFrame = require('dataframe-js');
        const accuracy = 5;

        const responceServer = getMultiplikatorsBest();

        it('Вывод столбца EV\/EBITDA мультипликаторов', async () => {
            const df = InvestorHelper.getMultiplicatorsDf(responceServer);
            expect([
                7.5146598782,
                0.8541510078,
                3.3172005413,
                8.7035288918,
                2.1279615454,
                2.1279615454,
                2.5478781999,
                1.9852308101,
                2.9289038634,
                4.440204602,
                6.6380030931,
                2.3496043309,
                3.1084226015,
                0.6146125887,
                2.7039549399,
                3.3197079536,
                4.6916804335,
                2.2083489336,
                0.8541510078,
                0.6146125887,
                1.9581735707,
                3.1084226015,
                1.5556963926,
                4.0298950956,
                5.3476309086,
                3.1065223611,
                2.0122476899,
                5.3476309086,
                2.9337334133,
                2.7039549399,
                4.7896998542,
                2.4109954837,
                2.9289038634,
                1.9852308101,
                4.7896998542,
                1.6886101226,
                1.9436161619,
                1.9436161619,
                4.9666911984,
                4.0298950956
            ]).toEqual(df.select("EV\/EBITDA").toDict()["EV\/EBITDA"]);

        });

        it('Нормировка EV\/EBITDA ', async () => {
            const columnPrefix = 'normalize';
            const df_ = InvestorHelper.getMultiplicatorsDf(responceServer);
            const df = InvestorHelper.getNormalizeMultiplicators(df_, ['EV\/EBITDA'], columnPrefix);
            expect([
                0.8530249332479337,
                0.029613165734969856,
                0.33411001564749276,
                1,
                0.1870892094803878,
                0.1870892094803878,
                0.23900180676354563,
                0.16944398607199382,
                0.28610646815730684,
                0.47294246471976453,
                0.7446474012954731,
                0.21449001042761207,
                0.308299643531269,
                0,
                0.2582969427436503,
                0.33441999688676444,
                0.5040314044586545,
                0.19702717708788947,
                0.029613165734969856,
                0,
                0.16609900902115815,
                0.308299643531269,
                0.11634238365642857,
                0.4222175602918682,
                0.5851239081415784,
                0.30806472449776734,
                0.17278397362875048,
                0.5851239081415784,
                0.2867035258741915,
                0.2582969427436503,
                0.5161491489162693,
                0.22207955029916102,
                0.28610646815730684,
                0.16944398607199382,
                0.5161491489162693,
                0.13277397041287234,
                0.16429933546112627,
                0.16429933546112627,
                0.5380298728065845,
                0.4222175602918682
            ]).toEqual(df.select(columnPrefix + "EV\/EBITDA").toDict()[columnPrefix + "EV\/EBITDA"]);

        });

        it('Расчёт мультикритерия EV\/EBITDA ', async () => {
            const columnPrefix = 'normalize_';
            const df_m = InvestorHelper.getMultiplicatorsDf(responceServer);
            const df_ = InvestorHelper.getNormalizeMultiplicators(df_m, ['EV\/EBITDA', 'Debt/EBITDA', 'P_current\/E'], columnPrefix);
            const ev = {
                name: `${columnPrefix}EV/EBITDA`,
                weight: 0.4
            };
            const dbt = {
                name: `${columnPrefix}Debt/EBITDA`,
                weight: 0.4
            };
            const pe = {
                name: `${columnPrefix}P_current/E`,
                weight: 0.2
            };
            const resultColumnName = 'supper_critery_';
            const df = InvestorHelper.getDFWithMulticriterion(df_, [ev, dbt, pe], resultColumnName);
            expect([
                0.49734614754405093,
                0.46347192462380427,
                0.660982116640592,
                0.7152859650784213,
                0.2523497398267641,
                0.2523497398267641,
                0.3116979134468617,
                0.2412443142302822,
                0.6026016020661376,
                0.5899606397888952,
                0.8352146007183843,
                0.2904924635419115,
                0.32500744727244607,
                0.3087498841325675,
                0.6637877428462766,
                0.45765178812428686,
                0.6775183282795193,
                0.6268156207073418,
                0.46347192462380427,
                0.3087498841325675,
                0.40655196578930164,
                0.32500744727244607,
                0.26518562825945646,
                0.6939088646623199,
                0.40210214301649083,
                0.48865082505943414,
                0.24226385772957593,
                0.40210214301649083,
                0.5970424734626162,
                0.6637877428462766,
                0.37749267551534227,
                0.6613639799374468,
                0.6026016020661376,
                0.2412443142302822,
                0.37749267551534227,
                0.37711544827487864,
                0.09882762727477176,
                0.09882762727477176,
                0.6097095290338163,
                0.6939088646623199
            ]).toEqual(df.select(resultColumnName).toDict()[resultColumnName]);

            const to_dict = df.toDict();
            const to_json = df.toJSON();
            const to_toArray = df.toArray();
            const to_toCollection = df.toCollection();
            const a = 1;

        });


        function getMultiplikatorsBest() {
            return {
                "schema": {
                    "fields": [{"name": "index", "type": "integer"}, {"name": "link", "type": "string"}, {
                        "name": "short_name",
                        "type": "string"
                    }, {"name": "ticker", "type": "string"}, {
                        "name": "EV\/EBITDA",
                        "type": "string"
                    }, {"name": "EV_curent\/EBITDA", "type": "string"}, {
                        "name": "Debt\/EBITDA",
                        "type": "string"
                    }, {"name": "E\/P", "type": "string"}, {"name": "P\/E", "type": "string"}, {
                        "name": "P_current\/E",
                        "type": "string"
                    }, {"name": "P\/B", "type": "string"}, {"name": "P\/S", "type": "string"}, {
                        "name": "L\/A",
                        "type": "string"
                    }, {"name": "capital", "type": "string"}, {"name": "capitalization", "type": "string"}, {
                        "name": "earnings",
                        "type": "string"
                    }, {"name": "ebitda", "type": "string"}, {"name": "revenue", "type": "string"}, {
                        "name": "total_cash",
                        "type": "string"
                    }, {"name": "total_debt", "type": "string"}, {
                        "name": "total_liabilities",
                        "type": "string"
                    }, {"name": "last_fiscal_year", "type": "string"}, {
                        "name": "last_fiscal_quarter",
                        "type": "string"
                    }, {"name": "sector_id", "type": "string"}, {
                        "name": "sector_name",
                        "type": "string"
                    }, {"name": "company_url", "type": "string"}, {"name": "company_desc", "type": "string"}, {
                        "name": "assets",
                        "type": "string"
                    }, {"name": "current_capitalisacion", "type": "number"}],
                    "primaryKey": ["index"],
                    "pandas_version": "0.20.0"
                },
                "data": [{
                    "index": 8,
                    "link": "https:\/\/tezis.io\/company\/180",
                    "short_name": "iQIWI",
                    "ticker": "QIWI",
                    "EV\/EBITDA": 7.5146598782,
                    "EV_curent\/EBITDA": -6.7192341166,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.0419694733,
                    "P\/E": 23.8268417832,
                    "P_current\/E": 0.0,
                    "P\/B": 2.9522264703,
                    "P\/S": 3.0523972004,
                    "L\/A": 0.6152382275,
                    "capital": 27699000000,
                    "capitalization": 81773721000,
                    "earnings": 3432000000,
                    "ebitda": 5745000000,
                    "revenue": 26790000000,
                    "total_cash": 38602000000,
                    "total_debt": 0,
                    "total_liabilities": 44291000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "1",
                    "sector_name": "Технологии",
                    "company_url": "www.qiwi.com",
                    "company_desc": "Международный платежный сервис. Лидер российского рынка моментальных платежей",
                    "assets": 71990000000,
                    "current_capitalisacion": 0.0
                }, {
                    "index": 20,
                    "link": "https:\/\/tezis.io\/company\/171",
                    "short_name": "ЧелябЭС ап",
                    "ticker": "CLSBP",
                    "EV\/EBITDA": 0.8541510078,
                    "EV_curent\/EBITDA": 0.4586461487,
                    "Debt\/EBITDA": 0.8486073458,
                    "E\/P": 1.9348820389,
                    "P\/E": 0.5168273724,
                    "P_current\/E": 0.0,
                    "P\/B": 0.175655751,
                    "P\/S": 0.0232865834,
                    "L\/A": 0.7909022197,
                    "capital": 2749870000,
                    "capitalization": 483030480,
                    "earnings": 934607000,
                    "ebitda": 1221301000,
                    "revenue": 20742866000,
                    "total_cash": 476260000,
                    "total_debt": 1036405000,
                    "total_liabilities": 10401250000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 4,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/www.esbt74.ru\/",
                    "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Челябинскую область",
                    "assets": 13151120000,
                    "current_capitalisacion": 0.0
                }, {
                    "index": 25,
                    "link": "https:\/\/tezis.io\/company\/128",
                    "short_name": "СЗПароход",
                    "ticker": "SZPR",
                    "EV\/EBITDA": 3.3172005413,
                    "EV_curent\/EBITDA": 3.1613974839,
                    "Debt\/EBITDA": 1.0174453247,
                    "E\/P": 0.3251234419,
                    "P\/E": 3.0757548398,
                    "P_current\/E": 2.8797508549,
                    "P\/B": 0.7457437827,
                    "P\/S": 0.5513557943,
                    "L\/A": 0.474097955,
                    "capital": 11505209000,
                    "capitalization": 8579938080,
                    "earnings": 2789539000,
                    "ebitda": 3509307000,
                    "revenue": 15561527000,
                    "total_cash": 509391000,
                    "total_debt": 3570528000,
                    "total_liabilities": 10371886000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "9",
                    "sector_name": "Транспорт",
                    "company_url": "www.nwsc.spb.ru",
                    "company_desc": "Судоходная компания. Перевозит грузы по речным и морским маршутам. Входит в UCL Holding",
                    "assets": 21877095000,
                    "current_capitalisacion": 8033177320.0
                }, {
                    "index": 29,
                    "link": "https:\/\/tezis.io\/company\/95",
                    "short_name": "Новатэк ао",
                    "ticker": "NVTK",
                    "EV\/EBITDA": 8.7035288918,
                    "EV_curent\/EBITDA": 8.8181725846,
                    "Debt\/EBITDA": 0.3347446482,
                    "E\/P": 0.1398301097,
                    "P\/E": 7.1515355491,
                    "P_current\/E": 7.2482366527,
                    "P\/B": 3.1174997706,
                    "P\/S": 4.3963971024,
                    "L\/A": 0.2044927855,
                    "capital": 1282115000000,
                    "capitalization": 3996993218400,
                    "earnings": 558900000000,
                    "ebitda": 471428000000,
                    "revenue": 909152000000,
                    "total_cash": 51714000000,
                    "total_debt": 157808000000,
                    "total_liabilities": 329580000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.novatek.ru",
                    "company_desc": "Крупнейший независимый производитель природного газа в России",
                    "assets": 1611695000000,
                    "current_capitalisacion": 4051039465200.0
                }, {
                    "index": 50,
                    "link": "https:\/\/tezis.io\/company\/423",
                    "short_name": "Телеграф",
                    "ticker": "CNTL",
                    "EV\/EBITDA": 2.1279615454,
                    "EV_curent\/EBITDA": 0.6773486652,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.1663908845,
                    "P\/E": 6.0099446121,
                    "P_current\/E": 3.6382602589,
                    "P\/B": 1.8422489664,
                    "P\/S": 1.8947281768,
                    "L\/A": 0.3688340651,
                    "capital": 2585072000,
                    "capitalization": 4762346220,
                    "earnings": 792411000,
                    "ebitda": 1295555000,
                    "revenue": 2513472000,
                    "total_cash": 2005455000,
                    "total_debt": 0,
                    "total_liabilities": 1510637000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 4,
                    "sector_id": "13",
                    "sector_name": "Связь",
                    "company_url": "http:\/\/www.moscow.cnt.ru",
                    "company_desc": "Телекоммуникационная компания. Предоставляет услуги мобильной связи и доступа в интернет. Входит в «Ростелеком»",
                    "assets": 4095709000,
                    "current_capitalisacion": 2882997450.0
                }, {
                    "index": 73,
                    "link": "https:\/\/tezis.io\/company\/423",
                    "short_name": "Телеграф",
                    "ticker": "CNTL",
                    "EV\/EBITDA": 2.1279615454,
                    "EV_curent\/EBITDA": 0.6773486652,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.1663908845,
                    "P\/E": 6.0099446121,
                    "P_current\/E": 3.6382602589,
                    "P\/B": 1.8422489664,
                    "P\/S": 1.8947281768,
                    "L\/A": 0.3688340651,
                    "capital": 2585072000,
                    "capitalization": 4762346220,
                    "earnings": 792411000,
                    "ebitda": 1295555000,
                    "revenue": 2513472000,
                    "total_cash": 2005455000,
                    "total_debt": 0,
                    "total_liabilities": 1510637000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 4,
                    "sector_id": "13",
                    "sector_name": "Связь",
                    "company_url": "http:\/\/www.moscow.cnt.ru",
                    "company_desc": "Телекоммуникационная компания. Предоставляет услуги мобильной связи и доступа в интернет. Входит в «Ростелеком»",
                    "assets": 4095709000,
                    "current_capitalisacion": 2882997450.0
                }, {
                    "index": 96,
                    "link": "https:\/\/tezis.io\/company\/471",
                    "short_name": "Омскшина",
                    "ticker": "OMSH",
                    "EV\/EBITDA": 2.5478781999,
                    "EV_curent\/EBITDA": 0.1715796418,
                    "Debt\/EBITDA": 0.1721996538,
                    "E\/P": 0.0340441928,
                    "P\/E": 29.3735852851,
                    "P_current\/E": 0.0,
                    "P\/B": 0.3173415082,
                    "P\/S": 0.0431716261,
                    "L\/A": 0.7322048883,
                    "capital": 1509677000,
                    "capitalization": 479083176,
                    "earnings": 16310000,
                    "ebitda": 201609000,
                    "revenue": 11097177000,
                    "total_cash": 125000,
                    "total_debt": 34717000,
                    "total_liabilities": 4127756000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 2,
                    "sector_id": "8",
                    "sector_name": "Промышленные товары",
                    "company_url": "www.omsktyre.ru",
                    "company_desc": "Машиностоительная компания. Производит шины для грузовых автомобилей, автобусов, троллейбусов и специальной техники",
                    "assets": 5637433000,
                    "current_capitalisacion": 0.0
                }, {
                    "index": 111,
                    "link": "https:\/\/tezis.io\/company\/143",
                    "short_name": "Сургнфгз",
                    "ticker": "SNGS",
                    "EV\/EBITDA": 1.9852308101,
                    "EV_curent\/EBITDA": 2.2508291177,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.3745916468,
                    "P\/E": 2.669573677,
                    "P_current\/E": 2.9494519515,
                    "P\/B": 0.2743854469,
                    "P\/S": 0.7271519368,
                    "L\/A": 0.1512666823,
                    "capital": 4327984000000,
                    "capitalization": 1187535824067,
                    "earnings": 444841000000,
                    "ebitda": 468758000000,
                    "revenue": 1633133000000,
                    "total_cash": 256943000000,
                    "total_debt": 0,
                    "total_liabilities": 771361000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.surgutneftegas.ru",
                    "company_desc": "Нефтегазодобывающая компания. Одна из крупнейших в России",
                    "assets": 5099345000000,
                    "current_capitalisacion": 1312037155541.125
                }, {
                    "index": 112,
                    "link": "https:\/\/tezis.io\/company\/20",
                    "short_name": "Башнефт ао",
                    "ticker": "BANE",
                    "EV\/EBITDA": 2.9289038634,
                    "EV_curent\/EBITDA": 2.567930622,
                    "Debt\/EBITDA": 0.90825075,
                    "E\/P": 0.313220507,
                    "P\/E": 3.1926389796,
                    "P_current\/E": 2.6828689637,
                    "P\/B": 0.6793471424,
                    "P\/S": 0.3550113096,
                    "L\/A": 0.4009326873,
                    "capital": 468110000000,
                    "capitalization": 318009190844,
                    "earnings": 99607000000,
                    "ebitda": 140666000000,
                    "revenue": 895772000000,
                    "total_cash": 33772000000,
                    "total_debt": 127760000000,
                    "total_liabilities": 313288000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.bashneft.ru",
                    "company_desc": "Нефтяная компания. Головной офис расположен в Башкирии",
                    "assets": 781398000000,
                    "current_capitalisacion": 267232528867.5
                }, {
                    "index": 117,
                    "link": "https:\/\/tezis.io\/company\/170",
                    "short_name": "ЧЦЗ ао",
                    "ticker": "CHZN",
                    "EV\/EBITDA": 4.440204602,
                    "EV_curent\/EBITDA": -0.0088692868,
                    "Debt\/EBITDA": 0.7025933475,
                    "E\/P": 0.1610976674,
                    "P\/E": 6.2074145233,
                    "P_current\/E": 0.0,
                    "P\/B": 0.8636169634,
                    "P\/S": 1.45970106,
                    "L\/A": 0.2402907357,
                    "capital": 30372931000,
                    "capitalization": 26230578440,
                    "earnings": 4225685000,
                    "ebitda": 5895739000,
                    "revenue": 17969829000,
                    "total_cash": 4194598000,
                    "total_debt": 4142307000,
                    "total_liabilities": 9606746000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 2,
                    "sector_id": "7",
                    "sector_name": "Металлургия",
                    "company_url": "www.zinc.ru",
                    "company_desc": "Металлургическая компания. Добывает и обогащает руду, производит цинк и сплавы на его основе. Входит в УГМК",
                    "assets": 39979677000,
                    "current_capitalisacion": 0.0
                }, {
                    "index": 118,
                    "link": "https:\/\/tezis.io\/company\/2",
                    "short_name": "АЛРОСА ао",
                    "ticker": "ALRS",
                    "EV\/EBITDA": 6.6380030931,
                    "EV_curent\/EBITDA": 6.5525578011,
                    "Debt\/EBITDA": 0.9688382193,
                    "E\/P": 0.1319595268,
                    "P\/E": 7.5780811289,
                    "P_current\/E": 7.4650994714,
                    "P\/B": 2.0782993794,
                    "P\/S": 2.0805275652,
                    "L\/A": 0.4071849718,
                    "capital": 253975000000,
                    "capitalization": 527836084873,
                    "earnings": 69653000000,
                    "ebitda": 92100000000,
                    "revenue": 253703000000,
                    "total_cash": 5706000000,
                    "total_debt": 89230000000,
                    "total_liabilities": 174447000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "12",
                    "sector_name": "Драгоценности",
                    "company_url": "http:\/\/www.alrosa.ru",
                    "company_desc": "Алмазодобывающая компания. Добывает, перерабатывает и продает драгоценные камни",
                    "assets": 428422000000,
                    "current_capitalisacion": 519966573477.9999389648
                }, {
                    "index": 121,
                    "link": "https:\/\/tezis.io\/company\/45",
                    "short_name": "ИнтерРАОао",
                    "ticker": "IRAO",
                    "EV\/EBITDA": 2.3496043309,
                    "EV_curent\/EBITDA": 2.5125242829,
                    "Debt\/EBITDA": 0.0442163753,
                    "E\/P": 0.1864423022,
                    "P\/E": 5.3635896364,
                    "P_current\/E": 5.6440933069,
                    "P\/B": 0.843744886,
                    "P\/S": 0.4266438697,
                    "L\/A": 0.2999623549,
                    "capital": 516961000000,
                    "capitalization": 436183200000,
                    "earnings": 81323000000,
                    "ebitda": 140016000000,
                    "revenue": 1022359000000,
                    "total_cash": 113392000000,
                    "total_debt": 6191000000,
                    "total_liabilities": 221515000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.interrao.ru",
                    "company_desc": "Энергетическая компания. Работает в России, а также в странах Европы и СНГ",
                    "assets": 738476000000,
                    "current_capitalisacion": 458994599999.9999389648
                }, {
                    "index": 125,
                    "link": "https:\/\/tezis.io\/company\/60",
                    "short_name": "КрасОкт-ао",
                    "ticker": "KROT",
                    "EV\/EBITDA": 3.1084226015,
                    "EV_curent\/EBITDA": 2.640176758,
                    "Debt\/EBITDA": 0.0315278076,
                    "E\/P": 0.1443770453,
                    "P\/E": 6.926308803,
                    "P_current\/E": 5.8839513568,
                    "P\/B": 0.4121879981,
                    "P\/S": 0.2135263187,
                    "L\/A": 0.2254365376,
                    "capital": 7989382000,
                    "capitalization": 3293127373,
                    "earnings": 475452000,
                    "ebitda": 1058399000,
                    "revenue": 15422583000,
                    "total_cash": 36545000,
                    "total_debt": 33369000,
                    "total_liabilities": 2325308000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "5",
                    "sector_name": "Потребительские товары",
                    "company_url": "http:\/\/www.konfetki.ru",
                    "company_desc": "Производит конфеты, какао, шоколад. Владеет брендами «Аленка», «Мишка косолапый», «Золотой Ярлык». Входит в «Объединенные кондитеры",
                    "assets": 10314690000,
                    "current_capitalisacion": 2797536440.5
                }, {
                    "index": 126,
                    "link": "https:\/\/tezis.io\/company\/66",
                    "short_name": "Лензол. ап",
                    "ticker": "LNZLP",
                    "EV\/EBITDA": 0.6146125887,
                    "EV_curent\/EBITDA": -2.0564397163,
                    "Debt\/EBITDA": 0.4287825059,
                    "E\/P": 0.1859179946,
                    "P\/E": 5.3787155035,
                    "P_current\/E": 0.5631369206,
                    "P\/B": 0.621015501,
                    "P\/S": 0.7912727486,
                    "L\/A": 0.245159493,
                    "capital": 16257000000,
                    "capitalization": 10095849000,
                    "earnings": 1877000000,
                    "ebitda": 3384000000,
                    "revenue": 12759000000,
                    "total_cash": 9467000000,
                    "total_debt": 1451000000,
                    "total_liabilities": 5280000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "12",
                    "sector_name": "Драгоценности",
                    "company_url": "http:\/\/www.len-zoloto.ru",
                    "company_desc": "Золотодобывающая компания. Разрабатывает рудники в Иркутской области. Входит в «Полюс»",
                    "assets": 21537000000,
                    "current_capitalisacion": 1057008000.0
                }, {
                    "index": 133,
                    "link": "https:\/\/tezis.io\/company\/67",
                    "short_name": "Ленэнерго",
                    "ticker": "LSNG",
                    "EV\/EBITDA": 2.7039549399,
                    "EV_curent\/EBITDA": 2.729464769,
                    "Debt\/EBITDA": 1.0859796666,
                    "E\/P": 0.2273857846,
                    "P\/E": 4.3978122981,
                    "P_current\/E": 4.4568429283,
                    "P\/B": 0.4298050138,
                    "P\/S": 0.7204775238,
                    "L\/A": 0.3648790429,
                    "capital": 136983553000,
                    "capitalization": 58876217889,
                    "earnings": 13387615000,
                    "ebitda": 30979406000,
                    "revenue": 81718327000,
                    "total_cash": 8752305000,
                    "total_debt": 33643005000,
                    "total_liabilities": 78697494000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.lenenergo.ru",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Санкт-Петербурге и Ленинградской области. Входит в «Россети»",
                    "assets": 215681047000,
                    "current_capitalisacion": 59666497240.0
                }, {
                    "index": 136,
                    "link": "https:\/\/tezis.io\/company\/71",
                    "short_name": "ММК",
                    "ticker": "MAGN",
                    "EV\/EBITDA": 3.3197079536,
                    "EV_curent\/EBITDA": 2.9590989646,
                    "Debt\/EBITDA": 0.3870559731,
                    "E\/P": 0.1589547202,
                    "P\/E": 6.291099747,
                    "P_current\/E": 5.611484916,
                    "P\/B": 1.3641830213,
                    "P\/S": 0.9097406981,
                    "L\/A": 0.3272332029,
                    "capital": 345383000000,
                    "capitalization": 471165624450,
                    "earnings": 74894000000,
                    "ebitda": 141147544000,
                    "revenue": 517912000000,
                    "total_cash": 57229000000,
                    "total_debt": 54632000000,
                    "total_liabilities": 167994000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "7",
                    "sector_name": "Металлургия",
                    "company_url": "www.mmk.ru",
                    "company_desc": "Производитель стали. Добывает и перерабатывает сырье, продает изделия из металла. Больше 70% продукции продает в России",
                    "assets": 513377000000,
                    "current_capitalisacion": 420266551300.0
                }, {
                    "index": 141,
                    "link": "https:\/\/tezis.io\/company\/93",
                    "short_name": "НЛМК ао",
                    "ticker": "NLMK",
                    "EV\/EBITDA": 4.6916804335,
                    "EV_curent\/EBITDA": 4.0798509547,
                    "Debt\/EBITDA": 0.8153976645,
                    "E\/P": 0.1401018456,
                    "P\/E": 7.1376647139,
                    "P_current\/E": 6.1000389024,
                    "P\/B": 2.4301269969,
                    "P\/S": 1.1764949309,
                    "L\/A": 0.4607674788,
                    "capital": 375260000000,
                    "capitalization": 911929456838,
                    "earnings": 127763000000,
                    "ebitda": 216678324800,
                    "revenue": 775124000000,
                    "total_cash": 72023000000,
                    "total_debt": 176679000000,
                    "total_liabilities": 320655000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "7",
                    "sector_name": "Металлургия",
                    "company_url": "http:\/\/www.nlmk.com",
                    "company_desc": "Металлургическая компания. Добывает, перерабатывает и продает сталь в России, Северной Америке и странах ЕС",
                    "assets": 695915000000,
                    "current_capitalisacion": 779359270289.5999755859
                }, {
                    "index": 145,
                    "link": "https:\/\/tezis.io\/company\/156",
                    "short_name": "Таттел. ао",
                    "ticker": "TTLK",
                    "EV\/EBITDA": 2.2083489336,
                    "EV_curent\/EBITDA": 2.2926466717,
                    "Debt\/EBITDA": 1.019386181,
                    "E\/P": 0.1690366019,
                    "P\/E": 5.9158785048,
                    "P_current\/E": 6.2819499205,
                    "P\/B": 0.6242041877,
                    "P\/S": 0.3923966775,
                    "L\/A": 0.5214477167,
                    "capital": 5927752000,
                    "capitalization": 3700127622,
                    "earnings": 625457000,
                    "ebitda": 2716110000,
                    "revenue": 9429559000,
                    "total_cash": 470774000,
                    "total_debt": 2768765000,
                    "total_liabilities": 6459091000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "13",
                    "sector_name": "Связь",
                    "company_url": "http:\/\/www.tattelecom.ru",
                    "company_desc": "Телекоммуникационная компания. Предоставляет услуги интернета, телефонии, телевидения. Работает в республике Татарстан",
                    "assets": 12386843000,
                    "current_capitalisacion": 3929089551.4000000954
                }, {
                    "index": 146,
                    "link": "https:\/\/tezis.io\/company\/171",
                    "short_name": "ЧелябЭС ап",
                    "ticker": "CLSBP",
                    "EV\/EBITDA": 0.8541510078,
                    "EV_curent\/EBITDA": 0.4586461487,
                    "Debt\/EBITDA": 0.8486073458,
                    "E\/P": 1.9348820389,
                    "P\/E": 0.5168273724,
                    "P_current\/E": 0.0,
                    "P\/B": 0.175655751,
                    "P\/S": 0.0232865834,
                    "L\/A": 0.7909022197,
                    "capital": 2749870000,
                    "capitalization": 483030480,
                    "earnings": 934607000,
                    "ebitda": 1221301000,
                    "revenue": 20742866000,
                    "total_cash": 476260000,
                    "total_debt": 1036405000,
                    "total_liabilities": 10401250000,
                    "last_fiscal_year": 2018,
                    "last_fiscal_quarter": 4,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/www.esbt74.ru\/",
                    "company_desc": "Энергосбытовая компания. Обеспечивает электричеством Челябинскую область",
                    "assets": 13151120000,
                    "current_capitalisacion": 0.0
                }, {
                    "index": 149,
                    "link": "https:\/\/tezis.io\/company\/66",
                    "short_name": "Лензол. ап",
                    "ticker": "LNZLP",
                    "EV\/EBITDA": 0.6146125887,
                    "EV_curent\/EBITDA": -2.0564397163,
                    "Debt\/EBITDA": 0.4287825059,
                    "E\/P": 0.1859179946,
                    "P\/E": 5.3787155035,
                    "P_current\/E": 0.5631369206,
                    "P\/B": 0.621015501,
                    "P\/S": 0.7912727486,
                    "L\/A": 0.245159493,
                    "capital": 16257000000,
                    "capitalization": 10095849000,
                    "earnings": 1877000000,
                    "ebitda": 3384000000,
                    "revenue": 12759000000,
                    "total_cash": 9467000000,
                    "total_debt": 1451000000,
                    "total_liabilities": 5280000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "12",
                    "sector_name": "Драгоценности",
                    "company_url": "http:\/\/www.len-zoloto.ru",
                    "company_desc": "Золотодобывающая компания. Разрабатывает рудники в Иркутской области. Входит в «Полюс»",
                    "assets": 21537000000,
                    "current_capitalisacion": 1057008000.0
                }, {
                    "index": 150,
                    "link": "https:\/\/tezis.io\/company\/75",
                    "short_name": "МРСКВол",
                    "ticker": "MRKV",
                    "EV\/EBITDA": 1.9581735707,
                    "EV_curent\/EBITDA": 1.953080784,
                    "Debt\/EBITDA": 0.4644664907,
                    "E\/P": 0.2632750787,
                    "P\/E": 3.7983086167,
                    "P_current\/E": 3.7860030811,
                    "P\/B": 0.4621547778,
                    "P\/S": 0.2729429738,
                    "L\/A": 0.2990634691,
                    "capital": 37730470000,
                    "capitalization": 17437316979,
                    "earnings": 4590811000,
                    "ebitda": 11092628000,
                    "revenue": 63886301000,
                    "total_cash": 868180000,
                    "total_debt": 5152154000,
                    "total_liabilities": 16098184000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.mrsk-volgi.ru",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Приволжском федеральном округе. Входит в «Россети»",
                    "assets": 53828654000,
                    "current_capitalisacion": 17380824590.9636001587
                }, {
                    "index": 155,
                    "link": "https:\/\/tezis.io\/company\/60",
                    "short_name": "КрасОкт-ао",
                    "ticker": "KROT",
                    "EV\/EBITDA": 3.1084226015,
                    "EV_curent\/EBITDA": 2.640176758,
                    "Debt\/EBITDA": 0.0315278076,
                    "E\/P": 0.1443770453,
                    "P\/E": 6.926308803,
                    "P_current\/E": 5.8839513568,
                    "P\/B": 0.4121879981,
                    "P\/S": 0.2135263187,
                    "L\/A": 0.2254365376,
                    "capital": 7989382000,
                    "capitalization": 3293127373,
                    "earnings": 475452000,
                    "ebitda": 1058399000,
                    "revenue": 15422583000,
                    "total_cash": 36545000,
                    "total_debt": 33369000,
                    "total_liabilities": 2325308000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "5",
                    "sector_name": "Потребительские товары",
                    "company_url": "http:\/\/www.konfetki.ru",
                    "company_desc": "Производит конфеты, какао, шоколад. Владеет брендами «Аленка», «Мишка косолапый», «Золотой Ярлык». Входит в «Объединенные кондитеры",
                    "assets": 10314690000,
                    "current_capitalisacion": 2797536440.5
                }, {
                    "index": 158,
                    "link": "https:\/\/tezis.io\/company\/89",
                    "short_name": "+МосЭнерго",
                    "ticker": "MSNG",
                    "EV\/EBITDA": 1.5556963926,
                    "EV_curent\/EBITDA": 1.5246731967,
                    "Debt\/EBITDA": 0.1007702347,
                    "E\/P": 0.2113577023,
                    "P\/E": 4.7313156285,
                    "P_current\/E": 4.6671800166,
                    "P\/B": 0.3177813285,
                    "P\/S": 0.4585124314,
                    "L\/A": 0.1824598912,
                    "capital": 281439000000,
                    "capitalization": 89436059325,
                    "earnings": 18903000000,
                    "ebitda": 39079000000,
                    "revenue": 195057000000,
                    "total_cash": 32579000000,
                    "total_debt": 3938000000,
                    "total_liabilities": 62812000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.mosenergo.ru",
                    "company_desc": "Энергетическая компания. Обеспечивает электроэнергией и теплом московский регион",
                    "assets": 344251000000,
                    "current_capitalisacion": 88223703854.1499938965
                }, {
                    "index": 164,
                    "link": "https:\/\/tezis.io\/company\/97",
                    "short_name": "НКНХ ао",
                    "ticker": "NKNC",
                    "EV\/EBITDA": 4.0298950956,
                    "EV_curent\/EBITDA": 4.3027678229,
                    "Debt\/EBITDA": 0.9624952836,
                    "E\/P": 0.1867906175,
                    "P\/E": 5.3535879551,
                    "P_current\/E": 5.7419199857,
                    "P\/B": 1.0937155694,
                    "P\/S": 0.7596625109,
                    "L\/A": 0.2902994758,
                    "capital": 136738000000,
                    "capitalization": 149552479525,
                    "earnings": 27935000000,
                    "ebitda": 39755000000,
                    "revenue": 196867000000,
                    "total_cash": 27608000000,
                    "total_debt": 38264000000,
                    "total_liabilities": 55932000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "6",
                    "sector_name": "Химия и агрохимия",
                    "company_url": "www.nknh.ru",
                    "company_desc": "Нефтехимическая компания. Производит и продает пластик и синтетический каучук. Входит в ТАИФ",
                    "assets": 192670000000,
                    "current_capitalisacion": 160400534800.0
                }, {
                    "index": 173,
                    "link": "https:\/\/tezis.io\/company\/107",
                    "short_name": "ПермьЭнС-п",
                    "ticker": "PMSBP",
                    "EV\/EBITDA": 5.3476309086,
                    "EV_curent\/EBITDA": 0.9100575913,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.1174113319,
                    "P\/E": 8.5170654618,
                    "P_current\/E": 2.0280299536,
                    "P\/B": 3.5839065836,
                    "P\/S": 0.0970964257,
                    "L\/A": 0.7392488899,
                    "capital": 1128196000,
                    "capitalization": 4043349072,
                    "earnings": 474735000,
                    "ebitda": 694202000,
                    "revenue": 41642615000,
                    "total_cash": 331013000,
                    "total_debt": 0,
                    "total_liabilities": 3198520000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/permenergosbyt.ru\/",
                    "company_desc": "Энергетическая компания. Обеспечивает электричеством Пермский край",
                    "assets": 4326716000,
                    "current_capitalisacion": 962776800.0
                }, {
                    "index": 177,
                    "link": "https:\/\/tezis.io\/company\/65",
                    "short_name": "ЛУКОЙЛ",
                    "ticker": "LKOH",
                    "EV\/EBITDA": 3.1065223611,
                    "EV_curent\/EBITDA": 3.2454135077,
                    "Debt\/EBITDA": 0.5041175208,
                    "E\/P": 0.1821129507,
                    "P\/E": 5.4910976746,
                    "P_current\/E": 5.7441582886,
                    "P\/B": 0.9569124985,
                    "P\/S": 0.4453727472,
                    "L\/A": 0.3430478684,
                    "capital": 3874952000000,
                    "capitalization": 3707990000000,
                    "earnings": 675273000000,
                    "ebitda": 1230352000000,
                    "revenue": 8325588000000,
                    "total_cash": 506116000000,
                    "total_debt": 620242000000,
                    "total_liabilities": 2023426000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.lukoil.ru",
                    "company_desc": "Нефтяная компания. Крупнейшая частная компания в стране",
                    "assets": 5898378000000,
                    "current_capitalisacion": 3878875000000.0
                }, {
                    "index": 191,
                    "link": "https:\/\/tezis.io\/company\/120",
                    "short_name": "Распадская",
                    "ticker": "RASP",
                    "EV\/EBITDA": 2.0122476899,
                    "EV_curent\/EBITDA": 1.8259827748,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.3178581384,
                    "P\/E": 3.146057562,
                    "P_current\/E": 2.8955956802,
                    "P\/B": 1.1642768063,
                    "P\/S": 1.2278526154,
                    "L\/A": 0.3488354411,
                    "capital": 77079000000,
                    "capitalization": 89741291956,
                    "earnings": 28525000000,
                    "ebitda": 38356258200,
                    "revenue": 73088000000,
                    "total_cash": 12559000000,
                    "total_debt": 0,
                    "total_liabilities": 41292000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "18",
                    "sector_name": "Уголь",
                    "company_url": "www.raspadskaya.ru",
                    "company_desc": "Угольная компания. Добывает и обрабатывает уголь. Владеет шахтами в Кемеровской области",
                    "assets": 118371000000,
                    "current_capitalisacion": 82596866777.3199920654
                }, {
                    "index": 207,
                    "link": "https:\/\/tezis.io\/company\/107",
                    "short_name": "ПермьЭнС-п",
                    "ticker": "PMSBP",
                    "EV\/EBITDA": 5.3476309086,
                    "EV_curent\/EBITDA": 0.9100575913,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.1174113319,
                    "P\/E": 8.5170654618,
                    "P_current\/E": 2.0280299536,
                    "P\/B": 3.5839065836,
                    "P\/S": 0.0970964257,
                    "L\/A": 0.7392488899,
                    "capital": 1128196000,
                    "capitalization": 4043349072,
                    "earnings": 474735000,
                    "ebitda": 694202000,
                    "revenue": 41642615000,
                    "total_cash": 331013000,
                    "total_debt": 0,
                    "total_liabilities": 3198520000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/permenergosbyt.ru\/",
                    "company_desc": "Энергетическая компания. Обеспечивает электричеством Пермский край",
                    "assets": 4326716000,
                    "current_capitalisacion": 962776800.0
                }, {
                    "index": 219,
                    "link": "https:\/\/tezis.io\/company\/32",
                    "short_name": "Газпрнефть",
                    "ticker": "SIBN",
                    "EV\/EBITDA": 2.9337334133,
                    "EV_curent\/EBITDA": 2.9165048741,
                    "Debt\/EBITDA": 0.8617269566,
                    "E\/P": 0.2229486234,
                    "P\/E": 4.485338303,
                    "P_current\/E": 4.4531531911,
                    "P\/B": 0.9521046341,
                    "P\/S": 0.7853112977,
                    "L\/A": 0.4195265479,
                    "capital": 2116668000000,
                    "capitalization": 2015289411557,
                    "earnings": 449306000000,
                    "ebitda": 839361000000,
                    "revenue": 2566230000000,
                    "total_cash": 276128000000,
                    "total_debt": 723300000000,
                    "total_liabilities": 1529783000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.gazprom-neft.ru",
                    "company_desc": "Нефтяная компания. Контролируется государством",
                    "assets": 3646451000000,
                    "current_capitalisacion": 2000828447658.0
                }, {
                    "index": 223,
                    "link": "https:\/\/tezis.io\/company\/67",
                    "short_name": "Ленэнерго",
                    "ticker": "LSNG",
                    "EV\/EBITDA": 2.7039549399,
                    "EV_curent\/EBITDA": 2.729464769,
                    "Debt\/EBITDA": 1.0859796666,
                    "E\/P": 0.2273857846,
                    "P\/E": 4.3978122981,
                    "P_current\/E": 4.4568429283,
                    "P\/B": 0.4298050138,
                    "P\/S": 0.7204775238,
                    "L\/A": 0.3648790429,
                    "capital": 136983553000,
                    "capitalization": 58876217889,
                    "earnings": 13387615000,
                    "ebitda": 30979406000,
                    "revenue": 81718327000,
                    "total_cash": 8752305000,
                    "total_debt": 33643005000,
                    "total_liabilities": 78697494000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.lenenergo.ru",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Санкт-Петербурге и Ленинградской области. Входит в «Россети»",
                    "assets": 215681047000,
                    "current_capitalisacion": 59666497240.0
                }, {
                    "index": 228,
                    "link": "https:\/\/tezis.io\/company\/155",
                    "short_name": "Татнфт 3ап",
                    "ticker": "TATNP",
                    "EV\/EBITDA": 4.7896998542,
                    "EV_curent\/EBITDA": 0.1352254543,
                    "Debt\/EBITDA": 0.0356663442,
                    "E\/P": 0.133428693,
                    "P\/E": 7.4946398508,
                    "P_current\/E": 0.4216568231,
                    "P\/B": 2.0215776588,
                    "P\/S": 1.7668071023,
                    "L\/A": 0.352541018,
                    "capital": 819663000000,
                    "capitalization": 1657012408530,
                    "earnings": 221093000000,
                    "ebitda": 335975000000,
                    "revenue": 937857000000,
                    "total_cash": 59776000000,
                    "total_debt": 11983000000,
                    "total_liabilities": 446306000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.tatneft.ru",
                    "company_desc": "Нефтяная компания. Головной офис находится в Татарстане",
                    "assets": 1265969000000,
                    "current_capitalisacion": 93225372000.0
                }, {
                    "index": 236,
                    "link": "https:\/\/tezis.io\/company\/80",
                    "short_name": "МРСК ЦП",
                    "ticker": "MRKP",
                    "EV\/EBITDA": 2.4109954837,
                    "EV_curent\/EBITDA": 2.3803142754,
                    "Debt\/EBITDA": 1.1487440598,
                    "E\/P": 0.3497410889,
                    "P\/E": 2.8592579822,
                    "P_current\/E": 2.7904009996,
                    "P\/B": 0.5180165758,
                    "P\/S": 0.3040907466,
                    "L\/A": 0.4800060825,
                    "capital": 55107034000,
                    "capitalization": 28546357057,
                    "earnings": 9983834000,
                    "ebitda": 22406441000,
                    "revenue": 93874468000,
                    "total_cash": 263795000,
                    "total_debt": 25739266000,
                    "total_liabilities": 50869271000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.mrsk-cp.ru",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Центральном и Приволжском федеральных округах. Входит в «Россети»",
                    "assets": 105976305000,
                    "current_capitalisacion": 27858900373.0296020508
                }, {
                    "index": 255,
                    "link": "https:\/\/tezis.io\/company\/20",
                    "short_name": "Башнефт ао",
                    "ticker": "BANE",
                    "EV\/EBITDA": 2.9289038634,
                    "EV_curent\/EBITDA": 2.567930622,
                    "Debt\/EBITDA": 0.90825075,
                    "E\/P": 0.313220507,
                    "P\/E": 3.1926389796,
                    "P_current\/E": 2.6828689637,
                    "P\/B": 0.6793471424,
                    "P\/S": 0.3550113096,
                    "L\/A": 0.4009326873,
                    "capital": 468110000000,
                    "capitalization": 318009190844,
                    "earnings": 99607000000,
                    "ebitda": 140666000000,
                    "revenue": 895772000000,
                    "total_cash": 33772000000,
                    "total_debt": 127760000000,
                    "total_liabilities": 313288000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.bashneft.ru",
                    "company_desc": "Нефтяная компания. Головной офис расположен в Башкирии",
                    "assets": 781398000000,
                    "current_capitalisacion": 267232528867.5
                }, {
                    "index": 259,
                    "link": "https:\/\/tezis.io\/company\/143",
                    "short_name": "Сургнфгз",
                    "ticker": "SNGS",
                    "EV\/EBITDA": 1.9852308101,
                    "EV_curent\/EBITDA": 2.2508291177,
                    "Debt\/EBITDA": 0.0,
                    "E\/P": 0.3745916468,
                    "P\/E": 2.669573677,
                    "P_current\/E": 2.9494519515,
                    "P\/B": 0.2743854469,
                    "P\/S": 0.7271519368,
                    "L\/A": 0.1512666823,
                    "capital": 4327984000000,
                    "capitalization": 1187535824067,
                    "earnings": 444841000000,
                    "ebitda": 468758000000,
                    "revenue": 1633133000000,
                    "total_cash": 256943000000,
                    "total_debt": 0,
                    "total_liabilities": 771361000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.surgutneftegas.ru",
                    "company_desc": "Нефтегазодобывающая компания. Одна из крупнейших в России",
                    "assets": 5099345000000,
                    "current_capitalisacion": 1312037155541.125
                }, {
                    "index": 263,
                    "link": "https:\/\/tezis.io\/company\/155",
                    "short_name": "Татнфт 3ап",
                    "ticker": "TATNP",
                    "EV\/EBITDA": 4.7896998542,
                    "EV_curent\/EBITDA": 0.1352254543,
                    "Debt\/EBITDA": 0.0356663442,
                    "E\/P": 0.133428693,
                    "P\/E": 7.4946398508,
                    "P_current\/E": 0.4216568231,
                    "P\/B": 2.0215776588,
                    "P\/S": 1.7668071023,
                    "L\/A": 0.352541018,
                    "capital": 819663000000,
                    "capitalization": 1657012408530,
                    "earnings": 221093000000,
                    "ebitda": 335975000000,
                    "revenue": 937857000000,
                    "total_cash": 59776000000,
                    "total_debt": 11983000000,
                    "total_liabilities": 446306000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "3",
                    "sector_name": "Нефть и газ",
                    "company_url": "www.tatneft.ru",
                    "company_desc": "Нефтяная компания. Головной офис находится в Татарстане",
                    "assets": 1265969000000,
                    "current_capitalisacion": 93225372000.0
                }, {
                    "index": 264,
                    "link": "https:\/\/tezis.io\/company\/145",
                    "short_name": "ТГК-1",
                    "ticker": "TGKA",
                    "EV\/EBITDA": 1.6886101226,
                    "EV_curent\/EBITDA": 1.8896281761,
                    "Debt\/EBITDA": 0.4180835351,
                    "E\/P": 0.2965368707,
                    "P\/E": 3.3722619301,
                    "P_current\/E": 3.7935489753,
                    "P\/B": 0.297962482,
                    "P\/S": 0.4086003864,
                    "L\/A": 0.234584222,
                    "capital": 133159686000,
                    "capitalization": 39676590542,
                    "earnings": 11765572000,
                    "ebitda": 24657900000,
                    "revenue": 97103654000,
                    "total_cash": 8348073000,
                    "total_debt": 10309062000,
                    "total_liabilities": 40810710000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "www.tgc1.ru",
                    "company_desc": "Производит электричество и тепло. Работает в Северо-западном регионе России",
                    "assets": 173970396000,
                    "current_capitalisacion": 44633273603.8921813965
                }, {
                    "index": 268,
                    "link": "https:\/\/tezis.io\/company\/406",
                    "short_name": "ТРК ао",
                    "ticker": "TORS",
                    "EV\/EBITDA": 1.9436161619,
                    "EV_curent\/EBITDA": 3.1365222246,
                    "Debt\/EBITDA": 0.0950812388,
                    "E\/P": -0.0494932013,
                    "P\/E": -20.2047952626,
                    "P_current\/E": -26.5725128111,
                    "P\/B": 0.3599550017,
                    "P\/S": 0.1929503156,
                    "L\/A": 0.2223104611,
                    "capital": 3549865000,
                    "capitalization": 1277791662,
                    "earnings": -63242000,
                    "ebitda": 337585000,
                    "revenue": 6622387000,
                    "total_cash": 653754000,
                    "total_debt": 32098000,
                    "total_liabilities": 1014765000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/www.trk.tom.ru\/",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Томской области. Входит в «Россети»",
                    "assets": 4564630000,
                    "current_capitalisacion": 1680498855.2000000477
                }, {
                    "index": 269,
                    "link": "https:\/\/tezis.io\/company\/406",
                    "short_name": "ТРК ао",
                    "ticker": "TORS",
                    "EV\/EBITDA": 1.9436161619,
                    "EV_curent\/EBITDA": 3.1365222246,
                    "Debt\/EBITDA": 0.0950812388,
                    "E\/P": -0.0494932013,
                    "P\/E": -20.2047952626,
                    "P_current\/E": -26.5725128111,
                    "P\/B": 0.3599550017,
                    "P\/S": 0.1929503156,
                    "L\/A": 0.2223104611,
                    "capital": 3549865000,
                    "capitalization": 1277791662,
                    "earnings": -63242000,
                    "ebitda": 337585000,
                    "revenue": 6622387000,
                    "total_cash": 653754000,
                    "total_debt": 32098000,
                    "total_liabilities": 1014765000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "4",
                    "sector_name": "Электроэнергетика",
                    "company_url": "http:\/\/www.trk.tom.ru\/",
                    "company_desc": "Распределительная сетевая компания. Передает электричество в Томской области. Входит в «Россети»",
                    "assets": 4564630000,
                    "current_capitalisacion": 1680498855.2000000477
                }, {
                    "index": 271,
                    "link": "https:\/\/tezis.io\/company\/135",
                    "short_name": "СевСт-ао",
                    "ticker": "CHMF",
                    "EV\/EBITDA": 4.9666911984,
                    "EV_curent\/EBITDA": 4.3009851542,
                    "Debt\/EBITDA": 0.585106927,
                    "E\/P": 0.144561913,
                    "P\/E": 6.917451348,
                    "P_current\/E": 5.8924971723,
                    "P\/B": 4.2798585912,
                    "P\/S": 1.6286705,
                    "L\/A": 0.5387741805,
                    "capital": 205287000000,
                    "capitalization": 878599330608,
                    "earnings": 127012000000,
                    "ebitda": 195554000000,
                    "revenue": 539458000000,
                    "total_cash": 21763000000,
                    "total_debt": 114420000000,
                    "total_liabilities": 239803000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "7",
                    "sector_name": "Металлургия",
                    "company_url": "www.severstal.com",
                    "company_desc": "Одно из крупнейших в мире сталелитейных и горнодобывающих предприятий",
                    "assets": 445090000000,
                    "current_capitalisacion": 748417850844.0
                }, {
                    "index": 278,
                    "link": "https:\/\/tezis.io\/company\/97",
                    "short_name": "НКНХ ао",
                    "ticker": "NKNC",
                    "EV\/EBITDA": 4.0298950956,
                    "EV_curent\/EBITDA": 4.3027678229,
                    "Debt\/EBITDA": 0.9624952836,
                    "E\/P": 0.1867906175,
                    "P\/E": 5.3535879551,
                    "P_current\/E": 5.7419199857,
                    "P\/B": 1.0937155694,
                    "P\/S": 0.7596625109,
                    "L\/A": 0.2902994758,
                    "capital": 136738000000,
                    "capitalization": 149552479525,
                    "earnings": 27935000000,
                    "ebitda": 39755000000,
                    "revenue": 196867000000,
                    "total_cash": 27608000000,
                    "total_debt": 38264000000,
                    "total_liabilities": 55932000000,
                    "last_fiscal_year": 2019,
                    "last_fiscal_quarter": 2,
                    "sector_id": "6",
                    "sector_name": "Химия и агрохимия",
                    "company_url": "www.nknh.ru",
                    "company_desc": "Нефтехимическая компания. Производит и продает пластик и синтетический каучук. Входит в ТАИФ",
                    "assets": 192670000000,
                    "current_capitalisacion": 160400534800.0
                }]
            };
        }


        class MoexHelper {
            static async getDataFrame(security, dateFrom, limit = 100) {
                const history = await MoexHelper.getHistoryData(security, dateFrom, limit);
                return MoexHelper.getPreprocessingPrices(history);
            }

            static async getActualIndices(security) {
                const url = MoexHelper.getUrlForActualIndices(security);
                const response = await axios.get(url);
                return response.data.indices.data;
            }

            static getUrlForActualIndices(security) {
                return `https://iss.moex.com/iss/securities/${security}/indices.json?only_actual=1`;
            }

            static getUrlForLoadData(security, dateFrom, limit = 100) {
                const baseUrl = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/';
                const params = `${security}.json?from=${dateFrom}&limit=${limit}`;
                return baseUrl + params;
            }

            static async getHistoryData(security, dateFrom, limit = 100) {
                //API https://iss.moex.com/iss/reference/65
                const url = MoexHelper.getUrlForLoadData(security, dateFrom, limit);
                const response = await axios.get(url);
                return response.data.history;
            }

            static getPreprocessingPrices(historyData) {
                const df = new DataFrame.DataFrame(historyData.data, historyData.columns);
                return df;
            }

            static async getHistoryPrices(security, limit = 100) {
                const dates = getDatesSplitLoad(limit);
                const dataframes = [];
                for (const date of dates) {
                    const df = await MoexHelper.getDataFrame(security, date, limit);
                    dataframes.push(df);
                }
                let unionDf = dataframes[0];
                for (const df of dataframes) {
                    unionDf = unionDf.union(df);
                }
                const cleanDf = unionDf.dropDuplicates();
                return cleanDf;


                /*
                * вернуть интервалы дат котоорыеми будем отправлять запросы
                *   вычисляем старотовую дату
                *   потом считаем количество дат которые будут
                *   к стартовой дате прибавляем 100
                *   если новая дата больше текущей тогда возвращаем массив
                *   иначе добавлем массив и прибаляем 100
                *
                *
                * пройтись по всем интервалам дат и плавненько их вырузим
                *
                * смержим все данные
                *
                * вернём пользователю
                *
                * */

                /*
                *
                * распичатать методические рекомендации
                * нужны данные по коэфицентам - есть
                * нужны данные котировок индексов
                *
                * в принципе этого достаточно чтобы расчитать манипуляции за этот период
                *
                * сделать также как на фронте
                *   на выходе иметь датафрейм с уникальными значениями
                *
                * получить в итоге [
                * {data: '15-01-2018'
                * instrument:
                * waprise:
                * openPrice:
                * closePrice:
                * esterdayPriceClose:
                * sigma:
                * betta:
                * f_minus:
                * f_plys:
                * fz_number:
                * difference:
                * }]
                *
                * вход [дата, название бумаги]
                * вход [название бумаги, количество торговых дней]
                *
                * задача - фильтрануть исходный датасет по бумаге
                *
                * в идеале к исходному датасету подмержить катеровки?
                *
                * подтянуть катеровки за указаные торговые периоды
                * просто можно в тупую стягивать их все
                * потом мержить по дате
                *
                * тогда расчитывать коэфиценты манипуляций по методикам и добавлять их как новые столбцы?
                *
                * допустим весь
                *
                *
                * */

                function getDatesSplitLoad(limit) {
                    const dates = [];
                    let dateLoss = new Date();
                    dateLoss.setDate(dateLoss.getDate() - limit);
                    const now = new Date()
                    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf();
                    while (dateLoss.valueOf() < today) {
                        dates.push(getDateFormat(dateLoss));
                        dateLoss.setDate(dateLoss.getDate() + 100);

                    }
                    return dates;

                    function getDateFormat(date) {
                        const year = date.getFullYear();
                        const month = getForMoexFormat(date.getMonth() + 1);
                        const day = getForMoexFormat(date.getDay());
                        const dateFormat = `${year}-${month}-${day}`;
                        return dateFormat;

                        function getForMoexFormat(dayOrMonth) {
                            if (dayOrMonth > 9) {
                                return dayOrMonth;
                            }
                            return `0${dayOrMonth}`;
                        }
                    }
                }
            }
        }

    }
);


