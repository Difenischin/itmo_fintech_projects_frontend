describe('Index page', () => {
        const axios = require("axios");
        const DataFrame = require('dataframe-js');
        const accuracy = 5;

        it('Баланс агента', () => {
            const balansAgent = 10;
            expect(balansAgent).toEqual(url);
        });

        function getShortAgent() {

        }

        class AgentShort {
            constructor(stop, take, startBalance, priceOpenPosition){
                this.stop = stop;
                this.take = take;
                this.startBalance = startBalance;
                this.priceOpenPosition = priceOpenPosition;
                this.balance = startBalance;
            }
            getState(marketPrice){
                const s = this.balance + (marketPrice - priceOpenPosition);
                return a;
            }
            getPriceOpenPosition(){
                if (this.priceOpenPosition && this.balance <= 0){
                    return null;
                }

            }
        }

        it('Стартовый баланс', () => {
            const balansAgent = 10;
            expect(balansAgent).toEqual(url);
        });

        it('Решение агента', () => {
            const balansAgent = 10;
            const ansver = 100;
            expect(balansAgent).toEqual(url);
        });


        /*

        *
        * Задаем агентов с их параметрами
        *
        * Задаём началную цену рынка
        *
        * цикл по колличеству операций
            * Пробигаемся по всем агентам и получаем их решение о покупке продажи
            *
            * объединяем все решения в лог заявок
            *
            * по логу заявок совершаем сделки
            *   у каждого агента сводим балансы
            *
            * по оставшимся заявкам полуаем цену рынка
            *
            * подаём её на вход всем агентам
        *
        *
        * Задаем агентов типа шёрт()
        *   создать (шерт)
        *
        *   он может вернуть
        *       решение - цена
        *
        *
        *
        * */


        class MoexHelper {
            static async getDataFrame(security, dateFrom, limit = 100) {
                const history = await MoexHelper.getHistoryData(security, dateFrom, limit);
                return MoexHelper.getPreprocessingPrices(history);
            }

            static getUrlForLoadData(security, dateFrom, limit = 100) {
                const baseUrl = 'https://iss.moex.com/iss/history/engines/stock/markets/shares/boards/TQBR/securities/';
                const params = `${security}.json?from=${dateFrom}&limit=${limit}`;
                return baseUrl + params;
            }

            static async getHistoryData(security, dateFrom, limit = 100) {
                //API https://iss.moex.com/iss/reference/65
                const url = MoexHelper.getUrlForLoadData(security, dateFrom, limit);
                const response = await axios.get(url);
                return response.data.history;
            }

            static getPreprocessingPrices(historyData) {
                const df = new DataFrame.DataFrame(historyData.data, historyData.columns);
                return df;
            }

            static async getHistoryPrices(security, limit = 100) {
                const dates = getDatesSplitLoad(limit);
                const dataframes = [];
                for (const date of dates) {
                    const df = await MoexHelper.getDataFrame(security, date, 100);
                    dataframes.push(df);
                }
                let unionDf = dataframes[0];
                for (const df of dataframes) {
                    unionDf = unionDf.union(df);
                }
                const cleanDf = unionDf.dropDuplicates();
                return cleanDf;


                /*
                * вернуть интервалы дат котоорыеми будем отправлять запросы
                *   вычисляем старотовую дату
                *   потом считаем количество дат которые будут
                *   к стартовой дате прибавляем 100
                *   если новая дата больше текущей тогда возвращаем массив
                *   иначе добавлем массив и прибаляем 100
                *
                *
                * пройтись по всем интервалам дат и плавненько их вырузим
                *
                * смержим все данные
                *
                * вернём пользователю
                *
                * */

                function getDatesSplitLoad(limit) {
                    const dates = [];
                    let dateLoss = new Date();
                    dateLoss.setDate(dateLoss.getDate() - limit);
                    const now = new Date()
                    const today = new Date(now.getFullYear(), now.getMonth(), now.getDate()).valueOf();
                    while (dateLoss.valueOf() < today) {
                        dates.push(getDateFormat(dateLoss));
                        dateLoss.setDate(dateLoss.getDate() + 100);

                    }
                    return dates;

                    function getDateFormat(date) {
                        const year = date.getFullYear();
                        const month = date.getMonth() + 1;
                        const day = date.getDay();
                        const dateFormat = `${year}-${month}-${day}`;
                        return dateFormat;
                    }
                }
            }
        }

    }
);


