const loadData = {
    strategies: [
        {
            id: 1,
            name: 'negative',
            params: [
                {name: 'border', value: 1500},
                {name: 'stop', value: 1500}
            ],
            transactionsProd: [],
            transactionsTest: [],
            currentState:'active',
            connect:'online'
        },
        {
            id: 2,
            name: 'positive',
            params: [
                {name: 'border', value: 1500},
                {name: 'stop', value: 1500}],
            transactionsProd: [],
            transactionsTest: [],
            currentState:'archive',
            connect:'offline'
        }
    ],
    transactionsProd:[
        {
            id:'1',
            direction: "buy",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18000,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'limit',
        },
        {
            id:'2',
            direction: "sell",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18900,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'take'
        },
        {
            id:'3',
            direction: "buy",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18000,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'limit'
        },
    ],
    transactionsTest:[
        {
            id:'1',
            direction: "buy",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18000,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'time',
        },
        {
            id:'2',
            direction: "sell",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18900,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'take'
        },
        {
            id:'3',
            direction: "buy",
            dateCreate: '08.01.2018',
            dateDone: "08.01.2018",
            timeDone: "12:15",
            price: 18000,
            volume: 1,
            securityId: 1,
            stockStatus: 'done',
            tradingAccount: 'ASD',
            comment: '',
            strategyId: 1,
            type: 'limit'
        },
    ],
    instruments:[
        {
            id: 1,
            shortName: 'MTSS',
        },
        {
            id: 2,
            shortName: 'SR08',
        }
    ]
};

/*
* стратегия с её описанием
*
* */

const transaction = {
    direction: "buy",
    dateCreate: '08.01.2018',
    dateDone: "08.01.2018",
    timeDone: "12:15",
    price: 18000,
    volume: 1,
    securityId: 1,
    stockStatus: 'done',
    tradingAccount: 'ASD',
    comment: '',
    strategyId: 1,
    type: 'stop'
};

const desirableOrder = {
    id: 1,
    priceDesirable: 10,
    priceMarketDepth: 100,
};

const security = {
    id: 1,
    shortName: 'MTSS',
};

const strategy = {
    id: 1,
    name: 'negative',
    params: [{name: 'border', value: 1500}],
    transactions: []
};

