describe('Index page', () => {
        const accuracy = 5;
        const forTest = {
            strikePrice: 21,
            spotPrice: 21,
            rate: 0.04,
            time: 2,
            sigma: 0.1,
            countPriceStep: 10
        };
        it('Тестирование определения колличества необходимых итераций', () => {
            expect(getCountStepBySustainability({sigma: 0.1, time: 2, countPriceStep: 10})).toBeCloseTo(3, accuracy);
        });

        it('Сборные тесты', () => {
            const a = getPriceOptionsCallDifferenceGrid(forTest);
            expect(a).toEqual({
                "priceAmericanPut": 1.7456218024399859,
                "priceEuropeCall": 2.2615850059031875,
                "priceEuropePut": 1.7456218024399859
            });
        });


        it('Сборные тесты 2', () => {
            const forTest = {
                strikePrice: 11,
                spotPrice: 11,
                rate: 0.05,
                time: 1,
                sigma: 0.2,
                countPriceStep: 100
            };

            expect(getPriceOptionsCallDifferenceGrid(forTest)).toEqual({
                "priceAmericanPut": 0.6865969,//0.6698989,
                "priceEuropeCall": 1.172175,
                "priceEuropePut": 0.6665976//0.613087
            });
        });

        it('Тестирование определения шага по времени', () => {
            expect(getDeltaTime(3, 2)).toBeCloseTo(0.6666667, accuracy);
        });


        it('Тестирование стартовой матрицы', () => {
            const k = getCountStepBySustainability({sigma: 0.1, time: 2, countPriceStep: 10});
            const s = 10
            expect(getStartMatrix(k, s).length).toBeCloseTo(10, accuracy);
            expect(getStartMatrix(k, s)[0].length).toBeCloseTo(3, accuracy);
        });

        it('Тестирование выставления верхней границы', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setMaxBorderCall(matrix, forTest);
            const trueBorder = [22.61455672588065, 22.090657291638692, 21.552599263583954];
            expect(matrix[0]).toEqual(trueBorder);
        });


        it('Тестирование функций выплаты для колла', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setExpirationPayoffCall(matrix, forTest);
            const trueBorder = [[0, 0, 0], [0, 0, 16.333333333333332], [0, 0, 11.666666666666666], [0, 0, 7], [0, 0, 2.333333333333332], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирование матрицы для кола с граничными условиями', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setMaxBorderCall(matrix, forTest);
            setExpirationPayoffCall(matrix, forTest);
            const trueBorder = [[22.61455672588065, 22.090657291638692, 21.552599263583954], [0, 0, 16.333333333333332], [0, 0, 11.666666666666666], [0, 0, 7], [0, 0, 2.333333333333332], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирвание вычисления узла по трём точкам', () => {
            const matrix = {mDown: 0, mMiddle: 0, mUp: 2.333333};
            const priceIndex = 5;
            const countStepBySustainability = 3;
            const a = getPriceForFiniteDifferenceGrid(matrix, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(0.3409091, accuracy);
        });

        it('Тестирвание вычисления узла по трём точкам 2', () => {
            const matrix = {mDown: 11.66667, mMiddle: 16.33333, mUp: 21.55260};
            const priceIndex = 9;
            const countStepBySustainability = 3;
            const a = getPriceForFiniteDifferenceGrid(matrix, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(17.20992, accuracy);
        });
        it('Тестирвание вычисления узла по трём точкам 3', () => {
            const matrix = {mDown: 0, mMiddle: 2.333333, mUp: 7.000000};
            const priceIndex = 6;
            const countStepBySustainability = 3;
            const a = getPriceForFiniteDifferenceGrid(matrix, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(3.090908844155844, accuracy);

        });

        it('Тестирвание вычисления узла по трём точкам 5', () => {
            const forTest = {
                strikePrice: 11,
                spotPrice: 11,
                rate: 0.05,
                time: 1,
                sigma: 0.2,
                countPriceStep: 21
            };
            const matrix = {mDown: 1.1, mMiddle: 0, mUp: 0};
            const priceIndex = 10;
            const countStepBySustainability = 37;
            const a = getPriceForFiniteDifferenceGrid(matrix, priceIndex, countStepBySustainability, forTest);
            expect(a).toBeCloseTo(0.05195682, accuracy);
            const b = getPriceForFiniteDifferenceGrid( {mDown: 2.2, mMiddle: 1.1, mUp: 0}, 9, countStepBySustainability, forTest);
            expect(b).toBeCloseTo(1.08515520, accuracy);
            const c = getPriceForFiniteDifferenceGrid( {mDown: 11, mMiddle: 9.9, mUp: 8.8}, 1, countStepBySustainability, forTest);
            expect(c).toBeCloseTo(9.88515520, accuracy);
        });

        it('Тестирование вычисления матрицы для колла с граничными условиями', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setMaxBorderCall(matrix, forTest);
            setExpirationPayoffCall(matrix, forTest);
            setMatrixForFiniteDifferenceGrid(matrix, forTest);
            const trueBorder = [[22.61455672588065, 22.090657291638692, 21.552599263583954], [17.90446527525003, 17.209915954023774, 16.333333333333332], [13.048112331990096, 12.333333333333334, 11.666666666666666], [8.322215663124755, 7.666666666666666, 7], [3.7948642266824084, 3.090909090909091, 2.333333333333332], [0.7283057851239668, 0.3409090909090908, 0], [0.03541912632821723, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирование вычисления матрицы для европейского пута с граничными условиями', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setMinBorderPut(matrix, forTest);
            expect(matrix).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [19.38544327411935, 19.909342708361308, 20.447400736416046]]);
            setExpirationPayoffPut(matrix, forTest);
            expect(matrix).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 2.3333333333333357], [0, 0, 7], [0, 0, 11.666666666666671], [0, 0, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]]);
            setMatrixForFiniteDifferenceGrid(matrix, forTest);
            const trueBorder = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0.006198347107438031, 0, 0], [0.14551357733175946, 0.09090909090909108, 0], [1.7456218024399859, 2.0075757575757596, 2.3333333333333357], [5.719401810310902, 6.333333333333334, 7], [10.350649350649356, 11.000000000000004, 11.666666666666671], [15.022822389068908, 15.666666666666673, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирование вычисления матрицы для европейского пута с граничными условиями 1', () => {
            const forTest = {
                strikePrice: 11,
                spotPrice: 11,
                rate: 0.05,
                time: 1,
                sigma: 0.2,
                countPriceStep: 20
            };
            const steps = getCountStepBySustainability(forTest) + 21;
            const spot = forTest.countPriceStep + 1;
            const matrix = getStartMatrix(steps, spot);
            setMinBorderPut(matrix, forTest);
            expect(matrix).toEqual([]);
            setExpirationPayoffPut(matrix, forTest);
            expect(matrix).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 2.3333333333333357], [0, 0, 7], [0, 0, 11.666666666666671], [0, 0, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]]);
            setMatrixForFiniteDifferenceGrid(matrix, forTest);
            const trueBorder = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0.006198347107438031, 0, 0], [0.14551357733175946, 0.09090909090909108, 0], [1.7456218024399859, 2.0075757575757596, 2.3333333333333357], [5.719401810310902, 6.333333333333334, 7], [10.350649350649356, 11.000000000000004, 11.666666666666671], [15.022822389068908, 15.666666666666673, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирование вычисления матрицы для американского пута с граничными условиями', () => {
            const steps = getCountStepBySustainability(forTest);
            const spot = 10;
            const matrix = getStartMatrix(steps, spot);
            setMinBorderPut(matrix, forTest);
            expect(matrix).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [19.38544327411935, 19.909342708361308, 20.447400736416046]]);
            setExpirationPayoffPut(matrix, forTest);
            expect(matrix).toEqual([[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 2.3333333333333357], [0, 0, 7], [0, 0, 11.666666666666671], [0, 0, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]]);
            setMatrixForFiniteDifferenceGridAmericanPut(matrix, forTest);
            const trueBorder = [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0.006198347107438031, 0, 0], [0.14551357733175946, 0.09090909090909108, 0], [1.7456218024399859, 2.0075757575757596, 2.3333333333333357], [5.719401810310902, 6.333333333333334, 7], [10.350649350649356, 11.000000000000004, 11.666666666666671], [15.022822389068908, 15.666666666666673, 16.333333333333336], [19.38544327411935, 19.909342708361308, 21]];
            expect(matrix).toEqual(trueBorder);
        });

        it('Тестирование получения правильного ответа по матрице американского пута', () => {
            expect(getOptionDifferenceGridAmericanPut(forTest)).toBeCloseTo(1.745622, accuracy);
        });

        it('Тестирование получения правильного ответа по матрице европейский колл', () => {
            expect(getOptionDifferenceGridEuropeCall(forTest)).toBeCloseTo(2.261585, accuracy);
        });
        it('Тестирование по матрице европейский колл c заданным количеством шагов', () => {
            expect(getOptionDifferenceGridEuropeCall(forTest, 100)).toBeCloseTo(2.261585, accuracy);
        });

        it('Тестирование получения правильного ответа по матрице европейский пут 1 ', () => {
            expect(getOptionDifferenceGridEuropePut(forTest)).toBeCloseTo(1.745622, accuracy);
        });

        it('Тестирование получения правильного ответа по матрице европейский пут 2', () => {
            const forTest = {
                strikePrice: 11,
                spotPrice: 11,
                rate: 0.05,
                time: 1,
                sigma: 0.2,
                countPriceStep: 10
            };
            expect(getOptionDifferenceGridEuropePut(forTest)).toBeCloseTo(1.3002102691194604, accuracy);
        });

        function getCountStepBySustainability({sigma, time, countPriceStep}) {
            return Math.round(sigma ** 2 * time * countPriceStep ** 2) + 1;
        }

        function getDeltaTime(countStep, time) {
            return time / countStep;
        }

        function getStartMatrix(stepIndexMax, priceIndexMax) {
            let matrix = [];
            for (let priceIndex = 0; priceIndex < priceIndexMax; priceIndex++) {
                let priceVector = [];
                for (let step = 0; step < stepIndexMax; step++) {
                    priceVector.push(0);
                }
                matrix.push(priceVector);
            }
            return matrix;
        }

        function setMaxBorderCall(startMatrix, {spotPrice, strikePrice, time, rate}) {
            const dT = getDeltaTime(startMatrix[0].length, time);
            const priceBorder = 0;
            for (let step = startMatrix[0].length - 1; step >= 0; step--) {
                let revers = startMatrix[0].length - step;
                let expPart = Math.exp(-rate * dT * revers);
                startMatrix[priceBorder][step] = 2 * spotPrice - strikePrice * expPart;
            }
        }

        function setExpirationPayoffCall(matrix, {spotPrice, strikePrice, time, rate, countPriceStep}) {
            const expirationStep = matrix[0].length - 1;
            const deltaPrice = (spotPrice * 2) / (countPriceStep - 1);
            for (let priceIndex = matrix.length - 1; priceIndex > 0; priceIndex--) {
                matrix[priceIndex][expirationStep] = getPayoffCall(priceIndex, deltaPrice);
            }

            function getPayoffCall(numberStepPrice, deltaPrice) {
                const strikePriceForStep = numberStepPrice * deltaPrice;
                const profit = spotPrice - strikePriceForStep;
                return Math.max(profit, 0);
            }
        }

        function setMatrixForFiniteDifferenceGrid(matrix, {strikePrice, spotPrice, rate, time, sigma}) {
            const countStep = matrix[0].length;
            for (let step = matrix[0].length - 2; step >= 0; step--) {
                for (let priceIndex = matrix.length - 2; priceIndex >= 1; priceIndex--) {
                    let value = {
                        mDown: matrix[priceIndex + 1][step + 1],
                        mMiddle: matrix[priceIndex][step + 1],
                        mUp: matrix[priceIndex - 1][step + 1]
                    }
                    matrix[priceIndex][step] = getPriceForFiniteDifferenceGrid(value, matrix.length - priceIndex, countStep, {
                        strikePrice,
                        spotPrice,
                        rate,
                        time,
                        sigma
                    });
                }
            }
        }

        function getPriceForFiniteDifferenceGrid({mDown, mMiddle, mUp}, priceIndex, countStepBySustainability, {strikePrice, spotPrice, rate, time, sigma}) {
            const dawn = mDown * getPDawn(priceIndex);
            const middle = mMiddle * getPMiddle(priceIndex);
            const up = mUp * getPUp(priceIndex);
            const denominator = 1 + rate * getDeltaTime(countStepBySustainability);
            const value = (dawn + middle + up) / denominator;
            return value;

            function getPDawn(i) {
                const dawn = (sigma * sigma) * (i * i) - rate * i;
                return (dawn * getDeltaTime(countStepBySustainability)) / 2;
            }

            function getDeltaTime(countStepBySustainability) {
                return time / countStepBySustainability;
            }

            function getPMiddle(i) {
                const m = (sigma * sigma) * (i * i);
                return 1 - (m * getDeltaTime(countStepBySustainability));
            }

            function getPUp(i) {
                const up = (sigma * sigma) * (i * i) + rate * i;
                return (up * getDeltaTime(countStepBySustainability)) / 2;
            }
        }

        function setMinBorderPut(startMatrix, {spotPrice, strikePrice, time, rate}) {
            const dT = getDeltaTime(startMatrix[0].length - 1, time);
            const priceBorder = startMatrix.length - 1;
            const maxStep = startMatrix[0].length - 1;
            for (let step = maxStep; step > 0; step--) {
                let expPart = Math.exp(-rate * dT * (step));
                startMatrix[priceBorder][maxStep - step] = strikePrice * expPart;
            }
        }

        function setExpirationPayoffPut(matrix, {spotPrice, strikePrice, time, rate, countPriceStep}) {
            const expirationStep = matrix[0].length - 1;
            const deltaPrice = (spotPrice * 2) / (countPriceStep - 1);
            for (let priceIndex = matrix.length - 1; priceIndex > 0; priceIndex--) {
                matrix[priceIndex][expirationStep] = getPayoffPut(priceIndex, deltaPrice);
            }

            function getPayoffPut(numberStepPrice, deltaPrice) {
                const strikePriceForStep = numberStepPrice * deltaPrice;
                const profit = strikePriceForStep - spotPrice;
                return Math.max(profit, 0);
            }
        }

        function setMatrixForFiniteDifferenceGridAmericanPut(matrix, {strikePrice, spotPrice, rate, time, sigma}) {
            const countStep = matrix[0].length;
            const deltaPrice = 2 * strikePrice / matrix.length;
            for (let step = matrix[0].length - 2; step >= 0; step--) {
                for (let priceIndex = matrix.length - 2; priceIndex >= 1; priceIndex--) {
                    let value = {
                        mDown: matrix[priceIndex + 1][step + 1],
                        mMiddle: matrix[priceIndex][step + 1],
                        mUp: matrix[priceIndex - 1][step + 1]
                    }
                    let finiteDifferenceGrid = getPriceForFiniteDifferenceGrid(value, matrix.length - priceIndex, countStep, {
                        strikePrice,
                        spotPrice,
                        rate,
                        time,
                        sigma
                    });
                    let payoffPut = -(strikePrice - priceIndex * deltaPrice);
                    matrix[priceIndex][step] = Math.max(payoffPut, finiteDifferenceGrid);
                }
            }
        }

        function getOptionDifferenceGridAmericanPut(optionsParams) {
            const steps = getCountStepBySustainability(optionsParams);
            const matrix = getStartMatrix(steps, optionsParams.countPriceStep);
            setMinBorderPut(matrix, optionsParams);
            setExpirationPayoffPut(matrix, optionsParams);
            setMatrixForFiniteDifferenceGridAmericanPut(matrix, optionsParams);
            return getOptionPutPrice(matrix);
        }

        function getOptionDifferenceGridEuropeCall(optionsParams) {
            const steps = getCountStepBySustainability(optionsParams);
            const matrix = getStartMatrix(steps, optionsParams.countPriceStep);
            setMaxBorderCall(matrix, optionsParams);
            setExpirationPayoffCall(matrix, optionsParams);
            setMatrixForFiniteDifferenceGrid(matrix, optionsParams);
            return getOptionCallPrice(matrix);
        }

        function getOptionDifferenceGridEuropePut(optionsParams) {
            const steps = getCountStepBySustainability(optionsParams);
            const matrix = getStartMatrix(steps, optionsParams.countPriceStep);
            setMinBorderPut(matrix, optionsParams);
            setExpirationPayoffPut(matrix, optionsParams);
            setMatrixForFiniteDifferenceGrid(matrix, optionsParams);
            return getOptionPutPrice(matrix);
        }

        function getOptionPutPrice(grid) {
            const lastStep = 0;
            const countPriceStep = grid.length;
            const middleIndex = Math.floor(countPriceStep / 2);
            return (grid[middleIndex][lastStep] + grid[middleIndex - 1][lastStep]) / 2;
        }

        function getOptionCallPrice(grid) {
            const lastStep = 0;
            const countPriceStep = grid.length;
            const middleIndex = Math.floor(countPriceStep / 2) - 1;
            return (grid[middleIndex][lastStep] + grid[middleIndex + 1][lastStep]) / 2;
        }

        function getPriceOptionsCallDifferenceGrid(optionsParams) {
            const priceEuropeCall = getOptionDifferenceGridEuropeCall(optionsParams);
            const priceEuropePut = getOptionDifferenceGridEuropePut(optionsParams);
            const priceAmericanPut = getOptionDifferenceGridAmericanPut(optionsParams);
            console.log('priceEuropeCall ' + priceEuropeCall);
            console.log('priceEuropePut ' + priceEuropePut);
            console.log('priceAmericanPut ' + priceAmericanPut);
            return {
                priceEuropeCall,
                priceAmericanPut,
                priceEuropePut
            };
        }
    }
);