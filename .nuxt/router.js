import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _2771e0d5 = () => interopDefault(import('../pages/complexSystems.vue' /* webpackChunkName: "pages/complexSystems" */))
const _466424f8 = () => interopDefault(import('../pages/InvestorCompanyBest.vue' /* webpackChunkName: "pages/InvestorCompanyBest" */))
const _5fd38c6a = () => interopDefault(import('../pages/InvestorCompanyBestBt.vue' /* webpackChunkName: "pages/InvestorCompanyBestBt" */))
const _27e9e423 = () => interopDefault(import('../pages/InvestorCompanyFull.vue' /* webpackChunkName: "pages/InvestorCompanyFull" */))
const _75cd8f7e = () => interopDefault(import('../pages/StrategiesSystem.vue' /* webpackChunkName: "pages/StrategiesSystem" */))
const _37ccf09d = () => interopDefault(import('../pages/financialMathematics/americanOptions.vue' /* webpackChunkName: "pages/financialMathematics/americanOptions" */))
const _6fc08f5e = () => interopDefault(import('../pages/financialMathematics/financialMathematics.vue' /* webpackChunkName: "pages/financialMathematics/financialMathematics" */))
const _a12bc7fa = () => interopDefault(import('../pages/financialMathematics/finiteDifferenceGrid.vue' /* webpackChunkName: "pages/financialMathematics/finiteDifferenceGrid" */))
const _130817dd = () => interopDefault(import('../pages/financialMathematics/options.vue' /* webpackChunkName: "pages/financialMathematics/options" */))
const _04097274 = () => interopDefault(import('../pages/financialMathematics/simpleStatistics.vue' /* webpackChunkName: "pages/financialMathematics/simpleStatistics" */))
const _4846ce4b = () => interopDefault(import('../pages/invest/InvestorCorrelationInstruments.vue' /* webpackChunkName: "pages/invest/InvestorCorrelationInstruments" */))
const _7392b31a = () => interopDefault(import('../pages/manipulations/insiderDealing.vue' /* webpackChunkName: "pages/manipulations/insiderDealing" */))
const _29e3639f = () => interopDefault(import('../pages/manipulations/momentumIgnition.vue' /* webpackChunkName: "pages/manipulations/momentumIgnition" */))
const _ab80059a = () => interopDefault(import('../pages/manipulations/spoofing.vue' /* webpackChunkName: "pages/manipulations/spoofing" */))
const _8f331d3e = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/complexSystems",
    component: _2771e0d5,
    name: "complexSystems"
  }, {
    path: "/InvestorCompanyBest",
    component: _466424f8,
    name: "InvestorCompanyBest"
  }, {
    path: "/InvestorCompanyBestBt",
    component: _5fd38c6a,
    name: "InvestorCompanyBestBt"
  }, {
    path: "/InvestorCompanyFull",
    component: _27e9e423,
    name: "InvestorCompanyFull"
  }, {
    path: "/StrategiesSystem",
    component: _75cd8f7e,
    name: "StrategiesSystem"
  }, {
    path: "/financialMathematics/americanOptions",
    component: _37ccf09d,
    name: "financialMathematics-americanOptions"
  }, {
    path: "/financialMathematics/financialMathematics",
    component: _6fc08f5e,
    name: "financialMathematics-financialMathematics"
  }, {
    path: "/financialMathematics/finiteDifferenceGrid",
    component: _a12bc7fa,
    name: "financialMathematics-finiteDifferenceGrid"
  }, {
    path: "/financialMathematics/options",
    component: _130817dd,
    name: "financialMathematics-options"
  }, {
    path: "/financialMathematics/simpleStatistics",
    component: _04097274,
    name: "financialMathematics-simpleStatistics"
  }, {
    path: "/invest/InvestorCorrelationInstruments",
    component: _4846ce4b,
    name: "invest-InvestorCorrelationInstruments"
  }, {
    path: "/manipulations/insiderDealing",
    component: _7392b31a,
    name: "manipulations-insiderDealing"
  }, {
    path: "/manipulations/momentumIgnition",
    component: _29e3639f,
    name: "manipulations-momentumIgnition"
  }, {
    path: "/manipulations/spoofing",
    component: _ab80059a,
    name: "manipulations-spoofing"
  }, {
    path: "/",
    component: _8f331d3e,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
