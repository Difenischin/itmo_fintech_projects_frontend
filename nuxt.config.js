import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import colors from 'vuetify/es5/util/colors'
import pkg from './package'

export default {
    mode: 'spa',

    head: {
        title: 'integrator',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'Проекты посвященные финансам', content: 'Проекты в университете ИТМО'},
            {name: 'robots', content: 'noindex'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'}
        ]
    },
    plugins: [
        '@/plugins/vueChartkick.js',
        '@/plugins/vueTable2.js',
    ],

    css: [
        '~/assets/style/app.styl'
    ],
    modules: [
        [
            '@nuxtjs/yandex-metrika',
            {
                id: 48692231,
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            }
        ],
    ],
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#3B8070'},

    buildModules: [
        '@nuxtjs/vuetify',
    ],

    vuetify: {
        customVariables: ['~/assets/variables.scss'],
        theme: {
            dark: false,
            themes: {
                dark: {
                    primary: colors.blue.darken2,
                    accent: colors.grey.darken3,
                    secondary: colors.amber.darken3,
                    info: colors.teal.lighten1,
                    warning: colors.amber.base,
                    error: colors.deepOrange.accent4,
                    success: colors.green.accent3
                }
            }
        }
    },

    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
        }
    },
}
